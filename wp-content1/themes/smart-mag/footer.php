
	<footer class="main-footer">
	
	<?php if (!Bunyad::options()->disable_footer): ?>
		<div class="wrap">
		
		<?php /*?><?php if (is_active_sidebar('main-footer')): ?>
			<ul class="widgets row cf">
				<?php dynamic_sidebar('main-footer'); ?>
               
			</ul>
		<?php endif; ?><?php */?>
			 <div class="foot1"><?php dynamic_sidebar('footer_column1'); ?></div>
			<div class="foot1">	<?php dynamic_sidebar('footer_column2'); ?></div>
            <div class="foot1 footlast"><?php dynamic_sidebar('footer_column3'); ?></div>
            <div class="clear"></div>
		</div>
	
	<?php endif; ?>

	
	<?php if (!Bunyad::options()->disable_lower_footer): ?>
		<div class="lower-foot">
			<div class="wrap">
		
			<?php if (is_active_sidebar('lower-footer')): ?>
			
			<div class="widgets">
				<?php dynamic_sidebar('lower-footer'); ?>
			</div>
			
			<?php endif; ?>
		
			</div>
		</div>		
	<?php endif; ?>
	
	</footer>
	
</div> <!-- .main-wrap -->

<?php wp_footer(); ?>

</body>
</html>