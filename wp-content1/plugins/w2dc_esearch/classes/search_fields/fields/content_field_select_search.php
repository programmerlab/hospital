<?php 

class w2dc_content_field_select_search extends w2dc_content_field_search {
	public $value = array();

	public function renderSearch($random_id) {
		w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/select_checkbox_radio_input.tpl.php'), array('search_field' => $this, 'random_id' => $random_id));
	}
	
	public function validateSearch(&$args, $defaults = array()) {
		$field_index = 'field_' . $this->content_field->slug;

		$value = w2dc_getValue($_GET, $field_index, w2dc_getValue($defaults, $field_index));
		if (!is_array($value))
			$value = array_filter(explode(',', $value), 'trim');

		if ($value) {
			$this->value = $value;
			$args['meta_query']['relation'] = 'AND';
			$args['meta_query'][] = array(
					'key' => '_content_field_' . $this->content_field->id,
					'value' => $this->value,
					'compare' => 'IN'
			);
		}
	}
}
?>