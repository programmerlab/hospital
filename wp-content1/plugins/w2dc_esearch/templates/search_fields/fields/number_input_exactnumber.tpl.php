<div class="w2dc_field w2dc_field_search_block_<?php echo $random_id; ?> w2dc_field_search_block_<?php echo $search_field->content_field->id; ?>_<?php echo $random_id; ?> row">
	<label><?php echo $search_field->content_field->name; ?></label>
	<div class="col-md-3 form-group">
		<input type="text" name="field_<?php echo $search_field->content_field->slug; ?>" class="w2dc_field_search_input_number form-control" value="<?php echo esc_attr($search_field->value); ?>" size="9" />
	</div>
</div>