<?php if ($is_advanced_search_panel): ?>
<script>
	jQuery(document).ready(function($) {
		$("#advanced_search_label_<?php echo $random_id; ?>").click(function(){
			$("#w2dc_advanced_search_fields_<?php echo $random_id; ?>").toggle();
			if ($("#w2dc_advanced_search_fields_<?php echo $random_id; ?>").is(":hidden"))
				$("#use_advanced_<?php echo $random_id; ?>").val(0);
			else
				$("#use_advanced_<?php echo $random_id; ?>").val(1);
		});
	});
</script>

<div class="col-md-4 pull-right">
	<a id="advanced_search_label_<?php echo $random_id; ?>" class="advanced_search_label" href="javascript: void(0);"><?php _e('Advanced search', 'W2DC-ESEARCH'); ?></a>
</div>
<?php endif; ?>