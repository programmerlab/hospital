	<?php if ($frontend_controller->listings): ?>
		<table class="table table-striped">
			<tr>
				<th class="td_listings_id"><?php _e('ID', 'W2DC-FSUBMIT'); ?></th>
				<th class="td_listings_title"><?php _e('Title', 'W2DC-FSUBMIT'); ?></th>
				<?php 
				// adapted for WPML
				if (function_exists('icl_object_id') && get_option('w2dc_enable_frontend_translations') && ($languages = icl_get_languages()) && count($languages) > 1): ?>
				<th class="td_listings_translations">
					<?php foreach ($languages AS $lang_code=>$lang): ?>
					<?php if ($lang_code != ICL_LANGUAGE_CODE && icl_object_id($w2dc_instance->dashboard_page_id, 'page', false, $lang_code)): ?>
					<img src="<?php echo $lang['country_flag_url']; ?>" title="<?php esc_attr_e($lang['native_name']); ?>" />&nbsp;&nbsp;
					<?php endif; ?>
					<?php endforeach; ?>
				</th>
				<?php endif; ?>
				<th class="td_listings_level"><?php _e('Level', 'W2DC-FSUBMIT'); ?></th>
				<th class="td_listings_status"><?php _e('Status', 'W2DC-FSUBMIT'); ?></th>
				<th class="td_listings_date"><?php _e('Expiration date', 'W2DC-FSUBMIT'); ?></th>
				<th class="td_listings_options"></th>
			</tr>
		<?php while ($frontend_controller->query->have_posts()): ?>
			<?php $frontend_controller->query->the_post(); ?>
			<?php $listing = $frontend_controller->listings[get_the_ID()]; ?>
			<tr>
				<td class="td_listings_id"><?php echo $listing->post->ID; ?></td>
				<td class="td_listings_title">
					<?php
					if (current_user_can('edit_post', $listing->post->ID))
						echo '<a href="' . get_edit_post_link($listing->post->ID) . '">' . $listing->title() . '</a>';
					else
						echo $listing->title();
					do_action('w2dc_dashboard_listing_title', $listing);
					?>
					<?php if ($listing->post->post_status == 'pending') echo ' - ' . __('Pending', 'W2DC-FSUBMIT'); ?>
					<?php if ($listing->post->post_status == 'draft') echo ' - ' . __('Draft', 'W2DC-FSUBMIT'); ?>
				</td>
				<?php 
				// adapted for WPML
				if (function_exists('icl_object_id') && get_option('w2dc_enable_frontend_translations') && ($languages = icl_get_languages()) && count($languages) > 1):
				global $sitepress;
				$trid = $sitepress->get_element_trid($listing->post->ID, 'post_' . W2DC_POST_TYPE);
				$translations = $sitepress->get_element_translations($trid);
				?>
				<td class="td_listings_translations">
					<?php foreach ($languages AS $lang_code=>$lang): ?>
					<?php if ($lang_code != ICL_LANGUAGE_CODE && icl_object_id($w2dc_instance->dashboard_page_id, 'page', false, $lang_code)): ?>
					<?php $lang_details = $sitepress->get_language_details($lang_code); ?>
					<?php if (isset($translations[$lang_code])): ?>
					<a style="text-decoration:none" title="<?php echo sprintf(__('Edit the %s translation', 'sitepress'), $lang_details['display_name']); ?>" href="<?php echo add_query_arg(array('w2dc_action' => 'edit_listing', 'listing_id' => icl_object_id($listing->post->ID, W2DC_POST_TYPE, true, $lang_code)), get_permalink(icl_object_id($w2dc_instance->dashboard_page_id, 'page', true, $lang_code))); ?>">
						<img src="<?php echo ICL_PLUGIN_URL; ?>/res/img/edit_translation.png" alt="<?php esc_attr_e(__('edit', 'sitepress')); ?>" />
					</a>&nbsp;&nbsp;
					<?php else: ?>
					<a style="text-decoration:none" title="<?php echo sprintf(__('Add translation to %s', 'sitepress'), $lang_details['display_name']); ?>" href="<?php echo w2dc_dashboardUrl(array('w2dc_action' => 'add_translation', 'trid' => $trid, 'to_lang' => $lang_code)); ?>">
						<img src="<?php echo ICL_PLUGIN_URL; ?>/res/img/add_translation.png" alt="<?php esc_attr_e(__('add', 'sitepress')); ?>" />
					</a>&nbsp;&nbsp;
					<?php endif; ?>
					<?php endif; ?>
					<?php endforeach; ?>
				</td>
				<?php endif; ?>
				<td class="td_listings_level">
					<?php if ($listing->level->isUpgradable())
						echo '<a href="' . w2dc_dashboardUrl(array('w2dc_action' => 'upgrade_listing', 'listing_id' => $listing->post->ID)) . '">'; ?>
					<?php echo $listing->level->name; ?>
					<?php if ($listing->level->isUpgradable())
						echo '</a>'; ?>
				</td>
				<td class="td_listings_status">
					<?php
					if ($listing->status == 'active')
						echo '<span class="w2dc_badge listing_status_active">' . __('active', 'W2DC-FSUBMIT') . '</span>';
					elseif ($listing->status == 'expired')
						echo '<span class="w2dc_badge listing_status_expired">' . __('expired', 'W2DC-FSUBMIT') . '</span>';
					elseif ($listing->status == 'unpaid')
						echo '<span class="w2dc_badge listing_status_unpaid">' . __('unpaid', 'W2DC-FSUBMIT') . '</span>';
					elseif ($listing->status == 'stopped')
						echo '<span class="w2dc_badge listing_status_stopped">' . __('stopped', 'W2DC-FSUBMIT') . '</span>';
					do_action('w2dc_listing_status_option', $listing);
					?>
				</td>
				<td class="td_listings_date"><?php if ($listing->level->eternal_active_period) _e('Eternal active period', 'W2DC-FSUBMIT'); else echo date_i18n(get_option('date_format') . ' ' . get_option('time_format'), intval($listing->expiration_date)); ?></td>
				<td class="td_listings_options" width="86">
					<?php if(current_user_can('edit_post', $listing->post->ID)): ?>
					<div class="btn-group">
						<a href="<?php echo get_edit_post_link($listing->post->ID); ?>" class="btn btn-primary btn-xs" title="<?php _e('edit listing', 'W2DC-FSUBMIT'); ?>"><span class="glyphicon glyphicon-edit"></span></a>
						<a href="<?php echo w2dc_dashboardUrl(array('w2dc_action' => 'delete_listing', 'listing_id' => $listing->post->ID)); ?>" class="btn btn-primary btn-xs" title="<?php _e('delete listing', 'W2DC-FSUBMIT'); ?>"><span class="glyphicon glyphicon-trash"></span></a>
						<?php
						if ($listing->level->raiseup_enabled && $listing->status == 'active' && $listing->post->post_status == 'publish') {
							$raise_up_link = strip_tags(apply_filters('w2dc_raiseup_option', __('raise up listing', 'W2DC-FSUBMIT'), $listing));
							echo '<a href="' . w2dc_dashboardUrl(array('w2dc_action' => 'raiseup_listing', 'listing_id' => $listing->post->ID)) . '" class="btn btn-primary btn-xs" title="' . $raise_up_link . '"><span class="glyphicon glyphicon-arrow-up"></span></a>';
						}?>
						<?php
						if ($listing->status == 'expired') {
							$renew_link = strip_tags(apply_filters('w2dc_renew_option', __('renew listing', 'W2DC-FSUBMIT'), $listing));
							echo '<a href="' . w2dc_dashboardUrl(array('w2dc_action' => 'renew_listing', 'listing_id' => $listing->post->ID)) . '" class="btn btn-primary btn-xs" title="' . $renew_link . '"><span class="glyphicon glyphicon-refresh"></span></a>';
						}?>
						<?php do_action('w2dc_dashboard_listing_options', $listing); ?>
					</div>
					<?php endif; ?>
				</td>
			</tr>
		<?php endwhile; ?>
		</table>
		<?php renderPaginator($frontend_controller->query); ?>
		<?php endif; ?>