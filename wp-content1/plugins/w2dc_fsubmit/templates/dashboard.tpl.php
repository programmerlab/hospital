<?php w2dc_renderMessages(); ?>

<div class="w2dc_content">
	<?php w2dc_renderTemplate('frontend/frontpanel_buttons.tpl.php'); ?>

	<script>
		jQuery(document).ready(function() {
			jQuery(".w2dc_dashboard_tabs.nav-tabs li").click(function(e) {
				window.location = jQuery(this).find("a").attr("href");
			});
		});
	</script>
	<div class="dashboard_tabs_content">
		<ul class="w2dc_dashboard_tabs nav nav-tabs">
			<li <?php if ($frontend_controller->active_tab == 'listings') echo 'class="active"'; ?>><a href="<?php echo w2dc_dashboardUrl(); ?>"><?php _e('Listings', 'W2DC-FSUBMIT'); ?> (<?php echo $frontend_controller->listings_count; ?>)</a></li>
			<?php if (get_option('w2dc_allow_edit_profile')): ?>
			<li <?php if ($frontend_controller->active_tab == 'profile') echo 'class="active"'; ?>><a href="<?php echo w2dc_dashboardUrl(array('w2dc_action' => 'profile')); ?>"><?php _e('My profile', 'W2DC-FSUBMIT'); ?></a></li>
			<?php endif; ?>
			<?php do_action('w2dc_dashboard_links', $frontend_controller); ?>
		</ul>
	
		<div class="tab-content">
			<div class="tab-pane active">
				<?php w2dc_renderTemplate($frontend_controller->subtemplate, array('frontend_controller' => $frontend_controller)); ?>
			</div>
		</div>
	</div>
</div>