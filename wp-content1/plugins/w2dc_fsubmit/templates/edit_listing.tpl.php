<?php w2dc_renderMessages(); ?>

<div class="w2dc_content">
	<h2><?php echo sprintf(__('Edit listing "%s"', 'W2DC-FSUBMIT'), $w2dc_instance->current_listing->title()); ?></h2>

	<form class="form-inline" action="" method="POST">
		<input type="hidden" name="referer" value="<?php echo $frontend_controller->referer; ?>" />
		<div class="submit_section">
			<h3 class="submit_section_label"><?php _e('Listing title', 'W2DC-FSUBMIT'); ?><span class="red_asterisk">*</span></h3>
			<div class="submit_section_inside">
				<input type="text" name="post_title" style="width: 100%" class="form-control" value="<?php if ($w2dc_instance->current_listing->post->post_title != __('Auto Draft')) echo esc_attr($w2dc_instance->current_listing->post->post_title); ?>" />
			</div>
		</div>
	
		<div class="submit_section">
			<h3 class="submit_section_label"><?php echo $w2dc_instance->content_fields->getContentFieldBySlug('content')->name; ?><?php if ($w2dc_instance->content_fields->getContentFieldBySlug('content')->is_required): ?><span class="red_asterisk">*</span><?php endif; ?></h3>
			<div class="submit_section_inside">
				<?php wp_editor($w2dc_instance->current_listing->post->post_content, 'post_content', array('media_buttons' => false, 'editor_class' => 'editor_class')); ?>
			</div>
		</div>

		<div class="submit_section">
			<h3 class="submit_section_label"><?php echo $w2dc_instance->content_fields->getContentFieldBySlug('summary')->name; ?><?php if ($w2dc_instance->content_fields->getContentFieldBySlug('summary')->is_required): ?><span class="red_asterisk">*</span><?php endif; ?></h3>
			<div class="submit_section_inside">
				<textarea name="post_excerpt" class="editor_class form-control" rows="4"><?php echo esc_textarea($w2dc_instance->current_listing->post->post_excerpt)?></textarea>
			</div>
		</div>
		
		<?php do_action('w2dc_edit_listing_metaboxes_pre', $w2dc_instance->current_listing); ?>

		<?php if (!$w2dc_instance->current_listing->level->eternal_active_period && get_option('w2dc_change_expiration_date')): ?>
		<div class="submit_section">
			<h3 class="submit_section_label"><?php _e('Listing expiration date', 'W2DC-FSUBMIT'); ?></h3>
			<div class="submit_section_inside">
				<?php $w2dc_instance->listings_manager->listingExpirationDateMetabox($w2dc_instance->current_listing->post); ?>
			</div>
		</div>
		<?php endif; ?>
	
		<?php if ($w2dc_instance->current_listing->level->categories_number > 0 || $w2dc_instance->current_listing->level->unlimited_categories): ?>
		<div class="submit_section">
			<h3 class="submit_section_label"><?php echo $w2dc_instance->content_fields->getContentFieldBySlug('categories_list')->name; ?></h3>
			<div class="submit_section_inside">
				<div class="tabs-panel editor_class" id="<?php echo W2DC_CATEGORIES_TAX; ?>-all">
					<?php w2dc_terms_checklist($w2dc_instance->current_listing->post->ID); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	
		<?php if ($w2dc_instance->content_fields->isNotCoreContentFields()): ?>
		<div class="submit_section">
			<div class="submit_section_inside">
				<?php $w2dc_instance->content_fields_manager->contentFieldsMetabox($w2dc_instance->current_listing->post); ?>
			</div>
		</div>
		<?php endif; ?>
	
		<?php if ($w2dc_instance->current_listing->level->images_number > 0 || $w2dc_instance->current_listing->level->videos_number > 0): ?>
		<div class="submit_section">
			<div class="submit_section_inside">
				<?php $w2dc_instance->media_manager->mediaMetabox(); ?>
			</div>
		</div>
		<?php endif; ?>
	
		<?php if ($w2dc_instance->current_listing->level->locations_number > 0): ?>
		<div class="submit_section">
			<h3 class="submit_section_label"><?php _e('Listing locations', 'W2DC-FSUBMIT'); ?></h3>
			<div class="submit_section_inside">
				<?php $w2dc_instance->locations_manager->listingLocationsMetabox($w2dc_instance->current_listing->post); ?>
			</div>
		</div>
		<?php endif; ?>
		
		<?php do_action('w2dc_edit_listing_metaboxes_post', $w2dc_instance->current_listing); ?>

		<?php require_once(ABSPATH . 'wp-admin/includes/template.php'); ?>
		<?php submit_button(__('Save changes', 'W2DC-FSUBMIT'), 'btn btn-primary', 'submit', false); ?>
		&nbsp;&nbsp;&nbsp;
		<?php submit_button(__('Cancel', 'W2DC-FSUBMIT'), 'btn btn-primary', 'cancel', false); ?>
	</form>
</div>