<?php
/*
Plugin Name: Web 2.0 Directory payments
Plugin URI: http://www.salephpscripts.com/wordpress_directory/
Description: Includes payments processing and invoices management functionality into directory/classifieds website
Version: 2.0.2
Author: Mihail Chepovskiy
Author URI: http://www.salephpscripts.com
License: commercial
*/

define('W2DC_INVOICE_TYPE', 'w2dc_invoice');

define('W2DC_PAYMENTS_PATH', plugin_dir_path(__FILE__));

function w2dc_payments_loadPaths() {
	define('W2DC_PAYMENTS_TEMPLATES_PATH',  W2DC_PAYMENTS_PATH . 'templates/');

	if (!defined('W2DC_THEME_MODE'))
		define('W2DC_PAYMENTS_RESOURCES_URL', plugins_url('/', __FILE__) . 'resources/');
}
add_action('init', 'w2dc_payments_loadPaths', 0);

include_once W2DC_PAYMENTS_PATH . 'classes/invoice.php';

class w2dc_payments_plugin {
	
	public function __construct() {
		register_activation_hook(__FILE__, array($this, 'activation'));
	}
	
	public function activation() {
		include_once(ABSPATH . 'wp-admin/includes/plugin.php');
		if (!defined('W2DC_VERSION') && version_compare(W2DC_VERSION, '1.2.0', '<=')) {
			deactivate_plugins(basename(__FILE__)); // Deactivate ourself
			wp_die("Web 2.0 Web 2.0 Directory plugin v1.2.0 or greater required.");
		}
	}

	public function init() {
		global $w2dc_instance;
		
		if (!get_option('w2dc_installed_payments'))
			w2dc_install_payments();
		add_action('w2dc_version_upgrade', 'w2dc_upgrade_payments');
		
		add_action('plugins_loaded', array($this, 'load_textdomain'), 0);

		add_action('init', array($this, 'register_invoice_type'));
		add_action('load-post-new.php', array($this, 'disable_new_invoices_page'));
		// remove links on all pages - 2 hooks needed
		add_action('admin_menu', array($this, 'disable_new_invoices_link'));
		add_action('admin_head', array($this, 'disable_new_invoices_link'));

		add_action('w2dc_admin_settings_sections', array($this, 'add_payments_settings_tab'));
		add_action('admin_init', array($this, 'register_settings'));
		
		add_filter('manage_'.W2DC_INVOICE_TYPE.'_posts_columns', array($this, 'add_invoices_table_columns'));
		add_filter('manage_'.W2DC_INVOICE_TYPE.'_posts_custom_column', array($this, 'manage_invoices_table_rows'), 10, 2);
		add_filter('post_row_actions', array($this, 'remove_row_actions'), 10, 2);
		add_action('wp_before_admin_bar_render', array($this, 'remove_create_invoice_link'));

		add_action('admin_init', array($this, 'remove_metaboxes'));
		add_action('add_meta_boxes', array($this, 'add_invoice_info_metabox'));
		add_action('add_meta_boxes', array($this, 'add_invoice_payment_metabox'));
		add_action('add_meta_boxes', array($this, 'add_invoice_log_metabox'));
		add_action('add_meta_boxes', array($this, 'add_invoice_actions_metabox'));

		add_filter('template_include', array($this, 'print_invoice_template'));
		
		$this->loadPricesByLevels();
		add_filter('w2dc_levels_loading', array($this, 'loadPricesByLevels'), 10, 2);

		add_filter('w2dc_level_html', array($this, 'levels_price_in_level_html'));
		add_filter('w2dc_level_validation', array($this, 'levels_price_in_level_validation'));
		add_filter('w2dc_level_create_edit_args', array($this, 'levels_price_in_level_create_add'), 1, 2);
		add_filter('w2dc_level_table_header', array($this, 'levels_price_table_header'));
		add_filter('w2dc_level_table_row', array($this, 'levels_price_table_row'), 10, 2);

		add_action('w2dc_submitlisting_levels_th', array($this, 'levels_price_front_table_header'), 10, 2);
		add_action('w2dc_submitlisting_levels_rows', array($this, 'levels_price_front_table_row'), 10, 3);
		
		add_filter('w2dc_level_upgrade_meta', array($this, 'levels_upgrade_meta'), 10, 2);
		add_action('w2dc_upgrade_meta_html', array($this, 'levels_upgrade_meta_html'), 10, 2);
		
		add_filter('w2dc_create_listings_steps_html', array($this, 'pay_invoice_step'), 10, 2);

		add_filter('w2dc_create_option', array($this, 'create_price'), 10, 2);
		add_filter('w2dc_raiseup_option', array($this, 'raiseup_price'), 10, 2);
		add_filter('w2dc_renew_option', array($this, 'renew_price'), 10, 2);
		add_filter('w2dc_level_upgrade_option', array($this, 'upgrade_price'), 10, 3);
		
		add_action('admin_init', array($this, 'invoice_actions'));
		add_action('get_header', array($this, 'invoice_actions'));
		
		add_filter('query_vars', array($this, 'w2dc_payments_query_vars'));
		
		// This is really strange thing, that users may see ANY attachments (including invoices) owned by other users, so we need this hack
		add_filter('pre_get_posts', array($this, 'prevent_users_see_other_invoices'));
		
		add_filter('bulk_actions-edit-'.W2DC_INVOICE_TYPE, array($this, 'remove_bulk_actions'));
		
		add_action('w2dc_dashboard_links', array($this, 'add_invoices_dashboard_link'));
		add_filter('w2dc_frontend_controller_construct', array($this, 'handle_dashboard_controller'));
		add_filter('edit_objects_links', array($this, 'edit_invoices_links'), 10, 3);

		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_styles'));
		add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts_styles'));
	}

	public function load_textdomain() {
		load_plugin_textdomain('W2DC-PAYMENTS', '', dirname(plugin_basename( __FILE__ )) . '/languages');
	}
	
	public function add_invoices_table_columns($columns) {
		$w2dc_columns['item'] = __('Item', 'W2DC-PAYMENTS');
		$w2dc_columns['price'] = __('Price', 'W2DC-PAYMENTS');
		$w2dc_columns['payment'] = __('Payment', 'W2DC-PAYMENTS');

		$columns['title'] = __('Invoice', 'W2DC-PAYMENTS');
		
		unset($columns['cb']);

		return array_slice($columns, 0, 1, true) + $w2dc_columns + array_slice($columns, 1, count($columns)-1, true);
	}
	
	public function manage_invoices_table_rows($column, $invoice_id) {
		switch ($column) {
			case "item":
				if ($invoice = getInvoiceByID($invoice_id))
					echo $invoice->item_object->getItemLink();
				break;
			case "price":
				if ($invoice = getInvoiceByID($invoice_id))
					echo $invoice->price();
				break;
			case "payment":
				if ($invoice = getInvoiceByID($invoice_id))
					if ($invoice->status == 'unpaid') {
						echo '<span class="w2dc_badge invoice_status_unpaid">' . __('unpaid', 'W2DC') . '</span>';
						if (current_user_can('edit_post', $invoice->post->ID))
							echo '<br /><a href="' . get_edit_post_link($invoice_id) . '"><img src="' . W2DC_PAYMENTS_RESOURCES_URL . 'images/money_add.png' . '" class="w2dc_field_icon" />' . __('pay invoice', 'W2DC-PAYMENTS') . '</a>';
					} elseif ($invoice->status == 'paid') {
						echo '<span class="w2dc_badge invoice_status_paid">' . __('paid', 'W2DC') . '</span>';
						if ($invoice->gateway)
							echo '<br /><b>' . gatewayName($invoice->gateway) . '</b>';
					} elseif ($invoice->status == 'pending') {
						echo '<span class="w2dc_badge invoice_status_pending">' . __('pending', 'W2DC') . '</span>';
						if ($invoice->gateway)
							echo '<br /><b>' . gatewayName($invoice->gateway) . '</b>';
					}
				break;
		}
	}
	
	public function remove_row_actions($actions, $post) {
		if ($post->post_type == W2DC_INVOICE_TYPE) {
			unset($actions['inline hide-if-no-js']);
			unset($actions['view']);
			//unset($actions['trash']);
		}
		return $actions;
	}
	
	public function remove_create_invoice_link() {
		global $wp_admin_bar;

		$wp_admin_bar->remove_menu('new-w2dc_invoice');
	}
	
	public function remove_metaboxes() {
		remove_meta_box('submitdiv', W2DC_INVOICE_TYPE, 'side');
		remove_meta_box('slugdiv', W2DC_INVOICE_TYPE, 'normal');
		remove_meta_box('authordiv', W2DC_INVOICE_TYPE, 'normal');
	}
	
	public function add_invoice_info_metabox($post_type) {
		if ($post_type == W2DC_INVOICE_TYPE) {
			add_meta_box('w2dc_invoice_info',
					__('Invoice Info', 'W2DC'),
					array($this, 'invoice_info_metabox'),
					W2DC_INVOICE_TYPE,
					'normal',
					'high');
		}
	}
	
	public function invoice_info_metabox($post) {
		$invoice = getInvoiceByID($post->ID);
		w2dc_renderTemplate(array(W2DC_PAYMENTS_TEMPLATES_PATH, 'info_metabox.tpl.php'), array('invoice' => $invoice));
	}
	
	public function add_invoice_log_metabox($post_type) {
		global $post;

		if ($post_type == W2DC_INVOICE_TYPE) {
			if ($post && ($invoice = getInvoiceByID($post->ID)) && $invoice->log) {
				add_meta_box('w2dc_invoice_log',
						__('Invoice Log', 'W2DC'),
						array($this, 'invoice_log_metabox'),
						W2DC_INVOICE_TYPE,
						'normal',
						'high');
			}
		}
	}
	
	public function invoice_log_metabox($post) {
		$invoice = getInvoiceByID($post->ID);
		w2dc_renderTemplate(array(W2DC_PAYMENTS_TEMPLATES_PATH, 'log_metabox.tpl.php'), array('invoice' => $invoice));
	}

	public function add_invoice_payment_metabox($post_type) {
		global $post;

		if ($post_type == W2DC_INVOICE_TYPE) {
			if (get_option('w2dc_paypal_email') || get_option('w2dc_allow_bank')) {
				if ($post && ($invoice = getInvoiceByID($post->ID)) && $invoice->status == 'unpaid' && !$invoice->gateway) {
					add_meta_box('w2dc_invoice_payment',
							__('Invoice Payment - choose payment gateway', 'W2DC'),
							array($this, 'invoice_payment_metabox'),
							W2DC_INVOICE_TYPE,
							'normal',
							'high');
				}
			}
		}
	}
	
	public function invoice_payment_metabox($post) {
		$invoice = getInvoiceByID($post->ID);
		
		$paypal = new w2dc_paypal();
		$paypal_subscription = new w2dc_paypal_subscription();
		$bank_transfer = new w2dc_bank_transfer();
		$stripe = new w2dc_stripe();
		
		w2dc_renderTemplate(array(W2DC_PAYMENTS_TEMPLATES_PATH, 'payment_metabox.tpl.php'), array('invoice' => $invoice, 'paypal' => $paypal, 'paypal_subscription' => $paypal_subscription, 'bank_transfer' => $bank_transfer, 'stripe' => $stripe));
	}

	public function add_invoice_actions_metabox($post_type) {
		if ($post_type == W2DC_INVOICE_TYPE) {
			add_meta_box('w2dc_invoice_actions',
					__('Invoice actions', 'W2DC'),
					array($this, 'invoice_actions_metabox'),
					W2DC_INVOICE_TYPE,
					'side',
					'high');
		}
	}
	
	public function invoice_actions_metabox($post) {
		$invoice = getInvoiceByID($post->ID);
		w2dc_renderTemplate(array(W2DC_PAYMENTS_TEMPLATES_PATH, 'actions_metabox.tpl.php'), array('invoice' => $invoice));
	}

	public function add_payments_settings_tab($section) {
		echo '<a class="nav-tab ' . (($section == 'w2dc_payments_settings_page') ? 'nav-tab-active' : '' ) . '" href="' . admin_url('admin.php?page=w2dc_settings&section=w2dc_payments_settings_page') .'">' . __('Payments', 'W2DC') . '</a>';
	}
	
	public function register_settings() {
		add_settings_section(
				'w2dc_payments_section',
				__('General payments settings', 'W2DC'),
				null,
				'w2dc_payments_settings_page'
		);
		add_settings_field(
				'w2dc_payments_free_for_admins',
				__('Any services are Free for administrators', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_payments_free_for_admins_callback'),
				'w2dc_payments_settings_page',
				'w2dc_payments_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_payments_free_for_admins');
		add_settings_field(
				'w2dc_payments_currency',
				__('Currency', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_payments_currency_callback'),
				'w2dc_payments_settings_page',
				'w2dc_payments_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_payments_currency');
		add_settings_field(
				'w2dc_payments_symbol_code',
				__('Currency symbol or code', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_payments_symbol_code_callback'),
				'w2dc_payments_settings_page',
				'w2dc_payments_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_payments_symbol_code');
		add_settings_field(
				'w2dc_payments_symbol_position',
				__('Currency symbol or code position', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_payments_symbol_position_callback'),
				'w2dc_payments_settings_page',
				'w2dc_payments_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_payments_symbol_position');
		add_settings_field(
				'w2dc_payments_decimal_separator',
				__('Decimal separator', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_payments_decimal_separator_callback'),
				'w2dc_payments_settings_page',
				'w2dc_payments_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_payments_decimal_separator');
		add_settings_field(
				'w2dc_payments_thousands_separator',
				__('Thousands separator', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_payments_thousands_separator_callback'),
				'w2dc_payments_settings_page',
				'w2dc_payments_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_payments_thousands_separator');
		
		add_settings_section(
				'w2dc_bank_section',
				__('Bank transfer settings', 'W2DC'),
				null,
				'w2dc_payments_settings_page'
		);
		add_settings_field(
				'w2dc_allow_bank',
				__('Allow bank transfer', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_allow_bank_callback'),
				'w2dc_payments_settings_page',
				'w2dc_bank_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_allow_bank');
		add_settings_field(
				'w2dc_bank_info',
				__('Bank transfer information', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_bank_info_callback'),
				'w2dc_payments_settings_page',
				'w2dc_bank_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_bank_info');

		add_settings_section(
				'w2dc_paypal_section',
				__('PayPal settings', 'W2DC'),
				null,
				'w2dc_payments_settings_page'
		);
		add_settings_field(
				'w2dc_paypal_email',
				__('Business email', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_paypal_email_callback'),
				'w2dc_payments_settings_page',
				'w2dc_paypal_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_paypal_email');
		add_settings_field(
				'w2dc_paypal_subscriptions',
				__('Allow subscriptions', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_paypal_subscriptions_callback'),
				'w2dc_payments_settings_page',
				'w2dc_paypal_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_paypal_subscriptions');
		add_settings_field(
				'w2dc_paypal_test',
				__('Test Sandbox mode', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_paypal_test_callback'),
				'w2dc_payments_settings_page',
				'w2dc_paypal_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_paypal_test');

		add_settings_section(
				'w2dc_stripe_section',
				__('Stripe settings', 'W2DC'),
				null,
				'w2dc_payments_settings_page'
		);
		add_settings_field(
				'w2dc_stripe_test_secret',
				__('Test secret key', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_stripe_test_secret_callback'),
				'w2dc_payments_settings_page',
				'w2dc_stripe_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_stripe_test_secret');
		add_settings_field(
				'w2dc_stripe_test_public',
				__('Test publishable key', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_stripe_test_public_callback'),
				'w2dc_payments_settings_page',
				'w2dc_stripe_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_stripe_test_public');
		add_settings_field(
				'w2dc_stripe_live_secret',
				__('Live secret key', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_stripe_live_secret_callback'),
				'w2dc_payments_settings_page',
				'w2dc_stripe_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_stripe_live_secret');
		add_settings_field(
				'w2dc_stripe_live_public',
				__('Live publishable key', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_stripe_live_public_callback'),
				'w2dc_payments_settings_page',
				'w2dc_stripe_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_stripe_live_public');
		add_settings_field(
				'w2dc_stripe_test',
				__('Test Sandbox mode', 'W2DC-PAYMENTS'),
				array($this, 'w2dc_stripe_test_callback'),
				'w2dc_payments_settings_page',
				'w2dc_stripe_section'
		);
		register_setting('w2dc_payments_settings_page', 'w2dc_stripe_test');
	}
	
	public function w2dc_payments_free_for_admins_callback() {
		echo '<input type="checkbox" id="w2dc_payments_free_for_admins" name="w2dc_payments_free_for_admins" value="1" ' . checked(get_option('w2dc_payments_free_for_admins'), 1, false) .' />';
	}
	
	public function w2dc_payments_currency_callback() {
		echo '<select name="w2dc_payments_currency">';
		echo '<option value="USD" ' . selected(get_option('w2dc_payments_currency'), 'USD', false) . '>' . __('US Dollars ($)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="EUR" ' . selected(get_option('w2dc_payments_currency'), 'EUR', false) . '>' . __('Euros (€)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="GBP" ' . selected(get_option('w2dc_payments_currency'), 'GBP', false) . '>' . __('Pounds Sterling (£)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="AUD" ' . selected(get_option('w2dc_payments_currency'), 'AUD', false) . '>' . __('Australian Dollars ($)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="BRL" ' . selected(get_option('w2dc_payments_currency'), 'BRL', false) . '>' . __('Brazilian Real (R$)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="CAD" ' . selected(get_option('w2dc_payments_currency'), 'CAD', false) . '>' . __('Canadian Dollars ($)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="CZK" ' . selected(get_option('w2dc_payments_currency'), 'CZK', false) . '>' . __('Czech Koruna (Kč)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="DKK" ' . selected(get_option('w2dc_payments_currency'), 'DKK', false) . '>' . __('Danish Krone (kr)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="HKD" ' . selected(get_option('w2dc_payments_currency'), 'HKD', false) . '>' . __('Hong Kong Dollar ($)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="HUF" ' . selected(get_option('w2dc_payments_currency'), 'HUF', false) . '>' . __('Hungarian Forint (Ft)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="ILS" ' . selected(get_option('w2dc_payments_currency'), 'ILS', false) . '>' . __('Israeli Shekel (₪)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="JPY" ' . selected(get_option('w2dc_payments_currency'), 'JPY', false) . '>' . __('Japanese Yen (¥)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="MYR" ' . selected(get_option('w2dc_payments_currency'), 'MYR', false) . '>' . __('Malaysian Ringgits (RM)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="MXN" ' . selected(get_option('w2dc_payments_currency'), 'MXN', false) . '>' . __('Mexican Peso ($)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="NZD" ' . selected(get_option('w2dc_payments_currency'), 'NZD', false) . '>' . __('New Zealand Dollar ($)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="NOK" ' . selected(get_option('w2dc_payments_currency'), 'NOK', false) . '>' . __('Norwegian Krone (kr)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="PHP" ' . selected(get_option('w2dc_payments_currency'), 'PHP', false) . '>' . __('Philippine Pesos (P)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="PLN" ' . selected(get_option('w2dc_payments_currency'), 'PLN', false) . '>' . __('Polish Zloty (zł)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="SGD" ' . selected(get_option('w2dc_payments_currency'), 'SGD', false) . '>' . __('Singapore Dollar ($)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="SEK" ' . selected(get_option('w2dc_payments_currency'), 'SEK', false) . '>' . __('Swedish Krona (kr)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="CHF" ' . selected(get_option('w2dc_payments_currency'), 'CHF', false) . '>' . __('Swiss Franc (Fr)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="TWD" ' . selected(get_option('w2dc_payments_currency'), 'TWD', false) . '>' . __('Taiwan New Dollar ($)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="THB" ' . selected(get_option('w2dc_payments_currency'), 'THB', false) . '>' . __('Thai Baht (฿)', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="TRY" ' . selected(get_option('w2dc_payments_currency'), 'TRY', false) . '>' . __('Turkish Lira (₤)', 'W2DC-PAYMENTS') . '</option>';
		echo '</select>';
	}
	
	public function w2dc_payments_symbol_code_callback() {
		echo '<input type="text" id="w2dc_payments_symbol_code" name="w2dc_payments_symbol_code" value="' . esc_attr(get_option('w2dc_payments_symbol_code')) . '" size="3" />';
	}
	
	public function w2dc_payments_symbol_position_callback() {
		echo '<label><input type="radio" class="w2dc_payments_symbol_position" name="w2dc_payments_symbol_position" value="1" ' . checked(get_option('w2dc_payments_symbol_position'), 1, false) .' /> $1.00</label>';
		echo '<br />';
		echo '<label><input type="radio" class="w2dc_payments_symbol_position" name="w2dc_payments_symbol_position" value="2" ' . checked(get_option('w2dc_payments_symbol_position'), 2, false) .' /> $ 1.00</label>';
		echo '<br />';
		echo '<label><input type="radio" class="w2dc_payments_symbol_position" name="w2dc_payments_symbol_position" value="3" ' . checked(get_option('w2dc_payments_symbol_position'), 3, false) .' /> 1.00$</label>';
		echo '<br />';
		echo '<label><input type="radio" class="w2dc_payments_symbol_position" name="w2dc_payments_symbol_position" value="4" ' . checked(get_option('w2dc_payments_symbol_position'), 4, false) .' /> 1.00 $</label>';
	}

	public function w2dc_payments_decimal_separator_callback() {
		echo '<select id="w2dc_payments_decimal_separator" name="w2dc_payments_decimal_separator">';
		echo '<option value="." ' . selected(get_option('w2dc_payments_decimal_separator'), '.', false) . '>' . __('dot', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="," ' . selected(get_option('w2dc_payments_decimal_separator'), ',', false) . '>' . __('comma', 'W2DC-PAYMENTS') . '</option>';
		echo '</select>';
	}
	
	public function w2dc_payments_thousands_separator_callback() {
		echo '<select id="w2dc_payments_thousands_separator" name="w2dc_payments_thousands_separator">';
		echo '<option value="" ' . selected(get_option('w2dc_payments_thousands_separator'), '', false) . '>' . __('no separator', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="." ' . selected(get_option('w2dc_payments_thousands_separator'), '.', false) . '>' . __('dot', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="," ' . selected(get_option('w2dc_payments_thousands_separator'), ',', false) . '>' . __('comma', 'W2DC-PAYMENTS') . '</option>';
		echo '<option value="space" ' . selected(get_option('w2dc_payments_thousands_separator'), 'space', false) . '>' . __('space', 'W2DC-PAYMENTS') . '</option>';
		echo '</select>';
	}
	
	public function w2dc_allow_bank_callback() {
		echo '<input type="checkbox" id="w2dc_allow_bank" name="w2dc_allow_bank" value="1" ' . checked(get_option('w2dc_allow_bank'), 1, false) .' />';
	}

	public function w2dc_bank_info_callback() {
		echo '<textarea id="w2dc_bank_info" name="w2dc_bank_info" cols="60" rows="3">' . get_option('w2dc_bank_info') .'</textarea>';
	}
	
	public function w2dc_paypal_email_callback() {
		global $wp_rewrite;
		if ($wp_rewrite->using_permalinks())
			echo '<input type="text" id="w2dc_paypal_email" name="w2dc_paypal_email" value="' . esc_attr(get_option('w2dc_paypal_email')) . '" size="30" />';
		else 
			echo '<b>' . __('It is required to use permalinks for PayPal payment gateway! Enable permalinks first!', 'W2DC-PAYMENTS') . '</b>';
	}

	public function w2dc_paypal_test_callback() {
		echo '<input type="checkbox" id="w2dc_paypal_test" name="w2dc_paypal_test" value="1" ' . checked(get_option('w2dc_paypal_test'), 1, false) .' />';
		echo '<p class="description">' . __('You must have a <a href="http://developer.paypal.com/" target="_blank">PayPal Sandbox</a> account setup before using this feature', 'W2DC-PAYMENTS') . '</p>';
	}

	public function w2dc_paypal_subscriptions_callback() {
		echo '<input type="checkbox" id="w2dc_paypal_subscriptions" name="w2dc_paypal_subscriptions" value="1" ' . checked(get_option('w2dc_paypal_subscriptions'), 1, false) .' />';
		echo '<p class="description">' . __('Only for listings with limited active period', 'W2DC-PAYMENTS') . '</p>';
	}
	
	public function w2dc_stripe_test_secret_callback() {
		if (extension_loaded('openssl'))
			echo '<input type="text" id="w2dc_stripe_test_secret" name="w2dc_stripe_test_secret" value="' . esc_attr(get_option('w2dc_stripe_test_secret')) . '" size="30" />';
		else
			echo '<b>' . __('Payment by Stripe requires installed OpenSSL extension!', 'W2DC-PAYMENTS') . '</b>';
	}

	public function w2dc_stripe_test_public_callback() {
		if (extension_loaded('openssl'))
			echo '<input type="text" id="w2dc_stripe_test_public" name="w2dc_stripe_test_public" value="' . esc_attr(get_option('w2dc_stripe_test_public')) . '" size="30" />';
		else
			echo '<b>' . __('Payment by Stripe requires installed OpenSSL extension!', 'W2DC-PAYMENTS') . '</b>';
	}

	public function w2dc_stripe_live_secret_callback() {
		if (extension_loaded('openssl'))
			echo '<input type="text" id="w2dc_stripe_live_secret" name="w2dc_stripe_live_secret" value="' . esc_attr(get_option('w2dc_stripe_live_secret')) . '" size="30" />';
		else
			echo '<b>' . __('Payment by Stripe requires installed OpenSSL extension!', 'W2DC-PAYMENTS') . '</b>';
	}

	public function w2dc_stripe_live_public_callback() {
		if (extension_loaded('openssl'))
			echo '<input type="text" id="w2dc_stripe_live_public" name="w2dc_stripe_live_public" value="' . esc_attr(get_option('w2dc_stripe_live_public')) . '" size="30" />';
		else
			echo '<b>' . __('Payment by Stripe requires installed OpenSSL extension!', 'W2DC-PAYMENTS') . '</b>';
	}
	
	public function w2dc_stripe_test_callback() {
		echo '<input type="checkbox" id="w2dc_stripe_test" name="w2dc_stripe_test" value="1" ' . checked(get_option('w2dc_stripe_test'), 1, false) .' />';
		echo '<p class="description">' . __('You can only use <a href="http://stripe.com/" target="_blank">Stripe</a> in test mode until you activate your account.', 'W2DC-PAYMENTS') . '</p>';
	}

	public function register_invoice_type() {
		$args = array(
			'labels' => array(
				'name' => __('Directory invoices', 'W2DC-PAYMENTS'),
				'singular_name' => __('Directory invoice', 'W2DC-PAYMENTS'),
				'edit_item' => __('View Invoice', 'W2DC-PAYMENTS'),
				'search_items' => __('Search invoices', 'W2DC-PAYMENTS'),
				'not_found' =>  __('No invoices found', 'W2DC-PAYMENTS'),
				'not_found_in_trash' => __('No invoices found in trash', 'W2DC-PAYMENTS')
			),
			'has_archive' => true,
			'description' => __('Directory invoices', 'W2DC-PAYMENTS'),
			'show_ui' => true,
			'supports' => array('author'),
			'menu_icon' => W2DC_PAYMENTS_RESOURCES_URL . 'images/dollar.png',
		);
		register_post_type(W2DC_INVOICE_TYPE, $args);
	}
	
	function disable_new_invoices_page() {
		if (isset($_GET['post_type']) && $_GET['post_type'] == W2DC_INVOICE_TYPE)
			wp_die("You ain't allowed to do that!");
	}
	function disable_new_invoices_link() {
		global $submenu;
		unset($submenu['edit.php?post_type=' . W2DC_INVOICE_TYPE][10]);

		if (function_exists('get_current_screen')) {
			$screen = get_current_screen();
			if ($screen && $screen->post_type == W2DC_INVOICE_TYPE)
				echo '<style type="text/css">.add-new-h2 { display:none; }</style>';
		}
	}
	
	public function loadPricesByLevels($level = null, $array = array()) {
		global $w2dc_instance, $wpdb;

		if (!$array) {
			$array = $wpdb->get_results("SELECT * FROM {$wpdb->levels} ORDER BY order_num", ARRAY_A);

			foreach ($array AS $row) {
				$w2dc_instance->levels->levels_array[$row['id']]->price = $row['price'];
				$w2dc_instance->levels->levels_array[$row['id']]->raiseup_price = $row['raiseup_price'];
				
				if (is_object($level) && $level->id == $row['id']) {
					$level->price = $row['price'];
					$level->raiseup_price = $row['raiseup_price'];
				}
			}
		} else {
			$level->price = $array['price'];
			$level->raiseup_price = $array['raiseup_price'];
		}
		
		return $level;
	}
	
	public function levels_price_in_level_html($level) {
		w2dc_renderTemplate(array(W2DC_PAYMENTS_PATH, 'templates/levels_price_in_level.tpl.php'), array('level' => $level));
	}
	
	public function levels_price_in_level_validation($validation) {
		$validation->set_rules('price', __('Listings price', 'W2DC-PAYMENTS'), 'is_numeric');
		$validation->set_rules('raiseup_price', __('Listings raise up price', 'W2DC-PAYMENTS'), 'is_numeric');
		
		return $validation;
	}
	
	public function levels_price_in_level_create_add($insert_update_args, $array) {
		$insert_update_args['price'] = w2dc_getValue($array, 'price', 0);
		$insert_update_args['raiseup_price'] = w2dc_getValue($array, 'raiseup_price', 0);
		return $insert_update_args;
	}
	
	public function levels_price_table_header($columns) {
		$w2dc_columns['price'] = __('Price', 'W2DC-PAYMENTS');
		
		return array_slice($columns, 0, 2, true) + $w2dc_columns + array_slice($columns, 2, count($columns)-2, true);
	}

	public function levels_price_table_row($items_array, $level) {
		$w2dc_columns['price'] = formatPrice($level->price);
		
		return array_slice($items_array, 0, 1, true) + $w2dc_columns + array_slice($items_array, 1, count($items_array)-1, true);
	}
	
	public function levels_price_front_table_header($pre, $post) {
		echo $pre . __('Price', 'W2DC-PAYMENTS') . $post;
	}

	public function levels_price_front_table_row($level, $pre, $post) {
		if ($level->price == 0)
			$out = '<span class="w2dc_price w2dc_payments_free">' . __('FREE', 'W2DC-PAYMENTS') . '</span>';
		else {
			$thousands_separator = get_option('w2dc_payments_thousands_separator');
			if ($thousands_separator == 'space')
				$thousands_separator = ' ';

			$value = explode('.', number_format($level->price, 2, '.', $thousands_separator));
			$price = $value[0];
			$cents = $value[1];
			$out = $price . '<span class="w2dc_price_cents">' . $cents . '</span>';
			switch (get_option('w2dc_payments_symbol_position')) {
				case 1:
					$out = get_option('w2dc_payments_symbol_code') . $out;
					break;
				case 2:
					$out = get_option('w2dc_payments_symbol_code') . ' ' . $out;
					break;
				case 3:
					$out = $out . get_option('w2dc_payments_symbol_code');
					break;
				case 4:
					$out = $out . ' ' . get_option('w2dc_payments_symbol_code');
					break;
			}
			$out = '<span class="w2dc_price">' . $out . '</span>';
		}
		
		echo $pre . $out . $post;
	}
	
	public function levels_upgrade_meta($level, $upgrade_meta) {
		global $w2dc_instance;

		$results = array();
		foreach ($w2dc_instance->levels->levels_array AS $_level) {
			if (($price = w2dc_getValue($_POST, 'level_price_' . $level->id . '_' . $_level->id)) && is_numeric($price))
				$results[$_level->id]['price'] = $price;
			else
				$results[$_level->id]['price'] = 0;
		}

		foreach ($upgrade_meta AS $level_id=>$meta)
			if (isset($results[$level_id]))
				$upgrade_meta[$level_id] = $results[$level_id] + $upgrade_meta[$level_id];

		return $upgrade_meta;
	}
	
	public function levels_upgrade_meta_html($level1, $level2) {
		if (isset($level1->upgrade_meta[$level2->id]) && isset($level1->upgrade_meta[$level2->id]['price']))
			$price = $level1->upgrade_meta[$level2->id]['price'];
		else
			$price = 0;

		echo get_option('w2dc_payments_symbol_code') . '<input type="text" size="4" name="level_price_' . $level1->id . '_' . $level2->id . '" value="' . esc_attr($price) . '" /><br />';
	}
	
	public function pay_invoice_step($step, $level = null) {
		if ($level && recalcPrice($level->price)) {
			echo '<div class="adv_line"></div>';
			echo '<div class="adv_step">';
			echo '<div class="adv_circle">' . __('Step', 'W2DC-PAYMENTS') . $step++ . '</div>';
			echo __('Pay Invoice', 'W2DC-PAYMENTS');
			echo '</div>';
		}
		return $step++;
	}
	
	public function create_price($link_text, $listing) {
		return  $link_text .' - ' . formatPrice(recalcPrice($listing->level->price));
	}

	public function raiseup_price($link_text, $listing) {
		return  $link_text .' - ' . formatPrice(recalcPrice($listing->level->raiseup_price));
	}

	public function renew_price($link_text, $listing) {
		return  $link_text .' - ' . formatPrice(recalcPrice($listing->level->price));
	}

	public function upgrade_price($link_text, $old_level, $new_level) {
		return  $link_text .' - ' . formatPrice(recalcPrice($old_level->upgrade_meta[$new_level->id]['price']));
	}

	public function print_invoice_template($template) {
		global $w2dc_instance;

		if (is_page($w2dc_instance->index_page_id) && $w2dc_instance->action == 'w2dc_print_invoice' && isset($_GET['invoice_id']) && is_numeric($_GET['invoice_id'])) {
			if (current_user_can('edit_post', $_GET['invoice_id'])) {
				if (is_file(W2DC_PAYMENTS_TEMPLATES_PATH . 'invoice_print-custom.tpl.php'))
					$template = W2DC_PAYMENTS_TEMPLATES_PATH . 'invoice_print-custom.tpl.php';
				else
					$template = W2DC_PAYMENTS_TEMPLATES_PATH . 'invoice_print.tpl.php';
			} else
				wp_die('You are not able to access this page!');
		}
		return $template;
	}

	public function invoice_actions() {
		if (isset($_GET['post']) && is_numeric($_GET['post']) && current_user_can('edit_post', $_GET['post'])) {
			$invoice_id = $_GET['post'];
			if (($post = get_post($invoice_id)) && $post->post_type == W2DC_INVOICE_TYPE && ($invoice = getInvoiceByID($invoice_id))) {
				$redirect = false;
				if (isset($_GET['w2dc_gateway']) && !$invoice->gateway) {
					switch ($_GET['w2dc_gateway']) {
						case 'paypal':
							if (get_option('w2dc_paypal_email'))
								$gateway = $_GET['w2dc_gateway'];
							break;
						case 'paypal_subscription':
							if (get_option('w2dc_paypal_subscriptions') && $invoice->is_subscription)
								$gateway = $_GET['w2dc_gateway'];
							break;
						case 'stripe':
							if ((get_option('w2dc_stripe_test') && get_option('w2dc_stripe_test_secret') && get_option('w2dc_stripe_test_public')) || (get_option('w2dc_stripe_live_secret') && get_option('w2dc_stripe_live_public')))
								$gateway = $_GET['w2dc_gateway'];
							break;
						case 'bank_transfer':
							if (get_option('w2dc_allow_bank'))
								$gateway = $_GET['w2dc_gateway'];
							break;
					}
					if (isset($gateway)) {
						$invoice->setStatus('pending');
						$invoice->setGateway($gateway);

						$gateway = $invoice->getGatewayObject();
						$invoice->logMessage(sprintf(__('Payment gateway was selected: %s', 'W2DC-PAYMENTS'), $gateway->name()));
						w2dc_addMessage(__('Payment gateway was selected!', 'W2DC-PAYMENTS'));
						$gateway->submitPayment($invoice);
						$redirect = true;
					}
				}
	
				if (isset($_GET['invoice_action']) && $_GET['invoice_action'] == 'reset_gateway' && $invoice->gateway) {
					$invoice->setStatus('unpaid');
					$invoice->setGateway('');
					$invoice->logMessage(__('Payment gateway was reset', 'W2DC-PAYMENTS'));
					w2dc_addMessage(__('Payment gateway was reset!', 'W2DC-PAYMENTS'));
					$redirect = true;
				}
				
				if (isset($_GET['invoice_action']) && $_GET['invoice_action'] == 'set_paid' && $invoice->status != 'paid' && current_user_can('edit_others_posts')) {
					if ($invoice->item_object->complete()) {
						$invoice->setStatus('paid');
						$invoice->logMessage(__('Invoice was manually set as paid', 'W2DC-PAYMENTS'));
						w2dc_addMessage(__('Invoice was manually set as paid!', 'W2DC-PAYMENTS'));
					} else 
						w2dc_addMessage(__('An error has occured!', 'W2DC-PAYMENTS'), 'error');
					$redirect = true;
				}

				if ($redirect) {
					wp_redirect(get_edit_post_link($invoice_id, 'redirect'));
					exit;
				}
			}
		}
	}
	
	public function w2dc_payments_query_vars($vars) {
		$vars[] = 'ipn_token';
		$vars[] = 'gateway';

		return $vars;
	}
	
	public function prevent_users_see_other_invoices($wp_query) {
		global $current_user;
		if (is_admin() && !current_user_can('edit_others_posts') && isset($wp_query->query_vars['post_type']) && $wp_query->query_vars['post_type'] == W2DC_INVOICE_TYPE) {
			$wp_query->set('author', $current_user->ID);
			add_filter('views_edit-'.W2DC_INVOICE_TYPE, array($this, 'remove_invoices_counts'));
		}
	}
	public function remove_invoices_counts($views) {
		return array();
	}

	public function remove_bulk_actions($actions) {
		return array();
	}
	
	public function process_invoices_query($frontend_controller) {
		global $w2dc_instance;

		if ($w2dc_instance->action == 'invoices') {
			if (get_query_var('page'))
				$paged = get_query_var('page');
			elseif (get_query_var('paged'))
				$paged = get_query_var('paged');
			else
				$paged = 1;
		} else
			$paged = -1;

		$args = array(
				'post_type' => W2DC_INVOICE_TYPE,
				'author' => get_current_user_id(),
				'paged' => $paged,
				'posts_per_page' => 10,
		);
		$frontend_controller->invoices_query = new WP_Query($args);
		wp_reset_postdata();
	}
	
	public function add_invoices_dashboard_link($frontend_controller) {
		global $w2dc_instance;
		$this->process_invoices_query($frontend_controller);

		echo '<li ' . (($frontend_controller->active_tab == 'invoices') ? 'class="active"' : '') . '><a href="' . w2dc_dashboardUrl(array('w2dc_action' => 'invoices')) . '">' . __('Invoices', 'W2DC-PAYMENTS'). ' (' . $frontend_controller->invoices_query->found_posts . ')</a></li>';
	}
	
	public function handle_dashboard_controller($frontend_controller) {
		global $w2dc_instance;

		if (get_class($frontend_controller) == 'w2dc_dashboard_controller') {
			if (!is_user_logged_in())
				$this->template = W2DC_FSUBMIT_TEMPLATES_PATH . 'login_form.tpl.php';
			else {
				if ($w2dc_instance->action == 'invoices') {
					$this->process_invoices_query($frontend_controller);
		
					$frontend_controller->invoices = array();
					while ($frontend_controller->invoices_query->have_posts()) {
						$frontend_controller->invoices_query->the_post();
						
						$invoice = getInvoiceByID(get_the_ID());
						$frontend_controller->invoices[get_the_ID()] = $invoice;
					}
					// this is reset is really required after the loop ends
					wp_reset_postdata();
					
					$frontend_controller->template = W2DC_FSUBMIT_TEMPLATES_PATH . 'dashboard.tpl.php';
					$frontend_controller->subtemplate = W2DC_PAYMENTS_TEMPLATES_PATH . 'invoices_dashboard.tpl.php';
					$frontend_controller->active_tab = 'invoices';
				} elseif ($w2dc_instance->action == 'view_invoice' && isset($_GET['post']) && is_numeric($_GET['post']) && current_user_can('edit_post', $_GET['post'])) {
					if ($frontend_controller->invoice = getInvoiceByID($_GET['post'])) {
						$frontend_controller->paypal = new w2dc_paypal();
						$frontend_controller->paypal_subscription = new w2dc_paypal_subscription();
						$frontend_controller->bank_transfer = new w2dc_bank_transfer();
						$frontend_controller->stripe = new w2dc_stripe();
		
						$frontend_controller->template = W2DC_FSUBMIT_TEMPLATES_PATH . 'dashboard.tpl.php';
						$frontend_controller->subtemplate = W2DC_PAYMENTS_TEMPLATES_PATH . 'view_invoice_dashboard.tpl.php';
						$frontend_controller->active_tab = 'invoices';
					}
				}
			}
		}

		return $frontend_controller;
	}
	
	public function edit_invoices_links($url, $post_id, $dashboard_url) {
		if (($post = get_post($post_id)) && $post->post_type == W2DC_INVOICE_TYPE)
			return w2dc_dashboardUrl(array('w2dc_action' => 'view_invoice', 'post' => $post_id));
		
		return $url;
	}

	public function enqueue_scripts_styles() {
		wp_register_style('w2dc_payments', W2DC_PAYMENTS_RESOURCES_URL . 'css/payments.css');
		wp_enqueue_style('w2dc_payments');
		if (is_file(W2DC_PAYMENTS_RESOURCES_URL . 'css/payments-custom.css'))
			wp_register_style('w2dc_payments-custom', W2DC_PAYMENTS_RESOURCES_URL . 'css/payments-custom.css');

		wp_enqueue_style('w2dc_payments-custom');
	}
}

function recalcPrice($price) {
	// if any services are free for admins - show 0 price
	if (get_option('w2dc_payments_free_for_admins') && current_user_can('manage_options')) {
		return 0;
	} else
		return $price;
}

function formatPrice($value = 0) {
	if ($value == 0) {
		$out = '<span class="w2dc_payments_free">' . __('FREE', 'W2DC-PAYMENTS') . '</span>';
	} else {
		$decimal_separator = get_option('w2dc_payments_decimal_separator');

		$thousands_separator = get_option('w2dc_payments_thousands_separator');
		if ($thousands_separator == 'space')
			$thousands_separator = ' ';

		$value = number_format($value, 2, $decimal_separator, $thousands_separator); 
	
		switch (get_option('w2dc_payments_symbol_position')) {
			case 1:
				$out = get_option('w2dc_payments_symbol_code') . $value;
				break;
			case 2:
				$out = get_option('w2dc_payments_symbol_code') . ' ' . $value;
				break;
			case 3:
				$out = $value . get_option('w2dc_payments_symbol_code');
				break;
			case 4:
				$out = $value . ' ' . get_option('w2dc_payments_symbol_code');
				break;
		}
	}
	return $out;
}

function ipn_token() {
	return md5(site_url() . AUTH_SALT);
}

function w2dc_install_payments() {
	global $wpdb;

	// there may be possible bug in WP, on some servers it doesn't allow to execute more than one SQL query in one request
	$wpdb->query("ALTER TABLE {$wpdb->levels} ADD `price` FLOAT( 2 ) NOT NULL DEFAULT '0' AFTER `order_num`");
	if (array_search('price', $wpdb->get_col("DESC {$wpdb->levels}"))) {
		$wpdb->query("ALTER TABLE {$wpdb->levels} ADD `raiseup_price` FLOAT( 2 ) NOT NULL DEFAULT '0' AFTER `price`");
	
		update_option('w2dc_installed_payments', 1);
	
		update_option('w2dc_payments_free_for_admins', 1);
		update_option('w2dc_payments_currency', 'USD');
		update_option('w2dc_payments_symbol_code', '$');
		update_option('w2dc_payments_symbol_position', 1);
		update_option('w2dc_payments_decimal_separator', ',');
		update_option('w2dc_payments_thousands_separator', 'space');
		update_option('w2dc_allow_bank', 1);
		update_option('w2dc_bank_info', '');
		update_option('w2dc_paypal_email', '');
		update_option('w2dc_paypal_subscriptions', 1);
		update_option('w2dc_paypal_test', 0);
		
		w2dc_upgrade_payments('1.6.0');
	}
}

function w2dc_upgrade_payments($new_version) {
	if ($new_version == '1.6.0') {
		update_option('w2dc_stripe_test_secret', '');
		update_option('w2dc_stripe_test_public', '');
		update_option('w2dc_stripe_live_secret', '');
		update_option('w2dc_stripe_live_public', '');
		update_option('w2dc_stripe_test', 1);
	}
}

global $w2dc_payments_instance;

$w2dc_payments_instance = new w2dc_payments_plugin();
$w2dc_payments_instance->init();

?>
