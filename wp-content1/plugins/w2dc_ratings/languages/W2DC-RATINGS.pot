msgid ""
msgstr ""
"Project-Id-Version: w2dc esearch v1.0.0\n"
"POT-Creation-Date: 2014-08-21 10:17+0600\n"
"PO-Revision-Date: 2014-08-21 10:17+0600\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SearchPath-0: .\n"

#: templates/avg_rating.tpl.php:22
msgid "Only registered users can place ratings!"
msgstr ""

#: templates/ratings_metabox.tpl.php:5
msgid "Are you sure you want to flush all ratings of this listing?"
msgstr ""

#: templates/ratings_metabox.tpl.php:27
msgid "Average"
msgstr ""

#: templates/ratings_metabox.tpl.php:38
msgid "Star "
msgid_plural "Stars"
msgstr[0] ""
msgstr[1] ""

#: templates/ratings_metabox.tpl.php:49
msgid "Flush all ratings"
msgstr ""

#: templates/ratings_options_in_level.tpl.php:3 w2dc_ratings.php:192
msgid "Ratings"
msgstr ""

#: templates/single_rating.tpl.php:6
#, php-format
msgid "User rating: %d"
msgstr ""

#: w2dc_ratings.php:98
msgid "Ratings settings"
msgstr ""

#: w2dc_ratings.php:105
msgid "Only registered users may place ratings"
msgstr ""

#: w2dc_ratings.php:114
msgid "Show rating in map info window"
msgstr ""

#: w2dc_ratings.php:123
msgid "Allow users to flush ratings of own listings"
msgstr ""

#: w2dc_ratings.php:132
msgid "Allow sorting by ratings"
msgstr ""

#: w2dc_ratings.php:210 w2dc_ratings.php:234
msgid "Listing ratings"
msgstr ""

#: w2dc_ratings.php:252 w2dc_ratings.php:369 w2dc_ratings.php:398
#: w2dc_ratings.php:399
msgid "Rating"
msgstr ""
