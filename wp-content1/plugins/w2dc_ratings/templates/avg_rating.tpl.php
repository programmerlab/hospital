<div <?php if ($listing->avg_rating->ratings_count): ?>itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"<?php endif; ?>>
	<?php if ($active): ?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(function() {
				jQuery("#rater-<?php echo $listing->post->ID; ?>").rater({postHref: '<?php echo admin_url('admin-ajax.php?action=save_rating&post_id='.$listing->post->ID.'&_wpnonce='.wp_create_nonce('save_rating')); ?>'});
			});
		});
	</script>
	<?php endif; ?>
	
	<?php if ($listing->avg_rating->ratings_count): ?>
	<meta itemprop="ratingValue" content="<?php echo $listing->avg_rating->avg_value; ?>">
	<meta itemprop="bestRating" content="5">
	<meta itemprop="ratingCount" content="<?php echo $listing->avg_rating->ratings_count; ?>">
	<?php endif; ?>
	
	<div id="rater-<?php echo $listing->post->ID; ?>" class="stat">
		<div class="statVal">
			<nobr>
				<span class="ui-rater">
					<span class="ui-rater-starsOff" style="width:100px;" <?php if (get_option('w2dc_only_registered_users') && !get_current_user_id()) echo 'title="' . esc_attr__('Only registered users can place ratings!', 'W2DC-RATINGS') . '"'; ?>>
						<span class="ui-rater-starsOn" style="width: <?php echo $listing->avg_rating->avg_value*20; ?>px"></span>
					</span>
					<span class="ui-rater-avgvalue">
						<span class="ui-rater-rating"><?php echo $listing->avg_rating->avg_value; ?></span> (<span class="ui-rater-rateCount"><?php echo $listing->avg_rating->ratings_count; ?></span>)
					</span>
				</span>
			</nobr>
		</div>
	</div>
</div>