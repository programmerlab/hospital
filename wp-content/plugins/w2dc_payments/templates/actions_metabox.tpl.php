<div id="misc-publishing-actions">
	<div class="misc-pub-section">
		<script>
			var window_width = 860;
			var window_height = 800;
			var leftPosition, topPosition;
		   	leftPosition = (window.screen.width / 2) - ((window_width / 2) + 10);
		   	topPosition = (window.screen.height / 2) - ((window_height / 2) + 50);
		</script>
		<input type="button" onclick="window.open('<?php echo add_query_arg('invoice_id', $invoice->post->ID, w2dc_directoryUrl(array('w2dc_action' => 'w2dc_print_invoice'))); ?>', 'print_window', 'height='+window_height+',width='+window_width+',left='+leftPosition+',top='+topPosition+',menubar=yes,scrollbars=yes');" class="button button-primary" value="<?php _e('Print invoice', 'W2DC-PAYMENTS'); ?>" />
	</div>

	<?php if ($invoice->gateway): ?>
	<div class="misc-pub-section">
		<a class="button button-secondary" href="<?php echo add_query_arg('invoice_action', 'reset_gateway', get_edit_post_link($invoice->post->ID)); ?>"><?php _e('Reset gateway', 'W2DC-PAYMENTS'); ?></a>
	</div>
	<?php endif; ?>

	<?php if (current_user_can('edit_others_posts')): ?>
	<div class="misc-pub-section">
		<a class="button button-secondary" href="<?php echo add_query_arg('invoice_action', 'set_paid', get_edit_post_link($invoice->post->ID)); ?>"><?php _e('Set as paid', 'W2DC-PAYMENTS'); ?></a>
	</div>
	<?php endif; ?>
</div>