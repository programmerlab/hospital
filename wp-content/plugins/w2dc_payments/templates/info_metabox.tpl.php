<div id="misc-publishing-actions">
	<div class="misc-pub-section">
		<span>
			<b><?php echo $invoice->post->post_title; ?></b>
		</span>
	</div>
	<div class="misc-pub-section">
		<label><?php _e('Invoice ID', 'W2DC-PAYMENTS'); ?>:</label>
		<span>
			<b><?php echo $invoice->post->ID; ?></b>
		</span>
	</div>
	<?php if ($invoice->post->post_author != get_current_user_id()): ?>
	<div class="misc-pub-section">
		<label><?php _e('Author', 'W2DC-PAYMENTS'); ?>:</label>
		<span>
			<a href="<?php echo get_edit_user_link($invoice->post->post_author); ?>"><?php echo get_userdata($invoice->post->post_author)->user_login; ?></a>
		</span>
	</div>
	<?php endif; ?>
	<div class="misc-pub-section">
		<label><?php _e('Item', 'W2DC-PAYMENTS'); ?>:</label>
		<span>
			<?php echo $invoice->item_object->getItemLink(); ?>
		</span>
	</div>
	<?php if ($invoice->item_object->getItemOptions()): ?>
	<div class="misc-pub-section">
		<label><?php _e('Item options', 'W2DC-PAYMENTS'); ?>:</label>
		<span>
			<b><?php echo $invoice->item_object->getItemOptionsString(); ?></b>
		</span>
	</div>
	<?php endif; ?>
	<div class="misc-pub-section">
		<label><?php _e('Price', 'W2DC-PAYMENTS'); ?>:</label>
		<span>
			<b><?php echo $invoice->price(); ?></b>
		</span>
	</div>
	<div class="misc-pub-section">
		<label><?php _e('Status', 'W2DC-PAYMENTS'); ?>:</label>
		<span>
			<?php if ($invoice->status == 'unpaid')
				echo '<span class="w2dc_badge invoice_status_unpaid">' . __('unpaid', 'W2DC') . '</span>';
			elseif ($invoice->status == 'paid')
				echo '<span class="w2dc_badge invoice_status_paid">' . __('paid', 'W2DC') . '</span>';
			elseif ($invoice->status == 'pending')
				echo '<span class="w2dc_badge invoice_status_pending">' . __('pending', 'W2DC') . '</span>';
			?>
		</span>
	</div>
	<?php if ($invoice->gateway): ?>
	<div class="misc-pub-section">
		<label><?php _e('Gateway', 'W2DC-PAYMENTS'); ?>:</label>
		<span>
			<b><?php echo gatewayName($invoice->gateway); ?></b>
		</span>
	</div>
	<?php endif; ?>
</div>