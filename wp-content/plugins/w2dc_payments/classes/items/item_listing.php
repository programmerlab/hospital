<?php

class w2dc_item_listing {
	public $item_id;
	
	public function __construct($item_id) {
		$this->item_id = $item_id;
	}
	
	public function getItem() {
		$listing_id = $this->item_id;

		// adapted for WPML
		if (function_exists('icl_object_id')) {
			$listing_id = icl_object_id($listing_id, W2DC_POST_TYPE, true);
		}
		
		$listing = new w2dc_listing();
		if ($listing->loadListingFromPost($listing_id)) {
			return $listing;
		} else
			return false;
	}
	
	public function getItemLink() {
		if (($listing = $this->getItem()) && current_user_can('edit_post', $listing->post->ID))
			return edit_post_link($listing->post->post_title, '', '', $listing->post->ID);
		else
			return __('N/A', 'W2DC');
	}
	
	public function getItemOptions() {
		if ($listing = $this->getItem())
			return array('active_days' => $listing->level->active_days, 'active_months' => $listing->level->active_months, 'active_years' => $listing->level->active_years);
		else
			return __('N/A', 'W2DC');
	}
	
	public function getItemOptionsString() {
		if ($listing = $this->getItem())
			return __('Active period - ', 'W2DC-PAYMENTS') . $listing->level->getActivePeriodString();
		else
			return __('N/A', 'W2DC');
	}

	public function complete() {
		if ($listing = $this->getItem()) {
			return $listing->processActivate(false);
		}
	}
}

function getInvoicesByListingId($listing_id) {
	$listing = new w2dc_listing();
	if (!$listing->loadListingFromPost($listing_id))
		return false;

	$children = get_children(array('post_parent' => $listing_id, 'post_type' => W2DC_INVOICE_TYPE));

	$invoices = array();
	if (is_array($children) && count($children) > 0)
		foreach ($children as $child) {
		$invoice = new w2dc_invoice();
		$invoice->post = $child;
		$invoice->listing = $listing;
		$invoice->init();
		$invoices[] = $invoice;
	}
	return $invoices;
}

function getLastInvoiceByListingId($listing_id) {
	if ($invoices = getInvoicesByListingId($listing_id))
		return array_shift($invoices);
	else
		return false;
}

function w2dc_pay_invoice_link($listing) {
	if ($listing->status == 'unpaid') {
		if (($invoice = getLastInvoiceByListingId($listing->post->ID)) && current_user_can('edit_post', $invoice->post->ID))
			echo '<br /><a href="' . get_edit_post_link($invoice->post->ID) . '" title="' . esc_attr($invoice->post->post_title) . '"><img src="' . W2DC_PAYMENTS_RESOURCES_URL . 'images/money_add.png' . '" class="w2dc_field_icon" />' . __('pay invoice', 'W2DC-PAYMENTS') . '</a>';
	}
}
add_action('w2dc_listing_status_option', 'w2dc_pay_invoice_link');


function w2dc_create_new_listing_invoice($listing) {
	if (recalcPrice($listing->level->price) > 0) {
		$invoice_args = array(
				'item' => 'listing',
				'title' => sprintf(__('Invoice for activation of listing: %s', 'W2DC-PAYMENTS'), $listing->title()),
				'is_subscription' => ($listing->level->eternal_active_period) ? false : true,
				'price' => $listing->level->price,
				'item_id' => $listing->post->ID,
				'author_id' => $listing->post->post_author
		);
		if ($invoice_id = call_user_func_array('w2dc_create_invoice', $invoice_args)) {
			w2dc_addMessage(__('New invoice was created successfully, listing become active after payment', 'W2DC-PAYMENTS'));
			update_post_meta($listing->post->ID, '_listing_status', 'unpaid');

			if (is_user_logged_in() && current_user_can('edit_post', $invoice_id)) {
				wp_redirect(apply_filters('redirect_post_location', get_edit_post_link($invoice_id, 'url'), $invoice_id));
				exit;
			}
		}
	}
}
add_action('w2dc_listing_creation', 'w2dc_create_new_listing_invoice');
add_action('w2dc_listing_creation_front', 'w2dc_create_new_listing_invoice');

function w2dc_renew_listing_invoice($continue, $listing) {
	if (recalcPrice($listing->level->price) > 0) {
		$invoice_args = array(
				'item' => 'listing',
				'title' => sprintf(__('Invoice for renewal of listing: %s', 'W2DC-PAYMENTS'), $listing->title()),
				'is_subscription' => ($listing->level->eternal_active_period) ? false : true,
				'price' => $listing->level->price,
				'item_id' => $listing->post->ID,
				'author_id' => $listing->post->post_author
		);
		if (call_user_func_array('w2dc_create_invoice', $invoice_args)) {
			w2dc_addMessage(__('New invoice was created successfully, listing become active after payment', 'W2DC-PAYMENTS'));
			update_post_meta($listing->post->ID, '_listing_status', 'unpaid');
			return false;
		}
	} else
		return $continue;
}
add_filter('w2dc_listing_renew', 'w2dc_renew_listing_invoice', 10, 2);

?>