<?php
/*
Plugin Name: Web 2.0 Directory frontend submit
Plugin URI: http://www.salephpscripts.com/wordpress_directory/
Description: Allow users to submit new listings at the frontend side of your site, also provides users dashboard functionality.
Version: 2.0.3
Author: Mihail Chepovskiy
Author URI: http://www.salephpscripts.com
License: commercial
*/

define('W2DC_FSUBMIT_PATH', plugin_dir_path(__FILE__));

function w2dc_fsubmit_loadPaths() {
	define('W2DC_FSUBMIT_TEMPLATES_PATH', W2DC_FSUBMIT_PATH . 'templates/');

	if (!defined('W2DC_THEME_MODE')) {
		define('W2DC_FSUBMIT_RESOURCES_PATH', W2DC_FSUBMIT_PATH . 'resources/');
		define('W2DC_FSUBMIT_RESOURCES_URL', plugins_url('/', __FILE__) . 'resources/');
	}
}
add_action('init', 'w2dc_fsubmit_loadPaths', 0);

define('W2DC_FSUBMIT_SHORTCODE', 'webdirectory-submit');
define('W2DC_DASHBOARD_SHORTCODE', 'webdirectory-dashboard');

include_once W2DC_FSUBMIT_PATH . 'classes/dashboard_controller.php';
include_once W2DC_FSUBMIT_PATH . 'classes/submit_controller.php';

class w2dc_fsubmit_plugin {

	public function __construct() {
		register_activation_hook(__FILE__, array($this, 'activation'));
	}
	
	public function activation() {
		include_once(ABSPATH . 'wp-admin/includes/plugin.php');
		if (!defined('W2DC_VERSION') && version_compare(W2DC_VERSION, '1.1.2', '<=')) {
			deactivate_plugins(basename(__FILE__)); // Deactivate ourself
			wp_die("Web 2.0 Web 2.0 Directory plugin v1.1.2 or greater required.");
		}
	}

	public function init() {
		global $w2dc_instance, $w2dc_shortcodes_init;
		
		if (!get_option('w2dc_installed_fsubmit'))
			w2dc_install_fsubmit();
		add_action('w2dc_version_upgrade', 'w2dc_upgrade_fsubmit');
		
		add_action('plugins_loaded', array($this, 'load_textdomain'), 0);
		
		add_action('admin_init', array($this, 'register_settings'));

		// add new shortcodes for frontend submission and dashboard
		$w2dc_shortcodes_init['webdirectory-submit'] = 'w2dc_submit_controller';
		$w2dc_shortcodes_init['webdirectory-dashboard'] = 'w2dc_dashboard_controller';
		add_shortcode('webdirectory-submit', array($w2dc_instance, 'renderShortcode'));
		add_shortcode('webdirectory-dashboard', array($w2dc_instance, 'renderShortcode'));
		
		add_action('init', array($this, 'getSubmitPage'), 0);
		add_action('init', array($this, 'getDasboardPage'), 0);

		// this is special wrapper for native 'get_edit_post_link' hook,
		// allows to rewrite urls of listings in invoices to see them in dashboard
		add_filter('get_edit_post_link', array($this, 'get_edit_post_link'), 10, 2);
		add_filter('edit_objects_links', array($this, 'edit_listings_links'), 10, 3);

		add_action('w2dc_directory_frontpanel', array($this, 'add_submit_button'));
		//add_action('w2dc_directory_frontpanel', array($this, 'add_claim_button'));
		
		add_action('w2dc_directory_frontpanel', array($this, 'add_logout_button'));

		add_action('after_setup_theme', array($this, 'remove_admin_bar'));
		add_action('admin_init', array($this, 'restrict_dashboard'));
		
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_styles'));
		add_action('wp_print_styles', array($this, 'enqueue_frontend_style'), 10000);
	}

	public function load_textdomain() {
		load_plugin_textdomain('W2DC-FSUBMIT', '', dirname(plugin_basename( __FILE__ )) . '/languages');
	}
	
	public function register_settings() {
		add_settings_section(
				'w2dc_fsubmit_section',
				__('Frontend submission and dashboard', 'W2DC'),
				null,
				'w2dc_settings_page'
		);

		add_settings_field(
				'w2dc_fsubmit_login_mode',
				__('Select frontend submission login mode', 'W2DC-FSUBMIT'),
				array($this, 'w2dc_fsubmit_login_mode_callback'),
				'w2dc_settings_page',
				'w2dc_fsubmit_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_fsubmit_login_mode');
		add_settings_field(
				'w2dc_fsubmit_default_status',
				__('Post status after frontend submit', 'W2DC-FSUBMIT'),
				array($this, 'w2dc_fsubmit_default_status_callback'),
				'w2dc_settings_page',
				'w2dc_fsubmit_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_fsubmit_default_status');
		add_settings_field(
				'w2dc_fsubmit_edit_status',
				__('Post status after listing was modified', 'W2DC-FSUBMIT'),
				array($this, 'w2dc_fsubmit_edit_status_callback'),
				'w2dc_settings_page',
				'w2dc_fsubmit_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_fsubmit_edit_status');
		add_settings_field(
				'w2dc_fsubmit_button',
				__('Enable submit listing button', 'W2DC-FSUBMIT'),
				array($this, 'w2dc_fsubmit_button_callback'),
				'w2dc_settings_page',
				'w2dc_fsubmit_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_fsubmit_button');
		add_settings_field(
				'w2dc_hide_admin_bar',
				__('Hide top admin bar at the frontend for regular users and do not allow them to see dashboard at all', 'W2DC-FSUBMIT'),
				array($this, 'w2dc_hide_admin_bar_callback'),
				'w2dc_settings_page',
				'w2dc_fsubmit_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_hide_admin_bar');
		add_settings_field(
				'w2dc_allow_edit_profile',
				__('Allow users to manage own profile at the frontend dashboard', 'W2DC-FSUBMIT'),
				array($this, 'w2dc_allow_edit_profile_callback'),
				'w2dc_settings_page',
				'w2dc_fsubmit_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_allow_edit_profile');
		
		add_settings_field(
				'w2dc_newuser_notification',
				__('Registration of new user notification', 'W2DC'),
				array($this, 'w2dc_newuser_notification_callback'),
				'w2dc_notifications_settings_page',
				'w2dc_notifications_section'
		);
		register_setting('w2dc_notifications_settings_page', 'w2dc_newuser_notification');
		
		// adapted for WPML
		if (function_exists('icl_object_id')) {
			add_settings_field(
					'w2dc_enable_translations',
					__('Enable frontend translations management', 'W2DC-FSUBMIT'),
					array($this, 'w2dc_enable_frontend_translations_callback'),
					'w2dc_advanced_settings_page',
					'w2dc_wpml_section'
			);
			register_setting('w2dc_advanced_settings_page', 'w2dc_enable_frontend_translations');
		}
	}
	
	public function w2dc_fsubmit_login_mode_callback() {
		echo '<label><input type="radio" class="w2dc_fsubmit_login_mode" name="w2dc_fsubmit_login_mode" value="1" ' . checked(get_option('w2dc_fsubmit_login_mode'), 1, false) .' /> ' . __('login required', 'W2DC-FSUBMIT') . '</label>';
		echo '<br />';
		echo '<label><input type="radio" class="w2dc_fsubmit_login_mode" name="w2dc_fsubmit_login_mode" value="2" ' . checked(get_option('w2dc_fsubmit_login_mode'), 2, false) .' /> ' . __('necessary to fill in contact form', 'W2DC-FSUBMIT') . '</label>';
		echo '<br />';
		echo '<label><input type="radio" class="w2dc_fsubmit_login_mode" name="w2dc_fsubmit_login_mode" value="3" ' . checked(get_option('w2dc_fsubmit_login_mode'), 3, false) .' /> ' . __('not necessary to fill in contact form', 'W2DC-FSUBMIT') . '</label>';
		echo '<br />';
		echo '<label><input type="radio" class="w2dc_fsubmit_login_mode" name="w2dc_fsubmit_login_mode" value="4" ' . checked(get_option('w2dc_fsubmit_login_mode'), 4, false) .' /> ' . __('do not even show contact form', 'W2DC-FSUBMIT') . '</label>';
	}

	public function w2dc_fsubmit_default_status_callback() {
		echo '<select name="w2dc_fsubmit_default_status">';
		echo '<option value="1" ' . selected(get_option('w2dc_fsubmit_default_status'), 1, false) . '>' . __('Pending Review', 'W2DC-FSUBMIT') . '</option>';
		echo '<option value="2" ' . selected(get_option('w2dc_fsubmit_default_status'), 2, false) . '>' . __('Draft', 'W2DC-FSUBMIT') . '</option>';
		echo '<option value="3" ' . selected(get_option('w2dc_fsubmit_default_status'), 3, false) . '>' . __('Published', 'W2DC-FSUBMIT') . '</option>';
		echo '</select>';
	}

	public function w2dc_fsubmit_edit_status_callback() {
		echo '<select name="w2dc_fsubmit_edit_status">';
		echo '<option value="1" ' . selected(get_option('w2dc_fsubmit_edit_status'), 1, false) . '>' . __('Pending Review', 'W2DC-FSUBMIT') . '</option>';
		echo '<option value="2" ' . selected(get_option('w2dc_fsubmit_edit_status'), 2, false) . '>' . __('Draft', 'W2DC-FSUBMIT') . '</option>';
		echo '<option value="3" ' . selected(get_option('w2dc_fsubmit_edit_status'), 3, false) . '>' . __('Published', 'W2DC-FSUBMIT') . '</option>';
		echo '</select>';
	}

	public function w2dc_fsubmit_button_callback() {
		echo '<input type="checkbox" id="w2dc_fsubmit_button" name="w2dc_fsubmit_button" value="1" ' . checked(get_option('w2dc_fsubmit_button'), 1, false) .' />';
		echo '<p class="description">' . __('The page with [webdirectory-submit] shortcode required', 'W2DC-FSUBMIT') . '</p>';
	}

	public function w2dc_hide_admin_bar_callback() {
		echo '<input type="checkbox" id="w2dc_hide_admin_bar" name="w2dc_hide_admin_bar" value="1" ' . checked(get_option('w2dc_hide_admin_bar'), 1, false) .' />';
		echo '<p class="description">' . __('Be careful with this option! Do not enable if you do not know what you do.', 'W2DC-FSUBMIT') . '</p>';
	}

	public function w2dc_allow_edit_profile_callback() {
		echo '<input type="checkbox" id="w2dc_allow_edit_profile" name="w2dc_allow_edit_profile" value="1" ' . checked(get_option('w2dc_allow_edit_profile'), 1, false) .' />';
	}
	
	public function w2dc_newuser_notification_callback() {
		echo '<textarea id="w2dc_newuser_notification" name="w2dc_newuser_notification" cols="60" rows="4">' . esc_textarea(get_option('w2dc_newuser_notification')) . '</textarea>';
	}
	
	// adapted for WPML
	public function w2dc_enable_frontend_translations_callback() {
		echo '<input type="checkbox" id="w2dc_enable_frontend_translations" name="w2dc_enable_frontend_translations" value="1" ' . checked(get_option('w2dc_enable_frontend_translations'), 1, false) .' />';
	}
	
	public function getSubmitPage() {
		global $w2dc_instance, $wpdb, $wp_rewrite;
		
		$w2dc_instance->submit_page_url = '';
		$w2dc_instance->submit_page_slug = '';
		$w2dc_instance->submit_page_id = 0;

		if ($submit_page = $wpdb->get_row("SELECT ID AS id, post_name AS slug FROM {$wpdb->posts} WHERE post_content LIKE '%[" . W2DC_FSUBMIT_SHORTCODE . "]%' AND post_status = 'publish' AND post_type = 'page' LIMIT 1", ARRAY_A)) {
			$w2dc_instance->submit_page_id = $submit_page['id'];
			$w2dc_instance->submit_page_slug = $submit_page['slug'];
			
			// adapted for WPML
			if (function_exists('icl_object_id')) {
				if ($tpage = icl_object_id($w2dc_instance->submit_page_id, 'page')) {
					$w2dc_instance->submit_page_id = $tpage;
					$w2dc_instance->submit_page_slug = get_post($w2dc_instance->submit_page_id)->post_name;
				}
			}
			
			if ($wp_rewrite->using_permalinks())
				$w2dc_instance->submit_page_url = get_permalink($w2dc_instance->submit_page_id);
			else
				$w2dc_instance->submit_page_url = add_query_arg('page_id', $w2dc_instance->submit_page_id, home_url());
		} else {
			$w2dc_instance->submit_page_slug = '';
			$w2dc_instance->submit_page_id = 0;
		}

		if (get_option('w2dc_fsubmit_button') && $w2dc_instance->submit_page_id === 0)
			w2dc_addMessage(__('<b>Web 2.0 Directory Frontend submit plugin</b>: sorry, but there isn\'t any page with [webdirectory-submit] shortcode. This is impossible to add new listings from frontend.', 'W2DC-FSUBMIT'));
	}

	public function getDasboardPage() {
		global $w2dc_instance, $wpdb, $wp_rewrite;
		
		$w2dc_instance->dashboard_page_url = '';
		$w2dc_instance->dashboard_page_slug = '';
		$w2dc_instance->dashboard_page_id = 0;

		if ($dashboard_page = $wpdb->get_row("SELECT ID AS id, post_name AS slug FROM {$wpdb->posts} WHERE post_content LIKE '%[" . W2DC_DASHBOARD_SHORTCODE . "]%' AND post_status = 'publish' AND post_type = 'page' LIMIT 1", ARRAY_A)) {
			$w2dc_instance->dashboard_page_id = $dashboard_page['id'];
			$w2dc_instance->dashboard_page_slug = $dashboard_page['slug'];
			
			// adapted for WPML
			if (function_exists('icl_object_id')) {
				if ($tpage = icl_object_id($w2dc_instance->dashboard_page_id, 'page')) {
					$w2dc_instance->dashboard_page_id = $tpage;
					$w2dc_instance->dashboard_page_slug = get_post($w2dc_instance->dashboard_page_id)->post_name;
				}
			}
			
			if ($wp_rewrite->using_permalinks())
				$w2dc_instance->dashboard_page_url = get_permalink($w2dc_instance->dashboard_page_id);
			else
				$w2dc_instance->dashboard_page_url = add_query_arg('page_id', $w2dc_instance->dashboard_page_id, home_url());
		} else {
			$w2dc_instance->dashboard_page_slug = '';
			$w2dc_instance->dashboard_page_id = 0;
		}
	}
	
	public function add_submit_button() {
		global $w2dc_instance;

		if (get_option('w2dc_fsubmit_button') && $w2dc_instance->submit_page_url)
			echo '<input type="button" class="w2dc_submit_listing_link btn btn-info" name="submitlisting" value="' . esc_attr__('Submit new listing', 'W2DC-FSUBMIT') . '" onclick="window.location=\'' . w2dc_submitUrl() . '\';" /> ';
	}

	public function add_claim_button($listing) {
		global $w2dc_instance;

		if ($listing)
			echo '<input type="button" class="w2dc_claim_listing_link btn btn-info" name="claimlisting" value="' . esc_attr__('Claim listing', 'W2DC-FSUBMIT') . '" onclick="window.location=\'' . w2dc_directoryUrl(array('w2dc_action' => 'claimlisting')) . '\';" />';
	}

	public function add_logout_button() {
		global $w2dc_instance, $post;

		if ($post->ID == $w2dc_instance->dashboard_page_id)
			echo '<input type="button" class="w2dc_logout_link btn btn-info" name="logout" value="' . esc_attr__('Log out', 'W2DC-FSUBMIT') . '" onclick="window.location=\'' . wp_logout_url(w2dc_directoryUrl()) . '\';" /> ';
	}
	
	public function remove_admin_bar() {
		if (get_option('w2dc_hide_admin_bar') && !current_user_can('administrator') && !is_admin()) {
			show_admin_bar(false);
		}
	}

	public function restrict_dashboard() {
		global $w2dc_instance, $pagenow;

		if ($pagenow != 'admin-ajax.php')
			if (get_option('w2dc_hide_admin_bar') && !current_user_can('administrator') && is_admin()) {
				w2dc_addMessage(__('You can not see dashboard!', 'W2DC-FSUBMIT'), 'error');
				wp_redirect(w2dc_directoryUrl());
			}
	}
	
	public function get_edit_post_link($url, $post_id) {
		global $w2dc_instance;

		if (!is_admin() && $w2dc_instance->dashboard_page_url)
			return apply_filters('edit_objects_links', $url, $post_id, $w2dc_instance->dashboard_page_url);
		
		return $url;
	}
	
	public function edit_listings_links($url, $post_id, $dashboard_url) {
		if (($post = get_post($post_id)) && $post->post_type == W2DC_POST_TYPE)
			return w2dc_dashboardUrl(array('w2dc_action' => 'edit_listing', 'listing_id' => $post_id));
	
		return $url;
	}
	
	public function enqueue_scripts_styles() {

	}
	
	public function enqueue_frontend_style() {
		wp_register_style('w2dc_fsubmit', W2DC_FSUBMIT_RESOURCES_URL . 'css/submitlisting.css');
		wp_enqueue_style('w2dc_fsubmit');
		if (is_file(W2DC_FSUBMIT_RESOURCES_PATH . 'css/submitlisting-custom.css'))
			wp_register_style('w2dc_fsubmit-custom', W2DC_FSUBMIT_RESOURCES_URL . 'css/submitlisting-custom.css');
		
		wp_enqueue_style('w2dc_fsubmit-custom');
	}
}

function w2dc_install_fsubmit() {
	update_option('w2dc_fsubmit_default_status', 1);
	update_option('w2dc_fsubmit_login_mode', 1);
	update_option('w2dc_installed_fsubmit', true);
	
	w2dc_upgrade_fsubmit('1.5.0');
	w2dc_upgrade_fsubmit('1.5.4');
	w2dc_upgrade_fsubmit('1.6.2');
}

function w2dc_upgrade_fsubmit($new_version) {
	if ($new_version == '1.5.0') {
		update_option('w2dc_fsubmit_edit_status', 3);
		update_option('w2dc_fsubmit_button', 1);
		update_option('w2dc_hide_admin_bar', 0);
		update_option('w2dc_newuser_notification', 'Hello [author], your listing "[listing]" was successfully submitted.
You may manage your listing using following credentials:
login: [login]
password: [password]');
	}
	
	if ($new_version == '1.5.4')
		update_option('w2dc_allow_edit_profile', 1);

	if ($new_version == '1.6.2')
		update_option('w2dc_enable_frontend_translations', 1);
}

function w2dc_submitUrl($path = '') {
	global $w2dc_instance;

	// adapted for WPML
	if (function_exists('icl_object_id')) {
		global $sitepress;
		$settings = get_option( 'icl_sitepress_settings' );
		if ($sitepress->get_option('language_negotiation_type') == 3) {
			$code = $sitepress->get_current_language();
			// remove any previous value.
			if ( strpos( $w2dc_instance->submit_page_url, '?lang=' . $code . '&' ) !== false ) {
				$w2dc_instance->submit_page_url = str_replace( '?lang=' . $code . '&', '', $w2dc_instance->submit_page_url );
			} elseif ( strpos( $w2dc_instance->submit_page_url, '?lang=' . $code . '/' ) !== false ) {
				$w2dc_instance->submit_page_url = str_replace( '?lang=' . $code . '/', '', $w2dc_instance->submit_page_url );
			} elseif ( strpos( $w2dc_instance->submit_page_url, '?lang=' . $code ) !== false ) {
				$w2dc_instance->submit_page_url = str_replace( '?lang=' . $code, '', $w2dc_instance->submit_page_url );
			} elseif ( strpos( $w2dc_instance->submit_page_url, '&lang=' . $code . '/' ) !== false ) {
				$w2dc_instance->submit_page_url = str_replace( '&lang=' . $code . '/', '', $w2dc_instance->submit_page_url );
			} elseif ( strpos( $w2dc_instance->submit_page_url, '&lang=' . $code ) !== false ) {
				$w2dc_instance->submit_page_url = str_replace( '&lang=' . $code, '', $w2dc_instance->submit_page_url );
			}
		}
	}

	if (!is_array($path)) {
		if ($path) {
			// found that on some instances of WP "native" trailing slashes may be missing
			$url = rtrim($w2dc_instance->submit_page_url, '/') . '/' . rtrim($path, '/') . '/';
		} else
			$url = $w2dc_instance->submit_page_url;
	} else
		$url = add_query_arg($path, $w2dc_instance->submit_page_url);

	// adapted for WPML
	if (function_exists('icl_object_id')) {
		$url = $sitepress->convert_url($url);
	}

	return $url;
}

function w2dc_dashboardUrl($path = '') {
	global $w2dc_instance;

	// adapted for WPML
	if (function_exists('icl_object_id')) {
		global $sitepress;
		$settings = get_option( 'icl_sitepress_settings' );
		if ($sitepress->get_option('language_negotiation_type') == 3) {
			$code = $sitepress->get_current_language();
			// remove any previous value.
			if ( strpos( $w2dc_instance->dashboard_page_url, '?lang=' . $code . '&' ) !== false ) {
				$w2dc_instance->dashboard_page_url = str_replace( '?lang=' . $code . '&', '', $w2dc_instance->dashboard_page_url );
			} elseif ( strpos( $w2dc_instance->dashboard_page_url, '?lang=' . $code . '/' ) !== false ) {
				$w2dc_instance->dashboard_page_url = str_replace( '?lang=' . $code . '/', '', $w2dc_instance->dashboard_page_url );
			} elseif ( strpos( $w2dc_instance->dashboard_page_url, '?lang=' . $code ) !== false ) {
				$w2dc_instance->dashboard_page_url = str_replace( '?lang=' . $code, '', $w2dc_instance->dashboard_page_url );
			} elseif ( strpos( $w2dc_instance->dashboard_page_url, '&lang=' . $code . '/' ) !== false ) {
				$w2dc_instance->dashboard_page_url = str_replace( '&lang=' . $code . '/', '', $w2dc_instance->dashboard_page_url );
			} elseif ( strpos( $w2dc_instance->dashboard_page_url, '&lang=' . $code ) !== false ) {
				$w2dc_instance->dashboard_page_url = str_replace( '&lang=' . $code, '', $w2dc_instance->dashboard_page_url );
			}
		}
	}

	if (!is_array($path)) {
		if ($path) {
			// found that on some instances of WP "native" trailing slashes may be missing
			$url = rtrim($w2dc_instance->dashboard_page_url, '/') . '/' . rtrim($path, '/') . '/';
		} else
			$url = $w2dc_instance->dashboard_page_url;
	} else
		$url = add_query_arg($path, $w2dc_instance->dashboard_page_url);

	// adapted for WPML
	if (function_exists('icl_object_id')) {
		$url = $sitepress->convert_url($url);
	}

	return $url;
}

global $w2dc_fsubmit_instance;

$w2dc_fsubmit_instance = new w2dc_fsubmit_plugin();
$w2dc_fsubmit_instance->init();

?>
