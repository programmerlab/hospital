msgid ""
msgstr ""
"Project-Id-Version: w2dc fsubmit v1.0.0\n"
"POT-Creation-Date: 2014-08-21 09:59+0600\n"
"PO-Revision-Date: 2014-08-21 09:59+0600\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SearchPath-0: .\n"

#: classes/dashboard_controller.php:55 classes/dashboard_controller.php:57
#: classes/submit_controller.php:25 classes/submit_controller.php:64
#: classes/submit_controller.php:66 templates/edit_listing.tpl.php:11
#: templates/submitlisting_step_create.tpl.php:59
msgid "Auto Draft"
msgstr ""

#: classes/dashboard_controller.php:56 classes/submit_controller.php:65
msgid "Listing title field required"
msgstr ""

#: classes/dashboard_controller.php:107 classes/submit_controller.php:167
msgid "Listing was saved successfully! Now it's awaiting moderators approval."
msgstr ""

#: classes/dashboard_controller.php:110 classes/submit_controller.php:170
msgid "Listing was saved successfully as draft! Contact site manager, please."
msgstr ""

#: classes/dashboard_controller.php:113 classes/submit_controller.php:173
msgid "Listing was saved successfully!"
msgstr ""

#: classes/dashboard_controller.php:150
msgid "Listing was raised up successfully!"
msgstr ""

#: classes/dashboard_controller.php:166
msgid "Listing was renewed successfully!"
msgstr ""

#: classes/dashboard_controller.php:183
msgid "Listing was deleted successfully!"
msgstr ""

#: classes/dashboard_controller.php:187
msgid "An error has occurred and listing was not deleted"
msgstr ""

#: classes/dashboard_controller.php:199
msgid "New level ID"
msgstr ""

#: classes/dashboard_controller.php:203
msgid "Listing level was changed successfully!"
msgstr ""

#: classes/dashboard_controller.php:206
msgid "New level must be selected!"
msgstr ""

#: classes/dashboard_controller.php:251
msgid "Your profile was successfully updated!"
msgstr ""

#: classes/submit_controller.php:52
#: templates/submitlisting_step_create.tpl.php:47
msgid "Contact Name"
msgstr ""

#: classes/submit_controller.php:53
#: templates/submitlisting_step_create.tpl.php:50
msgid "Contact Email"
msgstr ""

#: classes/submit_controller.php:92
msgid "Verification code wasn't entered correctly!"
msgstr ""

#: classes/submit_controller.php:146
msgid "Registration notification"
msgstr ""

#: templates/dashboard.tpl.php:8
msgid "Listings"
msgstr ""

#: templates/dashboard.tpl.php:10
msgid "My profile"
msgstr ""

#: templates/delete.tpl.php:2
#, php-format
msgid "Delete listing \"%s\""
msgstr ""

#: templates/delete.tpl.php:5
msgid ""
"Listing will be completely deleted with all metadata, comments and "
"attachments."
msgstr ""

#: templates/delete.tpl.php:9
msgid "Delete listing"
msgstr ""

#: templates/delete.tpl.php:11 templates/edit_listing.tpl.php:81
#: templates/raise_up.tpl.php:13 templates/renew.tpl.php:12
#: templates/upgrade.tpl.php:22
msgid "Cancel"
msgstr ""

#: templates/edit_listing.tpl.php:4
#, php-format
msgid "Edit listing \"%s\""
msgstr ""

#: templates/edit_listing.tpl.php:9
#: templates/submitlisting_step_create.tpl.php:57
msgid "Listing title"
msgstr ""

#: templates/edit_listing.tpl.php:33
#: templates/submitlisting_step_create.tpl.php:79
msgid "Listing expiration date"
msgstr ""

#: templates/edit_listing.tpl.php:69
#: templates/submitlisting_step_create.tpl.php:115
msgid "Listing locations"
msgstr ""

#: templates/edit_listing.tpl.php:79 templates/profile.tpl.php:75
msgid "Save changes"
msgstr ""

#: templates/listings.tpl.php:4
msgid "ID"
msgstr ""

#: templates/listings.tpl.php:5
msgid "Title"
msgstr ""

#: templates/listings.tpl.php:6
msgid "Level"
msgstr ""

#: templates/listings.tpl.php:7
msgid "Status"
msgstr ""

#: templates/listings.tpl.php:8
msgid "Expiration date"
msgstr ""

#: templates/listings.tpl.php:24
msgid "Pending"
msgstr ""

#: templates/listings.tpl.php:25 w2dc_fsubmit.php:165 w2dc_fsubmit.php:173
msgid "Draft"
msgstr ""

#: templates/listings.tpl.php:37
msgid "active"
msgstr ""

#: templates/listings.tpl.php:39
msgid "expired"
msgstr ""

#: templates/listings.tpl.php:41
msgid "unpaid"
msgstr ""

#: templates/listings.tpl.php:43
msgid "stopped"
msgstr ""

#: templates/listings.tpl.php:47
msgid "Eternal active period"
msgstr ""

#: templates/listings.tpl.php:51
msgid "edit listing"
msgstr ""

#: templates/listings.tpl.php:52
msgid "delete listing"
msgstr ""

#: templates/listings.tpl.php:55
msgid "raise up listing"
msgstr ""

#: templates/listings.tpl.php:60
msgid "renew listing"
msgstr ""

#: templates/login_form.tpl.php:6
#: templates/submitlisting_step_create.tpl.php:5
#, php-format
msgid "Create new listing in level \"%s\""
msgstr ""

#: templates/login_form.tpl.php:15 templates/login_form.tpl.php:23
#: templates/login_form.tpl.php:30
#: templates/submitlisting_step_create.tpl.php:14
#: templates/submitlisting_step_create.tpl.php:22
#: templates/submitlisting_step_create.tpl.php:29
#: templates/submitlisting_step_level.tpl.php:8
#: templates/submitlisting_step_level.tpl.php:16
#: templates/submitlisting_step_level.tpl.php:23
msgid "Step"
msgstr ""

#: templates/login_form.tpl.php:16
#: templates/submitlisting_step_create.tpl.php:15
#: templates/submitlisting_step_level.tpl.php:9
msgid "Choose level"
msgstr ""

#: templates/login_form.tpl.php:24
#: templates/submitlisting_step_create.tpl.php:23
#: templates/submitlisting_step_level.tpl.php:17
msgid "Login"
msgstr ""

#: templates/login_form.tpl.php:31
#: templates/submitlisting_step_create.tpl.php:30
#: templates/submitlisting_step_level.tpl.php:24
msgid "Create listing"
msgstr ""

#: templates/profile.tpl.php:9
msgid "Username"
msgstr ""

#: templates/profile.tpl.php:10
msgid "Usernames cannot be changed."
msgstr ""

#: templates/profile.tpl.php:13
msgid "First Name"
msgstr ""

#: templates/profile.tpl.php:17
msgid "Last Name"
msgstr ""

#: templates/profile.tpl.php:21
msgid "Nickname"
msgstr ""

#: templates/profile.tpl.php:21 templates/profile.tpl.php:56
msgid "(required)"
msgstr ""

#: templates/profile.tpl.php:25
msgid "Display to Public as"
msgstr ""

#: templates/profile.tpl.php:56
msgid "E-mail"
msgstr ""

#: templates/profile.tpl.php:60
msgid "New Password"
msgstr ""

#: templates/profile.tpl.php:62
msgid ""
"If you would like to change the password type a new one. Otherwise leave "
"this blank."
msgstr ""

#: templates/profile.tpl.php:65
msgid "Repeat New Password"
msgstr ""

#: templates/profile.tpl.php:67
msgid "Type your new password again."
msgstr ""

#: templates/profile.tpl.php:68
msgid "Strength indicator"
msgstr ""

#: templates/profile.tpl.php:69
msgid ""
"Hint: The password should be at least seven characters long. To make it "
"stronger, use upper and lower case letters, numbers and symbols like ! \" ? "
"$ % ^ &amp; )."
msgstr ""

#: templates/raise_up.tpl.php:2
#, php-format
msgid "Raise up listing \"%s\""
msgstr ""

#: templates/raise_up.tpl.php:5
msgid ""
"Listing will be raised up to the top of all lists, those ordered by date."
msgstr ""

#: templates/raise_up.tpl.php:6
msgid ""
"Note, that listing will not stick on top, so new listings and other "
"listings, those were raised up later, will place higher."
msgstr ""

#: templates/raise_up.tpl.php:11
msgid "Raise up"
msgstr ""

#: templates/raise_up.tpl.php:15 templates/renew.tpl.php:14
#: templates/upgrade.tpl.php:24
msgid "Go back "
msgstr ""

#: templates/renew.tpl.php:2
#, php-format
msgid "Renew listing \"%s\""
msgstr ""

#: templates/renew.tpl.php:5
msgid ""
"Listing will be renewed and raised up to the top of all lists, those ordered "
"by date."
msgstr ""

#: templates/renew.tpl.php:10
msgid "Renew listing"
msgstr ""

#: templates/submitlisting_step_create.tpl.php:45
msgid "Contact info"
msgstr ""

#: templates/submitlisting_step_create.tpl.php:129 w2dc_fsubmit.php:243
msgid "Submit new listing"
msgstr ""

#: templates/submitlisting_step_level.tpl.php:61
msgid "Sticky"
msgstr ""

#: templates/submitlisting_step_level.tpl.php:69
msgid "Featured"
msgstr ""

#: templates/submitlisting_step_level.tpl.php:77
msgid "Categories number"
msgstr ""

#: templates/submitlisting_step_level.tpl.php:78
msgid "Unlimited"
msgstr ""

#: templates/submitlisting_step_level.tpl.php:81
msgid "Google map"
msgstr ""

#: templates/submitlisting_step_level.tpl.php:89
msgid "Images number"
msgstr ""

#: templates/submitlisting_step_level.tpl.php:93
msgid "Videos number"
msgstr ""

#: templates/submitlisting_step_level.tpl.php:97
msgid "Submit"
msgstr ""

#: templates/upgrade.tpl.php:2
#, php-format
msgid "Change level of listing \"%s\""
msgstr ""

#: templates/upgrade.tpl.php:5
msgid ""
"The level of listing will be changed. You may upgrade or downgrade the "
"level. If new level has an option of limited active period - expiration date "
"of listing will be reassigned automatically."
msgstr ""

#: templates/upgrade.tpl.php:9
msgid "Choose new level"
msgstr ""

#: templates/upgrade.tpl.php:20
msgid "Change level"
msgstr ""

#: w2dc_fsubmit.php:88
msgid "Frontend submission and dashboard"
msgstr ""

#: w2dc_fsubmit.php:95
msgid "Select frontend submission login mode"
msgstr ""

#: w2dc_fsubmit.php:103
msgid "Post status after frontend submit"
msgstr ""

#: w2dc_fsubmit.php:111
msgid "Post status after listing was modified"
msgstr ""

#: w2dc_fsubmit.php:119
msgid "Enable submit listing button"
msgstr ""

#: w2dc_fsubmit.php:127
msgid ""
"Hide top admin bar at the frontend for regular users and do not allow them "
"to see dashboard at all"
msgstr ""

#: w2dc_fsubmit.php:135
msgid "Allow users to manage own profile at the frontend dashboard"
msgstr ""

#: w2dc_fsubmit.php:144
msgid "Registration of new user notification"
msgstr ""

#: w2dc_fsubmit.php:153
msgid "login required"
msgstr ""

#: w2dc_fsubmit.php:155
msgid "necessary to fill in contact form"
msgstr ""

#: w2dc_fsubmit.php:157
msgid "not necessary to fill in contact form"
msgstr ""

#: w2dc_fsubmit.php:159
msgid "do not even show contact form"
msgstr ""

#: w2dc_fsubmit.php:164 w2dc_fsubmit.php:172
msgid "Pending Review"
msgstr ""

#: w2dc_fsubmit.php:166 w2dc_fsubmit.php:174
msgid "Published"
msgstr ""

#: w2dc_fsubmit.php:180
msgid "The page with [webdirectory-submit] shortcode required"
msgstr ""

#: w2dc_fsubmit.php:185
msgid ""
"Be careful with this option! Do not enable if you do not know what you do."
msgstr ""

#: w2dc_fsubmit.php:216
msgid ""
"<b>Web 2.0 Directory Frontend submit plugin</b>: sorry, but there isn't any "
"page with [webdirectory-submit] shortcode. This is impossible to add new "
"listings from frontend."
msgstr ""

#: w2dc_fsubmit.php:250
msgid "Claim listing"
msgstr ""

#: w2dc_fsubmit.php:257
msgid "Log out"
msgstr ""

#: w2dc_fsubmit.php:271
msgid "You can not see dashboard!"
msgstr ""
