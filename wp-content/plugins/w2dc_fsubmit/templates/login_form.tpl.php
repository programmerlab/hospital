<?php w2dc_renderMessages(); ?>

<div class="w2dc_content">
	<?php if (isset($_GET['level']) && ($level = $w2dc_instance->levels->getLevelById($_GET['level']))): ?>
	<?php if (count($w2dc_instance->levels->levels_array) > 1): ?>
	<h2><?php echo sprintf(__('Create new listing in level "%s"', 'W2DC-FSUBMIT'), $level->name); ?></h2>
	<?php endif; ?>

	<?php if ((count($w2dc_instance->levels->levels_array) > 1) || (get_option('w2dc_fsubmit_login_mode') == 1 && !is_user_logged_in())): ?>
	<div class="submit_section_adv">
		<?php $step = 1; ?>

		<?php if (count($w2dc_instance->levels->levels_array) > 1): ?>
		<div class="adv_step">
			<div class="adv_circle adv_circle_passed"><?php _e('Step', 'W2DC'); ?> <?php echo $step++; ?></div>
			<?php _e('Choose level', 'W2DC-FSUBMIT'); ?>
		</div>
		<div class="adv_line adv_line_passed"></div>
		<?php endif; ?>

		<?php if (get_option('w2dc_fsubmit_login_mode') == 1 && !is_user_logged_in()): ?>
		<div class="adv_step adv_step_active">
			<div class="adv_circle adv_circle_active"><?php _e('Step', 'W2DC'); ?> <?php echo $step++; ?></div>
			<?php _e('Login', 'W2DC-FSUBMIT'); ?>
		</div>
		<div class="adv_line"></div>
		<?php endif; ?>

		<div class="adv_step">
			<div class="adv_circle"><?php _e('Step', 'W2DC'); ?> <?php echo $step++; ?></div>
			<?php _e('Create listing', 'W2DC-FSUBMIT'); ?>
		</div>
		
		<?php $step = apply_filters('w2dc_create_listings_steps_html', $step, $level); ?>

		<div class="clear_float"></div>
	</div>
	<?php endif; ?>
	<?php endif; ?>

	<div class="submit_section_adv">
		<?php w2dc_login_form(); ?>
	</div>
</div>