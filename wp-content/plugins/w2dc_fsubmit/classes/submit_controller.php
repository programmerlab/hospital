<?php 

class w2dc_submit_controller extends w2dc_frontend_controller {

	public function __construct() {
		global $w2dc_instance;

		if ((!isset($_GET['level']) || !is_numeric($_GET['level']) || !array_key_exists($_GET['level'], $w2dc_instance->levels->levels_array)) && count($w2dc_instance->levels->levels_array) > 1) {
			$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'submitlisting_step_level.tpl.php');
		} elseif (count($w2dc_instance->levels->levels_array)) {
			if (count($w2dc_instance->levels->levels_array) == 1)
				$level = array_shift(array_keys($w2dc_instance->levels->levels_array));
			else
				$level = $_GET['level'];

			if (get_option('w2dc_fsubmit_login_mode') == 1 && !is_user_logged_in()) {
				add_action('wp_enqueue_scripts', array($this, 'enqueue_login_scripts_styles'));
				$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'login_form.tpl.php');
			} else {
				$this->w2dc_user_contact_name = '';
				$this->w2dc_user_contact_email = '';
				if (!isset($_POST['listing_id']) || !isset($_POST['listing_id_hash']) || !is_numeric($_POST['listing_id']) || md5($_POST['listing_id'] . AUTH_SALT) != $_POST['listing_id_hash']) {
					// Create Auto-Draft
					$new_post_args = array(
							'post_title' => __('Auto Draft'),
							'post_type' => W2DC_POST_TYPE,
							'post_status' => 'auto-draft'
					);
					if ($new_post_id = wp_insert_post($new_post_args)) {
						$w2dc_instance->listings_manager->current_listing = new w2dc_listing($level);
						$w2dc_instance->listings_manager->saveInitialDraft($new_post_id);

						$listing = w2dc_getCurrentListingInAdmin();
					}
				} else {
					// This is existed Auto-Draft
					$listing_id = $_POST['listing_id'];

					$listing = new w2dc_listing();
					$listing->loadListingFromPost($listing_id);
					$w2dc_instance->current_listing = $listing;
					$w2dc_instance->listings_manager->current_listing = $listing;

					$errors = array();

					if (!is_user_logged_in() && (get_option('w2dc_fsubmit_login_mode') == 2 || get_option('w2dc_fsubmit_login_mode') == 3)) {
						if (get_option('w2dc_fsubmit_login_mode') == 2)
							$required = '|required';
						else
							$required = '';
						$validation = new form_validation();
						$validation->set_rules('w2dc_user_contact_name', __('Contact Name', 'W2DC-FSUBMIT'), $required);
						$validation->set_rules('w2dc_user_contact_email', __('Contact Email', 'W2DC-FSUBMIT'), 'valid_email' . $required);
						if (!$validation->run()) {
							$user_valid = false;
							$errors[] = $validation->error_string();
						} else
							$user_valid = true;

						$this->w2dc_user_contact_name = $validation->result_array('w2dc_user_contact_name');
						$this->w2dc_user_contact_email = $validation->result_array('w2dc_user_contact_email');
					}

					if (!isset($_POST['post_title']) || !$_POST['post_title'] || $_POST['post_title'] == __('Auto Draft')) {
						$errors[] = __('Listing title field required', 'W2DC-FSUBMIT');
						$post_title = __('Auto Draft');
					} else 
						$post_title = $_POST['post_title'];

					if ($listing->level->categories_number > 0 || $listing->level->unlimited_categories) {
						if ($post_categories_ids = $w2dc_instance->categories_manager->validateCategories($listing->level, $_POST, $errors)) {
							foreach ($post_categories_ids AS $key=>$id)
								$post_categories_ids[$key] = intval($id);
						}
						wp_set_object_terms($listing->post->ID, $post_categories_ids, W2DC_CATEGORIES_TAX);
					}

					$w2dc_instance->content_fields->saveValues($listing->post->ID, $post_categories_ids, $errors, $_POST);

					if ($listing->level->locations_number) {
						if ($validation_results = $w2dc_instance->locations_manager->validateLocations($errors)) {
							$w2dc_instance->locations_manager->saveLocations($listing->level, $listing->post->ID, $validation_results);
						}
					}
						
					if ($listing->level->images_number || $listing->level->videos_number) {
						if ($validation_results = $w2dc_instance->media_manager->validateAttachments($listing->level, $errors))
							$w2dc_instance->media_manager->saveAttachments($listing->level, $listing->post->ID, $validation_results);
					}

					if (!w2dc_is_recaptcha_passed())
						$errors[] = __('Verification code wasn\'t entered correctly!', 'W2DC-FSUBMIT');
						
					$postarr = array(
							'ID' => $listing_id,
							'post_title' => $post_title,
							'post_content' => $_POST['post_content'],
							'post_excerpt' => $_POST['post_excerpt'],
							'post_type' => W2DC_POST_TYPE,
					);
					$result = wp_update_post($postarr, true);
					if (is_wp_error($result))
						$errors[] = $result->get_error_message();

					if ($errors) {
						foreach ($errors AS $error)
							w2dc_addMessage($error, 'error');
					} else {
						if (!is_user_logged_in() && (get_option('w2dc_fsubmit_login_mode') == 2 || get_option('w2dc_fsubmit_login_mode') == 3 || get_option('w2dc_fsubmit_login_mode') == 4)) {
							if (email_exists($this->w2dc_user_contact_email)) {
								$user = get_user_by('email', $this->w2dc_user_contact_email);
								$post_author_id = $user->ID;
								$post_author_username = $user->user_login;
							} else {
								$author_postfix = time();
								if ($this->w2dc_user_contact_name) {
									$display_author_name = $this->w2dc_user_contact_name;
									$login_author_name = $this->w2dc_user_contact_name . ' ' . $author_postfix;
								} else {
									$display_author_name = 'Author ' . $author_postfix;
									$login_author_name = 'Author ' . $author_postfix;
								}
								if ($this->w2dc_user_contact_email)
									$author_email = $this->w2dc_user_contact_email;
								else
									$author_email = '';
								
								$password = wp_generate_password(5, false);
								
								$post_author_id = wp_insert_user(array(
										'display_name' => $display_author_name,
										'user_login' => $login_author_name,
										'user_email' => $author_email,
										'user_pass' => $password
								));
								$post_author_username = $login_author_name;

								if ($author_email) {
									/* $headers =  "MIME-Version: 1.0\n" .
											"From: get_option('blogname' <" . get_option('admin_email') . ">\n" .
											"Reply-To: get_option('admin_email')\n" .
											"Content-Type: text/plain; charset=\"" . get_option('blog_charset') . "\"\n"; */
									$headers[] = "From: " . get_option('blogname') . " <" . get_option('admin_email') . ">";
									$headers[] = "Reply-To: " . get_option('admin_email');
										
									$subject = "[" . get_option('blogname') . "] " . __('Registration notification', 'W2DC-FSUBMIT');
									$body = str_replace('[author]', $display_author_name, str_replace('[listing]', $post_title, str_replace('[login]', $login_author_name, str_replace('[password]', $password, get_option('w2dc_newuser_notification')))));
									wp_mail($author_email, $subject, $body, $headers);
								}
							}

							// this is programmatic login of newely created user or existed user - he just need to enter email
							if ($post_author_id && $this->w2dc_user_contact_email) {
								add_filter('authenticate', 'allow_programmatic_login', 10, 3);
								$user = wp_signon(array('user_login' => $post_author_username));
								remove_filter('authenticate', 'allow_programmatic_login', 10, 3);
								if (is_a($user, 'WP_User'))
									wp_set_current_user($user->ID, $user->user_login);
							}
						} elseif (is_user_logged_in())
							$post_author_id = get_current_user_id();
						else
							$post_author_id = 0;

						if (get_option('w2dc_fsubmit_default_status') == 1) {
							$post_status = 'pending';
							$message = __('Listing was saved successfully! Now it\'s awaiting moderators approval.', 'W2DC-FSUBMIT');
						} elseif (get_option('w2dc_fsubmit_default_status') == 2) {
							$post_status = 'draft';
							$message = __('Listing was saved successfully as draft! Contact site manager, please.', 'W2DC-FSUBMIT');
						} elseif (get_option('w2dc_fsubmit_default_status') == 3) {
							$post_status = 'publish';
							$message = __('Listing was saved successfully!', 'W2DC-FSUBMIT');
						}

						wp_update_post(array('ID' => $listing_id, 'post_author' => $post_author_id, 'post_status' => $post_status));
						add_post_meta($listing->post->ID, '_listing_created', true);
						add_post_meta($listing->post->ID, '_order_date', time());
						add_post_meta($listing->post->ID, '_listing_status', 'active');
							
						if (!$listing->level->eternal_active_period) {
							if (get_option('w2dc_change_expiration_date'))
								$w2dc_instance->listings_manager->changeExpirationDate();
							else {
								$expiration_date = w2dc_sumDates(time(), $listing->level->active_days, $listing->level->active_months, $listing->level->active_years);
								add_post_meta($listing->post->ID, '_expiration_date', $expiration_date);
							}
						}

						w2dc_addMessage($message);
						
						// renew data inside $listing object
						$listing = $w2dc_instance->listings_manager->loadListing($listing_id);

						do_action('w2dc_listing_creation_front', $listing);

						if ($w2dc_instance->dashboard_page_url)
							$redirect_to = w2dc_dashboardUrl();
						else
							$redirect_to = w2dc_directoryUrl();
						wp_redirect($redirect_to);
						exit;
					}
					// renew data inside $listing object
					$listing = $w2dc_instance->listings_manager->loadListing($listing_id);
				}
	
				$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'submitlisting_step_create.tpl.php');
				if ($listing->level->categories_number > 0 || $listing->level->unlimited_categories) {
					add_action('wp_enqueue_scripts', array($w2dc_instance->categories_manager, 'admin_enqueue_scripts_styles'));
				}
					
				if ($listing->level->locations_number > 0 && $listing->level->google_map) {
					add_action('wp_print_scripts', array($w2dc_instance->locations_manager, 'remove_google_maps_view'), 100);
					add_action('wp_enqueue_scripts', array($w2dc_instance->locations_manager, 'admin_enqueue_scripts_styles'));
				}
					
				if ($listing->level->images_number > 0 || $listing->level->videos_number > 0)
					add_action('wp_enqueue_scripts', array($w2dc_instance->media_manager, 'admin_enqueue_scripts_styles'));
			}
		}
		
		apply_filters('w2dc_frontend_controller_construct', $this);
	}

	public function enqueue_login_scripts_styles() {
		global $action;
		$action = 'login';
		do_action('login_enqueue_scripts');
		do_action('login_head');
	}
	
	public function display() {
		$output =  w2dc_renderTemplate($this->template, array('frontend_controller' => $this), true);
		wp_reset_postdata();

		return $output;
	}
}

/**
 * An 'authenticate' filter callback that authenticates the user using only the username.
 *
 * To avoid potential security vulnerabilities, this should only be used in the context of a programmatic login,
 * and unhooked immediately after it fires.
 *
 * @param WP_User $user
 * @param string $username
 * @param string $password
 * @return bool|WP_User a WP_User object if the username matched an existing user, or false if it didn't
 */
function allow_programmatic_login($user, $username, $password) {
	return get_user_by('login', $username);
}

?>