			<tr>
				<th scope="row">
					<label><?php _e('Locations number available', 'W2DC-ELOCATIONS'); ?></label>
				</th>
				<td>
					<input
						name="locations_number"
						type="text"
						size="1"
						value="<?php echo $level->locations_number; ?>" />
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label><?php _e('Custom markers on google map', 'W2DC-ELOCATIONS'); ?></label>
				</th>
				<td>
					<input
						name="google_map_markers"
						type="checkbox"
						value="1"
						<?php if ($level->google_map_markers) echo 'checked'; ?> />
				</td>
			</tr>