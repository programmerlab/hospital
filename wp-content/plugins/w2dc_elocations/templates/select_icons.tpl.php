		<input type="button" id="reset_icon" class="button button-primary button-large" value="<?php _e('Reset icon image', 'W2DC-ELOCATIONS'); ?>" />

		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<?php $i = 0; ?>
			<?php foreach ($custom_map_icons AS $theme=>$dir): ?>
				<?php if (is_array($dir) && count($dir)): ?>
				<td align="left" valign="top" width="33%">
					<div class="theme_block">
						<div class="theme_name"><?php echo $theme; ?></div>
						<?php foreach ($dir AS $icon): ?>
							<div class="w2dc_icon" icon_file="<?php echo $theme . '/' . $icon; ?>"><img src="<?php echo W2DC_MAP_ICONS_URL . 'icons/' . $theme . '/' . $icon; ?>" title="<?php echo $theme . '/' . $icon; ?>" /></div>
						<?php endforeach;?>
					</div>
					<div class="clear_float"></div>
				</td>
				<?php if ($i++ == 2): ?>
					</tr><tr>
					<?php $i = 0; ?>
				<?php endif;?>
				<?php endif;?>
			<?php endforeach;?>
			</tr>
		</table>