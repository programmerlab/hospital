<?php if (count($search_field->min_max_options)): ?>
<?php if ($search_field->content_field->is_integer) $decimals = 0; else $decimals = 2; ?>
<div class="w2dc_field w2dc_field_search_block_<?php echo $random_id; ?> w2dc_field_search_block_<?php echo $search_field->content_field->id; ?>_<?php echo $random_id; ?> row">
	<label><?php echo $search_field->content_field->name; ?></label>

	<div class="col-md-6 form-group">
		<select name="field_<?php echo $search_field->content_field->slug; ?>_min" style="min-width: 120px" class="w2dc_field_search_input_number form-control">
		<option value=""><?php _e('- Select min -', 'W2DC-ESEARCH'); ?></option>
		<?php foreach ($search_field->min_max_options AS $item): ?>
			<?php if (is_numeric($item)): ?>
			<option value="<?php echo $item; ?>" <?php selected($search_field->min_max_value['min'], $item); ?>><?php echo number_format($item, $decimals, $search_field->content_field->decimal_separator, $search_field->content_field->thousands_separator); ?></option>
			<?php endif; ?>
		<?php endforeach; ?>
		</select>
	</div>

	<div class="col-md-6 form-group">
		<select name="field_<?php echo $search_field->content_field->slug; ?>_max" style="min-width: 120px" class="w2dc_field_search_input_number form-control">
		<option value=""><?php _e('- Select max -', 'W2DC-ESEARCH'); ?></option>
		<?php foreach ($search_field->min_max_options AS $item): ?>
			<?php if (is_numeric($item)): ?>
			<option value="<?php echo $item; ?>" <?php selected($search_field->min_max_value['max'], $item); ?>><?php echo number_format($item, $decimals, $search_field->content_field->decimal_separator, $search_field->content_field->thousands_separator); ?></option>
			<?php endif; ?>
		<?php endforeach; ?>
		</select>
	</div>
</div>
<?php endif; ?>