<?php 

class w2dc_content_field_datetime_search extends w2dc_content_field_search {
	public $min_max_value = array('min' => '', 'max' => '');

	public function renderSearch($random_id) {
		wp_enqueue_script('jquery-ui-datepicker');
		
		if ($i18n_file = w2dc_getDatePickerLangFile(get_locale())) {
			wp_register_script('datepicker-i18n', $i18n_file, array('jquery-ui-datepicker'));
			wp_enqueue_script('datepicker-i18n');
		}

		w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/datetime_input.tpl.php'), array('search_field' => $this, 'dateformat' => getDatePickerFormat(), 'random_id' => $random_id));
	}
	
	public function validateSearch(&$args, $defaults = array()) {
		$field_index = 'field_' . $this->content_field->slug . '_min';
		$value = w2dc_getValue($_GET, $field_index, w2dc_getValue($defaults, $field_index));
		if ($value && (is_numeric($value) || strtotime($value))) {
			$this->min_max_value['min'] = $value;
			$args['meta_query']['relation'] = 'AND';
			$args['meta_query'][] = array(
					'key' => '_content_field_' . $this->content_field->id . '_date',
					'value' => $this->min_max_value['min'],
					'type' => 'numeric',
					'compare' => '>='
			);
		}

		$field_index = 'field_' . $this->content_field->slug . '_max';
		$value = w2dc_getValue($_GET, $field_index, w2dc_getValue($defaults, $field_index));
		if ($value && (is_numeric($value) || strtotime($value))) {
			$this->min_max_value['max'] = $value;
			$args['meta_query']['relation'] = 'AND';
			$args['meta_query'][] = array(
					'key' => '_content_field_' . $this->content_field->id . '_date',
					'value' => $this->min_max_value['max'],
					'type' => 'numeric',
					'compare' => '<='
			);
		}
	}
	
	public function getBaseUrlArgs(&$args) {
		$field_index = 'field_' . $this->content_field->slug . '_min';
		if (isset($_GET[$field_index]) && $_GET[$field_index] && is_numeric($_GET[$field_index]))
			$args[$field_index] = $_GET[$field_index];

		$field_index = 'field_' . $this->content_field->slug . '_max';
		if (isset($_GET[$field_index]) && $_GET[$field_index] && is_numeric($_GET[$field_index]))
			$args[$field_index] = $_GET[$field_index];
	}
}
?>