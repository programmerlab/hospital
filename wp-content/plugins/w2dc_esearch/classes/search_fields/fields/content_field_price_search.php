<?php 

class w2dc_content_field_price_search extends w2dc_content_field_number_search {

	public function renderSearch($random_id) {
		if ($this->mode == 'exact_number')
			w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/price_input_exactnumber.tpl.php'), array('search_field' => $this, 'random_id' => $random_id));
		elseif ($this->mode == 'min_max')
			w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/price_input_minmax.tpl.php'), array('search_field' => $this, 'random_id' => $random_id));
	}
}
?>