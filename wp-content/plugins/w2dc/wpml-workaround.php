<?php 

// This workaround needed to avoid WPML bugs
// http://wpml.org/forums/topic/2-bugs-found/
// http://wpml.org/forums/topic/parent-child-taxonomy-terms-broken-during-duplication/

// waiting still v3.1.7.2

if (function_exists('icl_object_id')) {

	class TranslationManagementWA extends TranslationManagement {

		function duplicate_taxonomies( $master_post_id, $lang )
		{
			global $wpdb, $sitepress;

			$post_type = get_post_field( 'post_type', $master_post_id );
	
			$taxonomies = get_object_taxonomies( $post_type );
	
			$trid              = $sitepress->get_element_trid( $master_post_id, 'post_' . $post_type );
			$duplicate_post_id = false;
			if ( $trid ) {
				$translations = $sitepress->get_element_translations( $trid, 'post_' . $post_type, false, false, true );
				if ( isset( $translations[ $lang ] ) ) {
					$duplicate_post_id = $translations[ $lang ]->element_id;
				} else {
					return false; // translation not found!
				}
			}
	
			remove_filter( 'get_terms_args', array( $sitepress, 'get_terms_args_filter' ) );
			remove_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1 ); // AVOID filtering to current language
			remove_filter( 'terms_clauses', array( $sitepress, 'terms_clauses' ), 10, 4 );
	
			foreach ( $taxonomies as $taxonomy ) {
	
				$terms = wp_get_post_terms( $master_post_id, $taxonomy );
				usort( $terms, create_function( '$a,$b', 'return $a->term_taxonomy_id > $b->term_taxonomy_id;' ) );
	
				$is_translated_taxonomy = $sitepress->is_translated_taxonomy( $taxonomy );
	
				$terms_array = array();
	
				$is_taxonomy_hierarchical = is_taxonomy_hierarchical( $taxonomy );
				foreach ( $terms as $term ) {
	
					if ( $is_translated_taxonomy ) {
						$tr_id = icl_object_id( $term->term_id, $taxonomy, false, $lang );
	
						//If it has a parent
						if ( $is_taxonomy_hierarchical && $term->parent ) {
							//Get its translation
							$translated_parent = icl_object_id( $term->parent, $taxonomy, false, $lang );
	
							//If translation does not exists, create one
							if ( !$translated_parent && $sitepress->get_option( 'sync_taxonomy_parents' ) ) {
								$parent_term = get_term( $term->parent, $taxonomy );
	
								if( is_object($parent_term) && !is_wp_error( $parent_term ) ) {
									$parent_term->name =  apply_filters( 'icl_duplicate_generic_string', $parent_term->name, $lang, array( 'context' => 'taxonomy', 'attribute' => $taxonomy, 'key' => $parent_term->ID ) );
									$parent_term->slug =  apply_filters( 'icl_duplicate_generic_string', $parent_term->slug, $lang, array( 'context' => 'taxonomy_slug', 'attribute' => $taxonomy, 'key' => $parent_term->ID ) );
								}
	
								$this->create_translated_term( $lang, $taxonomy, $parent_term, true );
							}
						}
	
						if ( $tr_id ) {
							// not using get_term - unfiltered get_term
							$translated_term = $wpdb->get_row( $wpdb->prepare( "
							 SELECT * FROM {$wpdb->terms} t JOIN {$wpdb->term_taxonomy} x ON x.term_id = t.term_id WHERE t.term_id = %d AND x.taxonomy = %s", $tr_id, $taxonomy ) );
							if ( $is_taxonomy_hierarchical ) {
								$terms_array[ ] = $translated_term->term_id;
							} else {
								$terms_array[ ] = $translated_term->name;
							}
							$tt_id = $sitepress->get_element_trid( $translated_term->term_taxonomy_id, 'tax_' . $taxonomy );
							$sitepress->set_element_language_details( $translated_term->term_taxonomy_id, 'tax_' . $taxonomy, $tt_id, $lang, null, false );
						} else {
	
							//Create translated term and it parents, if missing from translations
							$term->name =  apply_filters( 'icl_duplicate_generic_string', $term->name, $lang, array( 'context' => 'taxonomy', 'attribute' => $taxonomy, 'key' => $term->term_id ) );
							$term->slug =  apply_filters( 'icl_duplicate_generic_string', $term->slug, $lang, array( 'context' => 'taxonomy_slug', 'attribute' => $taxonomy, 'key' => $term->term_id ) );
	
							$translated_term = $this->create_translated_term( $lang, $taxonomy, $term, true );
	
							if ( $translated_term ) {
								if ( $is_taxonomy_hierarchical ) {
									$terms_array[ ] = $translated_term->term_id;
								} else {
									$terms_array[ ] = $translated_term->name;
								}
							}
						}
	
					} else {
						if ( $is_taxonomy_hierarchical ) {
							$terms_array[ ] = $term->term_id;
						} else {
							$terms_array[ ] = $term->name;
						}
					}
	
				}
	
				if ( $duplicate_post_id ) {
					wp_set_post_terms( $duplicate_post_id, $terms_array, $taxonomy );
	
					//Update terms count for terms removed from the duplicated post
					$all_terms       = get_terms( $taxonomy );
					$all_terms_array = false;
					foreach ( $all_terms as $all_term ) {
						if ( !in_array( $all_term->term_id, $terms_array ) ) {
							$all_terms_array[ ] = $all_term->term_id;
						}
					}
					if ( $all_terms_array ) {
						wp_update_term_count( $all_terms_array, $taxonomy, false );
						$sitepress->update_terms_relationship_cache($all_terms_array, $taxonomy);
					}
				}
			}
	
			add_filter( 'terms_clauses', array( $sitepress, 'terms_clauses' ), 10, 4 );
			add_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1 ); // Add back the get_term_filter
			add_filter( 'get_terms_args', array( $sitepress, 'get_terms_args_filter' ) );
	
			return true;
		}
		
		private static function icl_insert_term( $name, $taxonomy, $term_args, $target_lang ) {
			global $sitepress, $wpdb;
		
			$new_term = wp_insert_term( $name, $taxonomy, $term_args );
			if ( $new_term && !is_wp_error( $new_term ) ) {
				$tt_id = $sitepress->get_element_trid( $new_term[ 'term_taxonomy_id' ], 'tax_' . $taxonomy );
				$sitepress->set_element_language_details( $new_term[ 'term_taxonomy_id' ], 'tax_' . $taxonomy, $tt_id, $target_lang );
				
				// check whether we have an orphan translation - the same trid and language but a different element id
				$translation_id = $wpdb->get_var( "
					SELECT translation_id FROM {$wpdb->prefix}icl_translations
					WHERE   trid = '{$tt_id}'
						AND language_code = '{$target_lang}'
						AND element_id <> '{$new_term[ 'term_taxonomy_id' ]}'
						AND source_language_code IS NOT NULL
				" );
	
				if ( $translation_id ) {
					$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}icl_translations WHERE translation_id=%d", $translation_id ) );
					$this->icl_translations_cache->clear();
				}
			}
		
			return $new_term;
		}
		
		/**
		 * @param $lang
		 * @param $taxonomy
		 * @param $term
		 * @param $create_missing_parent
		 *
		 * @return bool|mixed
		 */
		public function create_translated_term( $lang, $taxonomy, $term, $create_missing_parent )
		{
			//Avoid passing invalid arguments
			if(!$lang || !$taxonomy || !$term || !is_object($term)) return false;
		
			//Store the last used term_id
			static $term_id;
			//Avoid infinite recursive calls
			if(isset($term_id[$lang]) && $term_id==$term->term_id) return false;
		
			$term_id[$lang] = $term->term_id;
		
			global $wpdb, $sitepress;
		
			//Temporary switch to the target language
			$current_language = $sitepress->get_current_language();
			$sitepress->switch_lang( $lang );
			// hack setting language 1/2
			if ( isset( $_POST[ 'icl_translation_of' ] ) ) {
				$buf1 = $_POST[ 'icl_translation_of' ];
			}
			if ( isset( $_POST[ 'icl_tax_' . $taxonomy . '_language' ] ) ) {
				$buf2 = $_POST[ 'icl_tax_' . $taxonomy . '_language' ];
		
			}
			$_POST[ 'icl_translation_of' ]                 = $term->term_taxonomy_id;
			$_POST[ 'icl_tax_' . $taxonomy . '_language' ] = $lang;
		
			//Get the term translation
			$new_term = icl_object_id($term->term_id, $taxonomy, false, $lang);
			if ( is_wp_error( $new_term ) || empty( $new_term ) ) {
				$term_args = array();
		
				$translated_parent = false;
				//Find the original term parent id
				if($term->parent) {
					//Get the parent id translation
					$translated_parent = icl_object_id($term->parent, $taxonomy, false, $lang);
				}
		
				if ( $translated_parent ) {
					//If there is a translated parent, set it to the term's translation
					$term_args[ 'parent' ] = $translated_parent;
				} elseif($term->parent && $create_missing_parent) {
					//If there is not a translated parent and we need to have it, create one (recursive call)
					$parent_term = get_term($term->parent, $taxonomy);
					if ($parent_term = $this->create_translated_term($lang, $taxonomy, $parent_term, $create_missing_parent))
						$term_args['parent'] = $parent_term->term_id;
				}

				$new_term = self::icl_insert_term( $term->name, $taxonomy, $term_args, $lang );
			} else {
				$term_language = $sitepress->get_element_language_details($new_term['term_id'], 'tax_' . $taxonomy);
				if($term_language && $term_language->language_code != $lang) {
					$sitepress->set_element_language_details($new_term['term_id'], 'tax_' . $taxonomy, $term_language->trid, $lang,null, false);
				}
			}
		
			// hack setting language 2/2
			$sitepress->switch_lang($current_language);
			if ( isset( $buf1 ) ) {
				$_POST[ 'icl_translation_of' ] = $buf1;
				unset( $buf1 );
			}
			if ( isset( $buf2 ) ) {
				$_POST[ 'icl_tax_' . $taxonomy . '_language' ] = $buf2;
				unset( $buf2 );
			}
		
			$translated_term = false;
			if ( !is_wp_error( $new_term ) ) {
				// not using get_term - unfiltered get_term
				$translated_term = $wpdb->get_row(
						$wpdb->prepare( "
								SELECT *
								FROM {$wpdb->terms} t
								JOIN {$wpdb->term_taxonomy} x ON x.term_id = t.term_id
								WHERE t.term_id = %d AND x.taxonomy = %s",
								$new_term[ 'term_id' ], $taxonomy )
				);
		
				return $translated_term;
			}
		
			return $translated_term;
		}
	}
	
	$iclTranslationManagement = new TranslationManagementWA();
	
	return $iclTranslationManagement;
}
?>