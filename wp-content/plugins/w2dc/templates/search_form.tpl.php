<?php if (get_option('w2dc_show_what_search') || get_option('w2dc_show_where_search')): ?>

<script>

jQuery(document).ready(function($) {

	<?php if (get_option('w2dc_show_where_search')): ?>

	var cache = {};

	$("#where_search_<?php echo $random_id; ?>").autocomplete({

		minLength: 2,

		source: function(request, response) {

			var term = request.term;

			if (term in cache) {

				response(cache[term]);

				return;

			}

			$.ajax({

				type: "POST",

				url: js_objects.ajaxurl,

				data: {'action': 'w2dc_address_autocomplete', 'term': term},

				dataType: 'json',

				success: function(response_from_the_action_function){

					cache[term] = response_from_the_action_function;

					response(response_from_the_action_function);

				}

			});

		}

	});

	<?php endif; ?>

});

</script>



<form action="<?php echo $search_url; ?>" class="w2dc_content">

	<?php

	global $wp_rewrite;

	if (!$wp_rewrite->using_permalinks() && $w2dc_instance->index_page_id && (get_option('show_on_front') != 'page' || get_option('page_on_front') != $w2dc_instance->index_page_id)): ?>

	<input type="hidden" name="page_id" value="<?php echo $w2dc_instance->index_page_id; ?>" />

	<?php endif; ?>

	<?php if ($w2dc_instance->index_page_id): ?>

	<input type="hidden" name="w2dc_action" value="search" />

	<?php else: ?>

	<input type="hidden" name="s" value="search" />

	<?php endif; ?>

	<?php

	// adapted for WPML

	if (function_exists('icl_object_id')): ?>

	<input type="hidden" name="lang" value="<?php echo ICL_LANGUAGE_CODE; ?>" />

	<?php endif; ?>



	<div class="w2dc_search_tabs">

		<div class="w2dc_search_overlay">

			<?php if (get_option('w2dc_show_what_search')): ?>

			<div class="tab-pane active w2dc_search_tab">

				<div class="row">

					<div class="w2dc_search_section_label"><?php _e('Buscar por:', 'W2DC'); ?></div>

					<?php do_action('pre_search_what_form_html', $random_id); ?>

					<?php if (get_option('w2dc_show_keywords_search')): ?>

					<div class="col-md-6">

						<input type="text" name="what_search" class="form-control form-group" size="38" placeholder="<?php _e('Nombre del medico', 'W2DC'); ?>" value="<?php if (isset($_GET['what_search'])) echo esc_attr(stripslashes($_GET['what_search'])); ?>" />

					</div>

					<?php endif; ?>

				</div>

				<?php do_action('post_search_what_form_html', $random_id); ?>

			</div>

			<?php endif; ?>

			

			<?php if (get_option('w2dc_show_where_search')): ?>

			<div class="tab-pane w2dc_search_tab">

				<div class="row">

					<div class="w2dc_search_section_label"><?php _e('Search near:', 'W2DC'); ?></div>

					<?php if (get_option('w2dc_show_locations_search')): ?>

					<div class="col-md-6">

						<?php

						if (isset($_GET['search_location']) && is_numeric($_GET['search_location']))

							$term_id = $_GET['search_location'];

						else

							$term_id = 0; 

						w2dc_tax_dropdowns_init(W2DC_LOCATIONS_TAX, 'search_location', $term_id, get_option('w2dc_show_location_count_in_search'), array(), $w2dc_instance->locations_levels->getSelectionsArray()); ?>

					</div>

					<?php endif; ?>

					<?php if (get_option('w2dc_show_address_search')): ?>

					<div class="col-md-6">

						<input type="text" name="where_search" id="where_search_<?php echo $random_id; ?>" class="form-control form-group" size="38" placeholder="<?php _e('Enter address or zip code', 'W2DC'); ?>" value="<?php if (isset($_GET['where_search'])) echo esc_attr(stripslashes($_GET['where_search'])); ?>" />

					</div>

					<?php endif; ?>

					<?php do_action('post_search_where_form_html', $random_id); ?>

				</div>

			</div>

			<?php endif; ?>

			

			<?php do_action('post_search_form_html', $random_id); ?>

			

			<div class="row text-right">

				<div class="col-md-2 pull-right">

					<input type="submit" name="submit" class="btn btn-primary" value="<?php _e('Buscar', 'W2DC'); ?>" />

				</div>

				

				<?php do_action('buttons_search_form_html', $random_id); ?>

			</div>

		</div>

	</div>

</form>

<?php endif; ?>