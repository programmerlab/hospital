	<script>
		jQuery(document).ready( function() {
			<?php if (isset($_GET['compare_palettes'])): ?>
			jQuery.cookie("w2dc_compare_palettes", '<?php echo get_option('w2dc_color_scheme'); ?>', {expires: 1, path: "/"});
			<?php endif; ?>

			jQuery('#w2dc_color_picker_panel').hover(function () {
				jQuery(this).stop().animate({left: "0px"}, 500);
			}, function () {
				var width = jQuery(this).width() - 50;
				jQuery(this).stop().animate({left: - width}, 500);
			});
		});
	</script>
	<div id="w2dc_color_picker_panel" class="w2dc_content">
		<fieldset id="w2dc_color_picker">
			<label><?php _e('Choose color palette:'); ?></label>
			<?php $selected_scheme = (isset($_COOKIE['w2dc_compare_palettes']) ? $_COOKIE['w2dc_compare_palettes'] : get_option('w2dc_color_scheme')); ?>
			<?php w2dc_renderTemplate('color_picker/color_picker_settings.tpl.php', array('selected_scheme' => $selected_scheme)); ?>
			<label><?php printf(__('Return to the <a href="%s">backend</a>', 'W2DC'), admin_url('admin.php?page=w2dc_settings')); ?></label>
		</fieldset>
		<div id="w2dc_color_picker_panel_tools" class="clearfix">
			<img src="<?php echo W2DC_RESOURCES_URL . 'images/icons_by_Designmodo/settings.png'; ?>" />
		</div>
	</div>