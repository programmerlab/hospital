<div class="w2dc_directory_frontpanel">
	<?php do_action('w2dc_directory_frontpanel', (isset($listing)) ? $listing : null); ?>

	<?php if (get_option('w2dc_favourites_list') && $w2dc_instance->action != 'myfavourites'): ?>
	<input type="button" class="w2dc_favourites_link btn btn-info" value="<?php _e('My favourites', 'W2DC'); ?>" onClick="window.location='<?php echo w2dc_directoryUrl(array('w2dc_action' => 'myfavourites')); ?>';" />
	<?php endif; ?>

	<?php if (isset($listing)): ?>
		<?php if (current_user_can('edit_post', $listing->post->ID)): ?>
		<input type="button" class="w2dc_edit_listing_link btn btn-info" value="<?php _e('Edit listing', 'W2DC'); ?>" onClick="window.location='<?php echo get_edit_post_link($listing->post->ID); ?>';" />
		<?php endif; ?>
	
		<?php if (get_option('w2dc_print_button')): ?>
		<script>
			var window_width = 860;
			var window_height = 800;
			var leftPosition, topPosition;
		   	leftPosition = (window.screen.width / 2) - ((window_width / 2) + 10);
		   	topPosition = (window.screen.height / 2) - ((window_height / 2) + 50);
		</script>
		<input type="button" class="w2dc_print_listing_link btn btn-info" value="<?php _e('Print listing', 'W2DC'); ?>" onClick="window.open('<?php echo add_query_arg('w2dc_action', 'printlisting', get_permalink($listing->post->ID)); ?>', 'print_window', 'height='+window_height+',width='+window_width+',left='+leftPosition+',top='+topPosition+',menubar=yes,scrollbars=yes');" />
		<?php endif; ?>
	
		<?php if (get_option('w2dc_favourites_list')): ?>
		<input type="button" class="w2dc_save_listing_link add_to_favourites btn btn-info <?php if (checkQuickList($listing->post->ID)) 'in_favourites_list'; else 'not_in_favourites_list'; ?>" value="<?php if (checkQuickList(get_the_ID())) _e('Out of favourites list', 'W2DC'); else _e('Put in favourites list', 'W2DC'); ?>" listingid="<?php echo $listing->post->ID; ?>" />
		<?php endif; ?>
	
		<?php if (get_option('w2dc_pdf_button')): ?>
		<input type="button" class="w2dc_pdf_listing_link btn btn-info" value="<?php _e('Save listing in PDF', 'W2DC'); ?>" onClick="window.open('http://pdfmyurl.com/?url=<?php echo urlencode(add_query_arg('w2dc_action', 'pdflisting', get_permalink($listing->post->ID))); ?>');" />
		<?php endif; ?>
	<?php endif; ?>
</div>