	<div itemscope itemtype="http://schema.org/WebPage">

		<?php if (!$is_single): ?>
			<?php if ($listing->level->featured): ?>
			<div class="w2dc_featured_ribbon"><?php _e('Featured', 'W2DC'); ?></div>
			<?php endif; ?>

			<?php if ($listing->level->sticky && $w2dc_instance->order_by_date): ?>
			<div class="w2dc_sticky_icon" title="<?php _e('sticky listing', 'W2DC'); ?>"></div>
			<?php endif; ?>
			
			<?php if (checkQuickList($listing->post->ID)): ?>
			<div class="w2dc_remove_from_favourites_list" listingid="<?php the_ID(); ?>">
				<a href="javascript: void(0);" title="<?php echo esc_attr(__('Remove from favourites list', 'W2DC')); ?>"><img class="w2dc_field_icon" src="<?php echo W2DC_RESOURCES_URL; ?>images/delete.png" /> <?php _e('Remove from favourites list', 'W2DC'); ?></a>
			</div>
			<?php endif; ?>
		<?php endif; ?>

		<?php if ($listing->logo_image): ?>
			<div class="listing_logo_wrap <?php if ($is_single) echo "listing_logo_size_single"; else echo "listing_logo_size_" . $listing->level->logo_size; ?>">
				<div id="listing_logo" class="listing_logo">
					<?php
					$src_full = wp_get_attachment_image_src($listing->logo_image, 'full');
					if ($is_single)
						$src = $src_full;
					else
						$src = wp_get_attachment_image_src($listing->logo_image, $listing->level->logo_size);
					?>
					<?php if ($is_single && get_option('w2dc_images_on_tab')): ?>
						<img src="<?php echo $src[0]; ?>" itemprop="image" />
					<?php elseif ($is_single): ?>
						<a href="<?php echo $src_full[0]; ?>" data-lightbox="listing_images">
							<img src="<?php echo $src[0]; ?>" itemprop="image" />
						</a>
					<?php else: ?>
						<?php if (get_option('w2dc_listings_own_page')): ?>
						<a href="<?php the_permalink(); ?>">
						<?php endif; ?>
							<img src="<?php echo $src[0]; ?>" itemprop="image" />
						<?php if (get_option('w2dc_listings_own_page')): ?>
						</a>
						<?php endif; ?>
					<?php endif; ?>
				</div>

				<?php if ($is_single && !get_option('w2dc_images_on_tab') && count($listing->images) > 1): ?>
				<?php w2dc_renderTemplate('frontend/images_gallery_js.tpl.php', array('listing' => $listing)); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if (!$is_single): ?>
			<header class="w2dc_listing_header" itemprop="name">
				<?php if (!get_option('w2dc_listings_own_page')): ?>
				<?php echo $listing->title(); ?>
				<?php else: ?>
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr($listing->title()); ?>"><?php echo $listing->title(); ?></a>
				<?php endif; ?>
				<?php do_action('w2dc_listing_title_html', $listing); ?>
			</header>
		<?php endif; ?>

			<div class="listing_text_content_wrap clearfix">
				<?php do_action('w2dc_listing_pre_content_html', $listing); ?>

				<?php if (!$is_single && comments_open() && !get_option('w2dc_hide_comments_number_on_index')): ?>
				<div>
					<img src="<?php echo W2DC_RESOURCES_URL; ?>images/comments.png" class="w2dc_field_icon" />
					<?php echo sprintf(_n('%d reply', '%d replies', $listing->post->comment_count, 'W2DC'), $listing->post->comment_count);?>
				</div>
				<?php endif; ?>

				<?php if (!get_option('w2dc_hide_listings_creation_date')): ?>
				<em class="w2dc_listing_date" itemprop="dateCreated" datetime="<?php echo date("Y-m-d", mysql2date('U', $listing->post->post_date)); ?>T<?php echo date("H:i", mysql2date('U', $listing->post->post_date)); ?>"><?php echo get_the_date(); ?> <?php echo get_the_time(); ?> <?php //echo human_time_diff(mysql2date('U', $listing->post->post_date), time()); ?></em>
				<?php endif; ?>
			
				<?php $listing->renderContentFields($is_single); ?>
				
				<?php do_action('w2dc_listing_post_content_html', $listing); ?>
			</div>

		<?php if ($is_single): ?>
			<script>
				jQuery(document).ready(function($) {
					$('.w2dc_listing_tabs a').click(function (e) {
						  e.preventDefault();
						  $(this).tab('show');
					});
					$('.w2dc_listing_tabs a:first').tab('show');

					$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
						  if (e.target.hash.substring(1) == 'addresses-tab')
							  for (var key in maps)
									if (typeof maps[key] != 'undefined') {
										var zoom = maps[key].getZoom();
										var center = maps[key].getCenter();
										google.maps.event.trigger(maps[key], 'resize');
										maps[key].setZoom(zoom);
										maps[key].setCenter(center);
									}
					})

					var hash = window.location.hash.substring(1);
					if (hash == 'respond' || hash.indexOf('comment-', 0) >= 0)
						$('.w2dc_listing_tabs a[href="#comments-tab"]').tab('show');
					if (hash == 'contact')
						$('.w2dc_listing_tabs a[href="#contact-tab"]').tab('show');
				});
			</script>

			<?php if (
				   ($listing->level->google_map && $listing->isMap() && $listing->locations)
				|| (comments_open())
				|| ($listing->level->images_number && count($listing->images) > 1 && get_option('w2dc_images_on_tab'))
				|| ($listing->level->videos_number && $listing->videos)
				|| (get_option('w2dc_listing_contact_form'))
				): ?>
			<ul class="w2dc_listing_tabs nav nav-tabs">
				<?php if ($listing->level->google_map && $listing->isMap() && $listing->locations): ?>
				<li><a href="#addresses-tab" data-toggle="tab"><?php _e('Map', 'W2DC'); ?></a></li>
				<?php endif; ?>
				<?php if (comments_open()): ?>
				<li><a href="#comments-tab" data-toggle="tab"><?php echo _n('Comment', 'Comments', $listing->post->comment_count, 'W2DC'); ?> (<?php echo $listing->post->comment_count; ?>)</a></li>
				<?php endif; ?>
				<?php if ($listing->level->images_number && count($listing->images) > 1 && get_option('w2dc_images_on_tab')): ?>
				<li><a href="#images-tab" data-toggle="tab"><?php echo _n('Image', 'Images', count($listing->images), 'W2DC'); ?> (<?php echo count($listing->images); ?>)</a></li>
				<?php endif; ?>
				<?php if ($listing->level->videos_number && $listing->videos): ?>
				<li><a href="#videos-tab" data-toggle="tab"><?php echo _n('Video', 'Videos', count($listing->videos), 'W2DC'); ?> (<?php echo count($listing->videos); ?>)</a></li>
				<?php endif; ?>
				<?php if (get_option('w2dc_listing_contact_form') && ($listing_owner = get_userdata($listing->post->post_author)) && $listing_owner->user_email): ?>
				<li><a href="#contact-tab" data-toggle="tab"><?php _e('Contact', 'W2DC'); ?></a></li>
				<?php endif; ?>
			</ul>

			<div class="tab-content">
				<?php if ($listing->level->google_map && $listing->isMap() && $listing->locations): ?>
				<div id="addresses-tab" class="tab-pane">
					<?php $listing->renderMap(get_option('w2dc_show_directions'), false, get_option('w2dc_enable_radius_search_cycle'), get_option('w2dc_enable_clusters'), false, false); ?>
				</div>
				<?php endif; ?>
	
				<?php if (comments_open()): ?>
				<div id="comments-tab" class="tab-pane">
					<?php comments_template('', true); ?>
				</div>
				<?php endif; ?>
	
				<?php if ($listing->level->images_number && count($listing->images) > 1 && get_option('w2dc_images_on_tab')): ?>
				<div id="images-tab" class="tab-pane">
					<?php w2dc_renderTemplate('frontend/images_gallery_carousel.tpl.php', array('listing' => $listing)); ?>
				</div>
				<?php endif; ?>
	
				<?php if ($listing->level->videos_number && $listing->videos): ?>
				<div id="videos-tab" class="tab-pane">
				<?php foreach ($listing->videos AS $video): ?>
					<span><?php echo $video['caption']; ?></span>
					<object width="100%" height="400" data="http://www.youtube.com/v/<?php echo $video['id']; ?>" type="application/x-shockwave-flash"><param name="src" value="http://www.youtube.com/v/<?php echo $video['id']; ?>" /></object>
				<?php endforeach; ?>
				</div>
				<?php endif; ?>
	
				<?php if (get_option('w2dc_listing_contact_form') && ($listing_owner = get_userdata($listing->post->post_author)) && $listing_owner->user_email): ?>
				<div id="contact-tab" class="tab-pane">
				<?php if (defined('WPCF7_VERSION') && get_option('w2dc_listing_contact_form_7')): ?>
					<?php echo do_shortcode(get_option('w2dc_listing_contact_form_7')); ?>
				<?php else: ?>
					<?php w2dc_renderTemplate('frontend/contact_form.tpl.php', array('listing' => $listing)); ?>
				<?php endif; ?>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		<?php endif; ?>
		
	</div>