			<div class="w2dc_listings_block w2dc_content">
				<?php if ((!isset($frontend_controller->hide_order) || !$frontend_controller->hide_order)
						|| ($frontend_controller->query->found_posts > 1 && (!isset($frontend_controller->hide_count) || !$frontend_controller->hide_count))): ?>
				<div class="w2dc_listings_block_header">
					<?php if (!isset($frontend_controller->hide_count) || !$frontend_controller->hide_count): ?>
					<div id="w2dc_found_listings">
						<?php echo sprintf(_n('Found <b>%d</b> listing', 'Found <b>%d</b> listings', $frontend_controller->query->found_posts, 'W2DC'), $frontend_controller->query->found_posts); ?>
					</div>
					<?php endif; ?>
		
					<?php if ($frontend_controller->query->found_posts > 1 && (!isset($frontend_controller->hide_order) || !$frontend_controller->hide_order)): ?>
					<div id="w2dc_orderby_links">
						<?php if ($frontend_controller->query->found_posts) echo w2dc_orderLinks($frontend_controller->base_url, $frontend_controller->args); ?>
					</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			
				<?php if ($frontend_controller->listings): ?>
					<?php while ($frontend_controller->query->have_posts()): ?>
					<?php $frontend_controller->query->the_post(); ?>
					<article id="post-<?php the_ID(); ?>" class="w2dc_listing <?php if ($frontend_controller->listings[get_the_ID()]->level->featured) echo 'w2dc_featured'; ?>">
						<?php $frontend_controller->listings[get_the_ID()]->display(); ?>
					</article>
					<?php endwhile; ?>
						
					<?php renderPaginator($frontend_controller->query); ?>
				<?php endif; ?>
			</div>