		<div class="w2dc_content">
			<?php w2dc_renderMessages(); ?>

			<?php w2dc_renderTemplate('frontend/frontpanel_buttons.tpl.php'); ?>
			
			<?php if ($frontend_controller->getPageTitle()): ?>
			<header>
				<h2>
					<?php echo $frontend_controller->getPageTitle(); ?>
				</h2>

				<?php if ($frontend_controller->breadcrumbs): ?>
				<div class="breadcrumbs">
					<?php echo $frontend_controller->getBreadCrumbs(); ?>
				</div>
				<?php endif; ?>
			</header>
			<?php endif; ?>

			<div class="w2dc_listings_block">
				<div class="w2dc_listings_block_header">
					<div id="w2dc_found_listings">
						<?php echo sprintf(_n('Found %d listing', 'Found %d listings', $frontend_controller->query->found_posts, 'W2DC'), $frontend_controller->query->found_posts); ?>
					</div>
				</div>

				<?php if ($frontend_controller->listings): ?>
					<?php while ($frontend_controller->query->have_posts()): ?>
					<?php $frontend_controller->query->the_post(); ?>
					<article id="post-<?php the_ID(); ?>" class="w2dc_listing <?php if ($frontend_controller->listings[get_the_ID()]->level->featured) echo 'w2dc_featured'; ?>">
						<?php $frontend_controller->listings[get_the_ID()]->display(); ?>
					</article>
					<?php endwhile; ?>
					
					<?php renderPaginator($frontend_controller->query); ?>
				<?php endif; ?>
			</div>
		</div>