<?php 

class w2dc_ajax_controller {

	public function __construct() {
		global $w2dc_instance;

		add_action('wp_ajax_w2dc_address_autocomplete', array($this, 'address_autocomplete'));
		add_action('wp_ajax_nopriv_w2dc_address_autocomplete', array($this, 'address_autocomplete'));
		
		add_action('wp_ajax_w2dc_get_map_markers', array($this, 'get_map_markers'));
		add_action('wp_ajax_nopriv_w2dc_get_map_markers', array($this, 'get_map_markers'));

		add_action('wp_ajax_w2dc_get_map_marker_info', array($this, 'get_map_marker_info'));
		add_action('wp_ajax_nopriv_w2dc_get_map_marker_info', array($this, 'get_map_marker_info'));
	}

	public function address_autocomplete() {
		if (isset($_POST['term']) && $_POST['term']) {
			$term = trim($_POST['term']);
			
			global $wpdb;
			$output = array();
			$results = $wpdb->get_results($wpdb->prepare("SELECT address_line_1 FROM {$wpdb->locations_relationships} WHERE address_line_1 LIKE '%%%s%%'", $term), ARRAY_A);
			foreach ($results AS $row) {
				$address = $row['address_line_1']; 
				$output[] = array('id' => $address, 'label' => $address, 'value' => $address);
			}
			$results = $wpdb->get_results($wpdb->prepare("SELECT address_line_2 FROM {$wpdb->locations_relationships} WHERE address_line_2 LIKE '%%%s%%'", $term), ARRAY_A);
			foreach ($results AS $row) {
				$address = $row['address_line_2']; 
				$output[] = array('id' => $address, 'label' => $address, 'value' => $address);
			}
			$results = $wpdb->get_results($wpdb->prepare("SELECT zip_or_postal_index FROM {$wpdb->locations_relationships} WHERE zip_or_postal_index LIKE '%%%s%%'", $term), ARRAY_A);
			foreach ($results AS $row) {
				$address = $row['zip_or_postal_index']; 
				$output[] = array('id' => $address, 'label' => $address, 'value' => $address);
			}
			echo json_encode($output);
		}
		die();
	}
	
	public function get_map_markers() {
		$args = $_POST;
		if (isset($args['neLat']) && isset($args['neLng']) && isset($args['swLat']) && isset($args['swLng'])) {
			// needed to unset 'ajax_loading' parameter when it is calling by AJAX, then $args will be passed to map controller
			if (isset($args['ajax_loading']))
				unset($args['ajax_loading']);

			$controller = new w2dc_map_controller($args);
	
			$result_locations = array();
			foreach ($controller->google_map->locations_option_array AS $key=>$location) {
				$y1 = $args['neLat'];
				$y2 = $args['swLat'];
				
				// when zoom level 2 - there may be problems with neLng and swLng of bounds
				if ($args['neLng'] > $args['swLng']) {
					$x1 = $args['neLng'];
					$x2 = $args['swLng'];
				} else {
					$x1 = 180;
					$x2 = -180;
				}

				if ($location[1] >= $y2 && $location[1] <= $y1 && $location[2] >= $x2 && $location[2] <= $x1)
					$result_locations[] = $location;
			}
			echo json_encode($result_locations);
		}
		die();
	}
	
	public function get_map_marker_info() {
		global $w2dc_instance, $wpdb;

		if (isset($_POST['location_id']) && is_numeric($_POST['location_id'])) {
			$location_id = $_POST['location_id'];

			$row = $wpdb->get_row("SELECT * FROM {$wpdb->locations_relationships} WHERE id=".$location_id, ARRAY_A);

			if ($row && $row['location_id'] || $row['map_coords_1'] != '0.000000' || $row['map_coords_2'] != '0.000000' || $row['address_line_1'] || $row['zip_or_postal_index']) {
				$listing = new w2dc_listing;
				if ($listing->loadListingFromPost($row['post_id'])) {
					$location = new w2dc_location($row['post_id']);
					$location_settings = array(
							'id' => $row['id'],
							'selected_location' => $row['location_id'],
							'address_line_1' => $row['address_line_1'],
							'address_line_2' => $row['address_line_2'],
							'zip_or_postal_index' => $row['zip_or_postal_index'],
					);
					if ($listing->level->google_map) {
						$location_settings['manual_coords'] = w2dc_getValue($row, 'manual_coords');
						$location_settings['map_coords_1'] = w2dc_getValue($row, 'map_coords_1');
						$location_settings['map_coords_2'] = w2dc_getValue($row, 'map_coords_2');
						if ($listing->level->google_map_markers)
							$location_settings['map_icon_file'] = w2dc_getValue($row, 'map_icon_file');
					}
					$location->createLocationFromArray($location_settings);
						
					$logo_image = '';
					if ($listing->logo_image) {
						$src = wp_get_attachment_image_src($listing->logo_image, array(80, 80));
						$logo_image = $src[0];
					}
						
					$listing_link = '';
					if (get_option('w2dc_listings_own_page'))
						$listing_link = get_permalink($listing->post->ID);
						
					$content_fields_output = $listing->setMapContentFields($w2dc_instance->content_fields->getMapContentFields(), $location);

					$locations_option_array = array(
							$location->id,
							$location->map_coords_1,
							$location->map_coords_2,
							$location->map_icon_file,
							$listing->map_zoom,
							$listing->title(),
							$logo_image,
							$listing_link,
							$content_fields_output,
							'post-' . $listing->post->ID,
					);
						
					echo json_encode($locations_option_array);
				}
			}
		}
		die();
	}
}
?>