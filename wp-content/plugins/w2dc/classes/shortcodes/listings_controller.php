<?php 

class w2dc_listings_controller extends w2dc_frontend_controller {
	private $levels_ids;
	
	public function __construct($args = array()) {
		global $w2dc_instance;
	
		if (get_query_var('page'))
			$paged = get_query_var('page');
		elseif (get_query_var('paged'))
			$paged = get_query_var('paged');
		else
			$paged = 1;
		
		$shortcode_atts = array_merge(array(
				'perpage' => 10,
				'onepage' => 0,
				'sticky_featured' => 0,
				'order_by' => 'post_date',
				'order' => 'ASC',
				'hide_order' => 0,
				'hide_count' => 0,
				'template' => 'frontend/listings_block.tpl.php',
		), $args);

		$this->args = $shortcode_atts;
		
		$this->hide_order = $shortcode_atts['hide_order'];
		$this->hide_count = $shortcode_atts['hide_count'];
		
		$this->base_url = get_permalink();

		$this->template = $this->args['template'];

		$args = array(
				'post_type' => W2DC_POST_TYPE,
				'post_status' => 'publish',
				'meta_query' => array(array('key' => '_listing_status', 'value' => 'active')),
				'posts_per_page' => $shortcode_atts['perpage'],
				'paged' => $paged,
		);
		$args = array_merge($args, apply_filters('w2dc_order_args', array(), $shortcode_atts));

		$args = apply_filters('w2dc_search_args', $args, $this->args);

		if (isset($this->args['post_in'])) {
			$args = array_merge($args, array('post_in' => $this->args['post_in']));
		}
		
		if (isset($this->args['levels']) && !is_array($this->args['levels'])) {
			if ($levels = array_filter(explode(',', $this->args['levels']), 'trim')) {
				$this->levels_ids = $levels;
				add_filter('posts_where', array($this, 'where_levels_ids'));
			}
		}

		if (isset($this->args['levels']) || $this->args['sticky_featured']) {
			add_filter('posts_join', 'join_levels');
			if ($this->args['sticky_featured'])
				add_filter('posts_where', 'where_sticky_featured');
		}
		$this->query = new WP_Query($args);

		// render just one page
		if ($this->args['onepage'])
			$this->query->max_num_pages = 1;

		$this->processQuery(false);

		if ($this->args['sticky_featured']) {
			remove_filter('posts_join', 'join_levels');
			remove_filter('posts_where', 'where_sticky_featured');
		}

		if ($this->levels_ids)
			remove_filter('posts_where', array($this, 'where_levels_ids'));
		
		apply_filters('w2dc_frontend_controller_construct', $this);
	}
	
	public function where_levels_ids($where = '') {
		if ($this->levels_ids)
			$where .= " AND (w2dc_levels.id IN (" . implode(',', $this->levels_ids) . "))";
		return $where;
	}
	
	public function display() {
		$output =  w2dc_renderTemplate($this->template, array('frontend_controller' => $this), true);
		wp_reset_postdata();
	
		return $output;
	}
}

?>