<?php 

class w2dc_map_controller extends w2dc_frontend_controller {
	private $levels_ids;
	
	public function __construct($args) {
		global $w2dc_instance;

		$shortcode_atts = array_merge(array(
				'num' => -1,
				'radius_cycle' => 0,
				'clusters' => 0,
				'show_summary_button' => 0,
				'show_readmore_button' => 1,
				'sticky_featured' => 0,
				'ajax_loading' => 0,
				'ajax_markers_loading' => 0,
				'geolocation' => 0,
				'start_address' => '',
				'start_latitude' => '',
				'start_longitude' => '',
				'start_zoom' => '',
		), $args);
		$this->args = $shortcode_atts;

		$args = array(
				'post_type' => W2DC_POST_TYPE,
				'post_status' => 'publish',
				'meta_query' => array(array('key' => '_listing_status', 'value' => 'active')),
				'posts_per_page' => $shortcode_atts['num'],
		);
		$args = apply_filters('w2dc_search_args', $args, $this->args);
		
		if (isset($this->args['post_in'])) {
			$args = array_merge($args, array('post_in' => $this->args['post_in']));
		}

		if (isset($this->args['levels']) && !is_array($this->args['levels'])) {
			if ($levels = array_filter(explode(',', $this->args['levels']), 'trim')) {
				$this->levels_ids = $levels;
				add_filter('posts_join', 'join_levels');
				add_filter('posts_where', array($this, 'where_levels_ids'));
			}
		}
		
		if ($this->args['sticky_featured']) {
			add_filter('posts_join', 'join_levels');
			add_filter('posts_where', 'where_sticky_featured');
		}

		if (isset($this->args['neLat']) && isset($this->args['neLng']) && isset($this->args['swLat']) && isset($this->args['swLng'])) {
			$y1 = $this->args['neLat'];
			$y2 = $this->args['swLat'];
			// when zoom level 2 - there may be problems with neLng and swLng of bounds
			if ($this->args['neLng'] > $this->args['swLng']) {
				$x1 = $this->args['neLng'];
				$x2 = $this->args['swLng'];
			} else {
				$x1 = 180;
				$x2 = -180;
			}
			
			global $wpdb;
			$results = $wpdb->get_results($wpdb->prepare(
				"SELECT DISTINCT
					post_id FROM {$wpdb->locations_relationships}
				WHERE
					map_coords_1 >= %f 
				AND
					map_coords_1 <= %f
				AND
					map_coords_2 >= %f
				AND
					map_coords_2 <= %f", $y2, $y1, $x2, $x1), ARRAY_A);

			$post_ids = array();
			foreach ($results AS $row)
				$post_ids[] = $row['post_id'];
			$post_ids = array_unique($post_ids);
			
			if ($post_ids) {
				if (isset($args['post_in']))
					$args = array_merge($post_ids, $args['post_in']);
				else
					$args['post__in'] = $post_ids;
			} else
				// Do not show any listings
				$args['post__in'] = array(0);
		}

		$this->google_map = new google_maps($this->args);

		if (!$this->google_map->is_ajax_markers_management()) {
			$this->query = new WP_Query($args);
			$this->processQuery($this->args['ajax_markers_loading']);
		}
		
		if ($this->args['sticky_featured']) {
			remove_filter('posts_join', 'join_levels');
			remove_filter('posts_where', 'where_sticky_featured');
		}

		if ($this->levels_ids) {
			remove_filter('posts_join', 'join_levels');
			remove_filter('posts_where', array($this, 'where_levels_ids'));
		}
		
		apply_filters('w2dc_frontend_controller_construct', $this);
	}
	
	public function processQuery($is_ajax_map = false, $map_args = array()) {
		while ($this->query->have_posts()) {
			$this->query->the_post();

			$listing = new w2dc_listing;
			if (!$is_ajax_map) {
				$listing->loadListingForMap(get_post());
				$this->google_map->collectLocations($listing);
			} else {
				$listing->loadListingForAjaxMap(get_post());
				$this->google_map->collectLocationsForAjax($listing);
			}

			$this->listings[get_the_ID()] = $listing;
		}
		// this is reset is really required after the loop ends
		wp_reset_postdata();
	}
	
	public function where_levels_ids($where = '') {
		if ($this->levels_ids)
			$where .= " AND (w2dc_levels.id IN (" . implode(',', $this->levels_ids) . "))";
		return $where;
	}

	public function display() {
		$width = false;
		$height = false;
		if (isset($this->args['width']))
			$width = $this->args['width'];
		if (isset($this->args['height']))
			$height = $this->args['height'];
		
		ob_start();
		$this->google_map->display(false, false, $this->args['radius_cycle'], $this->args['clusters'], $this->args['show_summary_button'], $this->args['show_readmore_button'], $width, $height);
		$output = ob_get_clean();

		wp_reset_postdata();
	
		return $output;
	}
}

?>