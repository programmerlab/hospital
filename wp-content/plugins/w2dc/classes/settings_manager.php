<?php 

class w2dc_settings_manager {
	public function __construct() {
		add_action('admin_menu', array($this, 'menu'));
		add_action('admin_init', array($this, 'register_settings'));
	}

	public function menu() {
		add_submenu_page('w2dc_admin',
				__('Directory settings', 'W2DC'),
				__('Directory settings', 'W2DC'),
				'administrator',
				'w2dc_settings',
				array($this, 'w2dc_settings_page')
		);
	}
	
	public function register_settings() {
		// General settings page /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_general_section',
				__('General settings', 'W2DC'),
				null,
				'w2dc_settings_page'
		);
		add_settings_field(
				'w2dc_directory_title',
				__('Directory title', 'W2DC'),
				array($this, 'w2dc_directory_title_callback'),
				'w2dc_settings_page',
				'w2dc_general_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_directory_title');
		add_settings_field(
				'w2dc_category_slug',
				__('Category slug', 'W2DC'),
				array($this, 'w2dc_category_slug_callback'),
				'w2dc_settings_page',
				'w2dc_general_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_category_slug');
		add_filter('pre_update_option_w2dc_category_slug', array($this, 'update_category_slug'), 10, 2);
		add_settings_field(
				'w2dc_tag_slug',
				__('Tag slug', 'W2DC'),
				array($this, 'w2dc_tag_slug_callback'),
				'w2dc_settings_page',
				'w2dc_general_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_tag_slug');
		add_filter('pre_update_option_w2dc_tag_slug', array($this, 'update_tag_slug'), 10, 2);
		
		add_settings_field(
				'w2dc_color_scheme',
				__('Frontend color palette', 'W2DC'),
				array($this, 'w2dc_color_scheme_callback'),
				'w2dc_settings_page',
				'w2dc_general_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_color_scheme');

		// Recaptcha settings page /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_recaptcha_section',
				__('reCaptcha settings', 'W2DC'),
				null,
				'w2dc_settings_page'
		);
		add_settings_field(
				'w2dc_enable_recaptcha',
				__('Enable reCaptcha', 'W2DC'),
				array($this, 'w2dc_enable_recaptcha_callback'),
				'w2dc_settings_page',
				'w2dc_recaptcha_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_enable_recaptcha');
		add_settings_field(
				'w2dc_recaptcha_public_key',
				__('reCaptcha public key', 'W2DC'),
				array($this, 'w2dc_recaptcha_public_key_callback'),
				'w2dc_settings_page',
				'w2dc_recaptcha_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_recaptcha_public_key');
		add_settings_field(
				'w2dc_recaptcha_private_key',
				__('reCaptcha private key', 'W2DC'),
				array($this, 'w2dc_recaptcha_private_key_callback'),
				'w2dc_settings_page',
				'w2dc_recaptcha_section'
		);
		register_setting('w2dc_settings_page', 'w2dc_recaptcha_private_key');

		// Pages & Views settings page /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_categories_section',
				__('Categories settings', 'W2DC'),
				null,
				'w2dc_pagesviews_settings_page'
		);
		add_settings_field(
				'w2dc_show_categories_index',
				__('Show categories list on index and excerpt pages?', 'W2DC'),
				array($this, 'w2dc_show_categories_index_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_categories_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_categories_index');
		add_settings_field(
				'w2dc_categories_nesting_level',
				__('Categories nesting level', 'W2DC'),
				array($this, 'w2dc_categories_nesting_level_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_categories_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_categories_nesting_level');
		add_settings_field(
				'w2dc_categories_columns',
				__('Categories columns number', 'W2DC'),
				array($this, 'w2dc_categories_columns_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_categories_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_categories_columns');
		add_settings_field(
				'w2dc_subcategories_items',
				__('Show subcategories items number', 'W2DC'),
				array($this, 'w2dc_subcategories_items_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_categories_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_subcategories_items');
		add_settings_field(
				'w2dc_show_category_count',
				__('Show category listings count?', 'W2DC'),
				array($this, 'w2dc_show_category_count_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_categories_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_category_count');

		// Search settings /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_search_section',
				__('Search settings', 'W2DC'),
				null,
				'w2dc_pagesviews_settings_page'
		);
		add_settings_field(
				'w2dc_main_search',
				__('Display search block in main part of page?', 'W2DC'),
				array($this, 'w2dc_main_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_main_search');
		add_settings_field(
				'w2dc_show_what_search',
				__('Show "What search" section?', 'W2DC'),
				array($this, 'w2dc_show_what_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_what_search');
		add_settings_field(
				'w2dc_show_where_search',
				__('Show "Where search" section?', 'W2DC'),
				array($this, 'w2dc_show_where_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_where_search');
		add_settings_field(
				'w2dc_show_keywords_search',
				__('Show keywords search?', 'W2DC'),
				array($this, 'w2dc_show_keywords_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_keywords_search');
		add_settings_field(
				'w2dc_show_locations_search',
				__('Show locations search?', 'W2DC'),
				array($this, 'w2dc_show_locations_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_locations_search');
		add_settings_field(
				'w2dc_show_address_search',
				__('Show address search?', 'W2DC'),
				array($this, 'w2dc_show_address_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_address_search');
		add_settings_field(
				'w2dc_show_location_count_in_search',
				__('Show listings counts in locations search dropboxes?', 'W2DC'),
				array($this, 'w2dc_show_location_count_in_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_location_count_in_search');
		
		// Sorting settings /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_sorting_section',
				__('Sorting settings', 'W2DC'),
				null,
				'w2dc_pagesviews_settings_page'
		);
		add_settings_field(
				'w2dc_show_orderby_links',
				__('Show order by links block', 'W2DC'),
				array($this, 'w2dc_show_orderby_links_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_sorting_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_orderby_links');
		add_settings_field(
				'w2dc_orderby_date',
				__('Allow sorting by date', 'W2DC'),
				array($this, 'w2dc_orderby_date_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_sorting_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_orderby_date');
		add_settings_field(
				'w2dc_orderby_title',
				__('Allow sorting by title', 'W2DC'),
				array($this, 'w2dc_orderby_title_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_sorting_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_orderby_title');
		add_settings_field(
				'w2dc_default_orderby',
				__('Default order by', 'W2DC'),
				array($this, 'w2dc_default_orderby_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_sorting_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_default_orderby');
		add_settings_field(
				'w2dc_default_order',
				__('Default order', 'W2DC'),
				array($this, 'w2dc_default_order_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_sorting_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_default_order');

		// Listings settings page /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_listings_section',
				__('Listings settings', 'W2DC'),
				null,
				'w2dc_listings_settings_page'
		);
		add_settings_field(
				'w2dc_listings_on_index',
				__('Show listings on index', 'W2DC'),
				array($this, 'w2dc_listings_on_index_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_listings_on_index');
		add_settings_field(
				'w2dc_listings_number_index',
				__('Number of listings on index page', 'W2DC'),
				array($this, 'w2dc_listings_number_index_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_listings_number_index');
		add_settings_field(
				'w2dc_listings_number_excerpt',
				__('Number of listings on excerpt page', 'W2DC'),
				array($this, 'w2dc_listings_number_excerpt_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_listings_number_excerpt');
		add_settings_field(
				'w2dc_listings_own_page',
				__('Do listings have own pages?', 'W2DC'),
				array($this, 'w2dc_listings_own_page_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_listings_own_page');
		add_settings_field(
				'w2dc_images_on_tab',
				__('Place listing images gallery on separate tab?', 'W2DC'),
				array($this, 'w2dc_images_on_tab_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_images_on_tab');
		add_settings_field(
				'w2dc_images_lightbox',
				__('Enable lightbox slideshow?', 'W2DC'),
				array($this, 'w2dc_images_lightbox_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_images_lightbox');
		add_settings_field(
				'w2dc_listing_contact_form',
				__('Enable contact form on listing page', 'W2DC'),
				array($this, 'w2dc_listing_contact_form_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_listing_contact_form');
		add_settings_field(
				'w2dc_listing_contact_form_7',
				__('Contact Form 7 shortcode', 'W2DC'),
				array($this, 'w2dc_listing_contact_form_7_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_listing_contact_form_7');
		add_settings_field(
				'w2dc_favourites_list',
				__('Enable favourites list', 'W2DC'),
				array($this, 'w2dc_favourites_list_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_favourites_list');
		add_settings_field(
				'w2dc_print_button',
				__('Show print listing button', 'W2DC'),
				array($this, 'w2dc_print_button_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_print_button');
		add_settings_field(
				'w2dc_pdf_button',
				__('Show listing in PDF button', 'W2DC'),
				array($this, 'w2dc_pdf_button_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_pdf_button');
		add_settings_field(
				'w2dc_change_expiration_date',
				__('Allow users to change listings expiration dates', 'W2DC'),
				array($this, 'w2dc_change_expiration_date_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_change_expiration_date');
		add_settings_field(
				'w2dc_hide_comments_number_on_index',
				__('Hide comments number from index and excerpt pages', 'W2DC'),
				array($this, 'w2dc_hide_comments_number_on_index_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_hide_comments_number_on_index');
		add_settings_field(
				'w2dc_hide_listings_creation_date',
				__('Hide listings creation date', 'W2DC'),
				array($this, 'w2dc_hide_listings_creation_date_callback'),
				'w2dc_listings_settings_page',
				'w2dc_listings_section'
		);
		register_setting('w2dc_listings_settings_page', 'w2dc_hide_listings_creation_date');


		// Maps settings page /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_maps_section',
				__('Maps settings', 'W2DC'),
				null,
				'w2dc_maps_settings_page'
		);
		add_settings_field(
				'w2dc_map_on_index',
				__('Show map on index page?', 'W2DC'),
				array($this, 'w2dc_map_on_index_callback'),
				'w2dc_maps_settings_page',
				'w2dc_maps_section'
		);
		register_setting('w2dc_maps_settings_page', 'w2dc_map_on_index');
		add_settings_field(
				'w2dc_map_on_excerpt',
				__('Show map on excerpt page?', 'W2DC'),
				array($this, 'w2dc_map_on_excerpt_callback'),
				'w2dc_maps_settings_page',
				'w2dc_maps_section'
		);
		register_setting('w2dc_maps_settings_page', 'w2dc_map_on_excerpt');
		add_settings_field(
				'w2dc_show_directions',
				__('Show directions panel for the listing map?', 'W2DC'),
				array($this, 'w2dc_show_directions_callback'),
				'w2dc_maps_settings_page',
				'w2dc_maps_section'
		);
		register_setting('w2dc_maps_settings_page', 'w2dc_show_directions');
		add_settings_field(
				'w2dc_default_map_zoom',
				__('Default Google Maps zoom level', 'W2DC'),
				array($this, 'w2dc_default_map_zoom_callback'),
				'w2dc_maps_settings_page',
				'w2dc_maps_section'
		);
		register_setting('w2dc_maps_settings_page', 'w2dc_default_map_zoom');
		add_settings_field(
				'w2dc_map_style',
				__('Google Maps style', 'W2DC'),
				array($this, 'w2dc_map_style_callback'),
				'w2dc_maps_settings_page',
				'w2dc_maps_section'
		);
		register_setting('w2dc_maps_settings_page', 'w2dc_map_style');
		add_settings_field(
				'w2dc_enable_radius_search_cycle',
				__('Show cycle during radius search?', 'W2DC'),
				array($this, 'w2dc_enable_radius_search_cycle_callback'),
				'w2dc_maps_settings_page',
				'w2dc_maps_section'
		);
		register_setting('w2dc_maps_settings_page', 'w2dc_enable_radius_search_cycle');
		
		add_settings_field(
				'w2dc_enable_clusters',
				__('Enable clusters of map markers?', 'W2DC'),
				array($this, 'w2dc_enable_clusters_callback'),
				'w2dc_maps_settings_page',
				'w2dc_maps_section'
		);
		register_setting('w2dc_maps_settings_page', 'w2dc_enable_clusters');

		add_settings_field(
				'w2dc_default_geocoding_location',
				__('Default country/state for correct geocoding', 'W2DC'),
				array($this, 'w2dc_default_geocoding_location_callback'),
				'w2dc_maps_settings_page',
				'w2dc_maps_section'
		);
		register_setting('w2dc_maps_settings_page', 'w2dc_default_geocoding_location');

		// Notifications settings page /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_notifications_section',
				__('Email notifications settings', 'W2DC'),
				null,
				'w2dc_notifications_settings_page'
		);
		add_settings_field(
				'w2dc_send_expiration_notification_days',
				__('Days before pre-expiration notification will be sent', 'W2DC'),
				array($this, 'w2dc_send_expiration_notification_days_callback'),
				'w2dc_notifications_settings_page',
				'w2dc_notifications_section'
		);
		register_setting('w2dc_notifications_settings_page', 'w2dc_send_expiration_notification_days');
		add_settings_field(
				'w2dc_preexpiration_notification',
				__('Pre-expiration notification', 'W2DC'),
				array($this, 'w2dc_preexpiration_notification_callback'),
				'w2dc_notifications_settings_page',
				'w2dc_notifications_section'
		);
		register_setting('w2dc_notifications_settings_page', 'w2dc_preexpiration_notification');
		add_settings_field(
				'w2dc_expiration_notification',
				__('Expiration notification', 'W2DC'),
				array($this, 'w2dc_expiration_notification_callback'),
				'w2dc_notifications_settings_page',
				'w2dc_notifications_section'
		);
		register_setting('w2dc_notifications_settings_page', 'w2dc_expiration_notification');

		// advanced settings page /////////////////////////////////////////////////////////////////////////
		add_settings_section(
				'w2dc_advanced_section',
				__('Advanced settings', 'W2DC'),
				null,
				'w2dc_advanced_settings_page'
		);

		add_settings_field(
				'w2dc_notinclude_maps_api',
				__('Do not include Google Maps API', 'W2DC'),
				array($this, 'w2dc_notinclude_maps_api_callback'),
				'w2dc_advanced_settings_page',
				'w2dc_advanced_section'
		);
		register_setting('w2dc_advanced_settings_page', 'w2dc_notinclude_maps_api');
		
		// adapted for WPML /////////////////////////////////////////////////////////////////////////
		if (function_exists('icl_object_id')) {
			add_settings_section(
					'w2dc_wpml_section',
					__('WPML settings', 'W2DC'),
					null,
					'w2dc_advanced_settings_page'
			);
			
			add_settings_field(
					'w2dc_enable_automatic_translations',
					__('Enable automatic translations', 'W2DC'),
					array($this, 'w2dc_enable_automatic_translations_callback'),
					'w2dc_advanced_settings_page',
					'w2dc_wpml_section'
			);
			register_setting('w2dc_advanced_settings_page', 'w2dc_enable_automatic_translations');
		}
	}

	// General settings page /////////////////////////////////////////////////////////////////////////
	public function w2dc_directory_title_callback() {
		echo '<input type="text" id="w2dc_directory_title" name="w2dc_directory_title" value="' . esc_attr(get_option('w2dc_directory_title')) . '" size="53" />';
	}

	public function w2dc_category_slug_callback() {
		echo '<input type="text" id="w2dc_category_slug" name="w2dc_category_slug" value="' . esc_attr(get_option('w2dc_category_slug')) . '" size="25" />';
	}
	public function update_category_slug($new_value, $old_value) {
		global $w2dc_instance;
		
		$validator = new form_validation();
	
		if ($new_value == $w2dc_instance->index_page_slug) {
			w2dc_addMessage('Categories slug is the same as slug of directory page! This may cause problems. Use another slug.', 'error');
			return $old_value;
		} elseif ($new_value == get_option('w2dc_tag_slug')) {
			w2dc_addMessage('Categories slug is the same as tags slug! This may cause problems. Use another slug.', 'error');
			return $old_value;
		} elseif ($new_value === '') {
			w2dc_addMessage('Categories slug can not be empty.', 'error');
			return $old_value;
		} elseif (!$validator->alpha_dash($new_value)) {
			w2dc_addMessage('Categories slug must contain only alpha-numeric characters, underscores or dashes.', 'error');
			return $old_value;
		} else
			return $new_value;
	}

	public function w2dc_tag_slug_callback() {
		echo '<input type="text" id="w2dc_tag_slug" name="w2dc_tag_slug" value="' . esc_attr(get_option('w2dc_tag_slug')) . '" size="25" />';
	}
	public function update_tag_slug($new_value, $old_value) {
		global $w2dc_instance;
		
		$validator = new form_validation();
	
		if ($new_value == $w2dc_instance->index_page_slug) {
			w2dc_addMessage('Tags slug is the same as slug of directory page! This may cause problems. Use another slug.', 'error');
			return $old_value;
		} elseif ($new_value == get_option('w2dc_category_slug')) {
			w2dc_addMessage('Tags slug is the same as categories slug! This may cause problems. Use another slug.', 'error');
			return $old_value;
		} elseif ($new_value === '') {
			w2dc_addMessage('Tags slug can not be empty.', 'error');
			return $old_value;
		} elseif (!$validator->alpha_dash($new_value)) {
			w2dc_addMessage('Tags slug must contain only alpha-numeric characters, underscores or dashes.', 'error');
			return $old_value;
		} else
			return $new_value;
	}

	public function w2dc_color_scheme_callback() {
		global $w2dc_instance;
		$selected_scheme = ((isset($_COOKIE['w2dc_compare_palettes']) && $_COOKIE['w2dc_compare_palettes']) ? $_COOKIE['w2dc_compare_palettes'] : get_option('w2dc_color_scheme'));
		w2dc_renderTemplate('color_picker/color_picker_settings.tpl.php', array('selected_scheme' => $selected_scheme));
		printf(__('You may compare palettes at the <a href="%s">frontend</a>', 'W2DC'), w2dc_directoryUrl(array('compare_palettes' => 'true')));
	}

	// ReCaptcha settings page /////////////////////////////////////////////////////////////////////////
	public function w2dc_enable_recaptcha_callback() {
		echo '<input type="checkbox" id="w2dc_enable_recaptcha" name="w2dc_enable_recaptcha" value="1" ' . checked(get_option('w2dc_enable_recaptcha'), 1, false) .' />';
	}
	public function w2dc_recaptcha_public_key_callback() {
		echo '<p class="description">' . sprintf(__('get your reCAPTCHA API Keys <a href="%s" target="_blank">here</a>', 'W2DC'), 'http://www.google.com/recaptcha') . '</p>';
		echo '<input type="text" id="w2dc_recaptcha_public_key" name="w2dc_recaptcha_public_key" value="' . esc_attr(get_option('w2dc_recaptcha_public_key')) . '" size="53" />';
	}
	public function w2dc_recaptcha_private_key_callback() {
		echo '<input type="text" id="w2dc_recaptcha_private_key" name="w2dc_recaptcha_private_key" value="' . esc_attr(get_option('w2dc_recaptcha_private_key')) . '" size="53" />';
	}

	// Categories settings page /////////////////////////////////////////////////////////////////////////
	public function w2dc_show_categories_index_callback() {
		echo '<input type="checkbox" id="w2dc_show_categories_index" name="w2dc_show_categories_index" value="1" ' . checked(get_option('w2dc_show_categories_index'), 1, false) .' />';
	}
	public function w2dc_categories_nesting_level_callback() {
		echo '<select id="w2dc_categories_nesting_level" name="w2dc_categories_nesting_level">';
		echo '<option value=1 ' . selected(get_option('w2dc_categories_nesting_level'), 1, false) . '>1</option>';
		echo '<option value=2 ' . selected(get_option('w2dc_categories_nesting_level'), 2, false) . '>2</option>';
		echo '</select>';
		//echo '<input type="text" id="w2dc_categories_nesting_level" name="w2dc_categories_nesting_level" value="' . esc_attr(get_option('w2dc_categories_nesting_level')) .'" size="1" />';
	}
	public function w2dc_categories_columns_callback() {
		echo '<select id="w2dc_categories_columns" name="w2dc_categories_columns">';
		echo '<option value=1 ' . selected(get_option('w2dc_categories_columns'), 1, false) . '>1</option>';
		echo '<option value=2 ' . selected(get_option('w2dc_categories_columns'), 2, false) . '>2</option>';
		echo '<option value=3 ' . selected(get_option('w2dc_categories_columns'), 3, false) . '>3</option>';
		echo '<option value=4 ' . selected(get_option('w2dc_categories_columns'), 4, false) . '>4</option>';
		echo '</select>';
		//echo '<input type="text" id="w2dc_categories_columns" name="w2dc_categories_columns" value="' . esc_attr(get_option('w2dc_categories_columns')) .'" size="1" />';
	}
	public function w2dc_subcategories_items_callback() {
		echo '<input type="text" id="w2dc_subcategories_items" name="w2dc_subcategories_items" value="' . esc_attr(get_option('w2dc_subcategories_items')) .'" size="1" />';
		echo '<p class="description">' . __('Leave 0 to show all subcategories', 'W2DC') . '</p>';
	}
	public function w2dc_show_category_count_callback() {
		echo '<input type="checkbox" id="w2dc_show_category_count" name="w2dc_show_category_count" value="1" ' . checked(get_option('w2dc_show_category_count'), 1, false) .' />';
		echo '<p class="description">' . __('Number of listings under category and all its subcategories, categories widget has own setting', 'W2DC') . '</p>';
	}

	// Search settings page /////////////////////////////////////////////////////////////////////////
	public function w2dc_main_search_callback() {
		echo '<input type="checkbox" id="w2dc_main_search" name="w2dc_main_search" value="1" ' . checked(get_option('w2dc_main_search'), 1, false) .' />';
		echo '<p class="description">' . __('Note, that search widget is independent from this setting and this widget renders on each page where main search block was hidden', 'W2DC') . '</p>';
	}
	public function w2dc_show_what_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_what_search" name="w2dc_show_what_search" value="1" ' . checked(get_option('w2dc_show_what_search'), 1, false) .' />';
		echo '<p class="description">' . __('This setting is actual for both: main search block and widget', 'W2DC') . '</p>';
	}
	public function w2dc_show_where_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_where_search" name="w2dc_show_where_search" value="1" ' . checked(get_option('w2dc_show_where_search'), 1, false) .' />';
		echo '<p class="description">' . __('This setting is actual for both: main search block and widget', 'W2DC') . '</p>';
	}
	public function w2dc_show_keywords_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_keywords_search" name="w2dc_show_keywords_search" value="1" ' . checked(get_option('w2dc_show_keywords_search'), 1, false) .' />';
		echo '<p class="description">' . __('This setting is actual for both: main search block and widget', 'W2DC') . '</p>';
	}
	public function w2dc_show_locations_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_locations_search" name="w2dc_show_locations_search" value="1" ' . checked(get_option('w2dc_show_locations_search'), 1, false) .' />';
		echo '<p class="description">' . __('This setting is actual for both: main search block and widget', 'W2DC') . '</p>';
	}
	public function w2dc_show_address_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_address_search" name="w2dc_show_address_search" value="1" ' . checked(get_option('w2dc_show_address_search'), 1, false) .' />';
		echo '<p class="description">' . __('This setting is actual for both: main search block and widget', 'W2DC') . '</p>';
	}
	public function w2dc_show_location_count_in_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_location_count_in_search" name="w2dc_show_location_count_in_search" value="1" ' . checked(get_option('w2dc_show_location_count_in_search'), 1, false) .' />';
	}
	
	// Sorting settings page /////////////////////////////////////////////////////////////////////////
	public function w2dc_show_orderby_links_callback() {
		echo '<input type="checkbox" id="w2dc_show_orderby_links" name="w2dc_show_orderby_links" value="1" ' . checked(get_option('w2dc_show_orderby_links'), 1, false) .' />';
	}
	public function w2dc_orderby_date_callback() {
		echo '<input type="checkbox" id="w2dc_orderby_date" name="w2dc_orderby_date" value="1" ' . checked(get_option('w2dc_orderby_date'), 1, false) .' />';
	}
	public function w2dc_orderby_title_callback() {
		echo '<input type="checkbox" id="w2dc_orderby_title" name="w2dc_orderby_title" value="1" ' . checked(get_option('w2dc_orderby_title'), 1, false) .' />';
	}
	public function w2dc_default_orderby_callback() {
		echo '<select id="w2dc_default_orderby" name="w2dc_default_orderby">';
		
		$ordering = array('post_date' => __('Date', 'W2DC'), 'title' => __('Title', 'W2DC'));
		
		global $w2dc_instance;
		$content_fields = $w2dc_instance->content_fields->getOrderingContentFields();
		foreach ($content_fields AS $content_field)
			$ordering[$content_field->slug] = $content_field->name;

		$ordering = apply_filters('w2dc_default_orderby_options', $ordering);

		foreach ($ordering AS $field_slug=>$field_name)
			echo '<option value="' . $field_slug . '" ' . selected(get_option('w2dc_default_orderby'), $field_slug, false) . '> ' . $field_name . '</option>';

		echo '</select>';
	}
	public function w2dc_default_order_callback() {
		echo '<select id="w2dc_default_order" name="w2dc_default_order">';
		echo '<option value="ASC" ' . selected(get_option('w2dc_default_order'), 'ASC', false) . '>' . _('Ascending', 'W2DC') . '</option>';
		echo '<option value="DESC" ' . selected(get_option('w2dc_default_order'), 'DESC', false) . '>' . _('Descending', 'W2DC') . '</option>';
		echo '</select>';
	}

	// Listings settings page /////////////////////////////////////////////////////////////////////////
	public function w2dc_listings_on_index_callback() {
		echo '<input type="checkbox" id="w2dc_listings_on_index" name="w2dc_listings_on_index" value="1" ' . checked(get_option('w2dc_listings_on_index'), 1, false) .' />';
	}
	public function w2dc_listings_number_index_callback() {
		echo '<input type="text" id="w2dc_listings_number_index" name="w2dc_listings_number_index" value="' . esc_attr(get_option('w2dc_listings_number_index')) .'" size="1" />';
	}
	public function w2dc_listings_number_excerpt_callback() {
		echo '<input type="text" id="w2dc_listings_number_excerpt" name="w2dc_listings_number_excerpt" value="' . esc_attr(get_option('w2dc_listings_number_excerpt')) .'" size="1" />';
	}
	public function w2dc_listings_own_page_callback() {
		echo '<input type="checkbox" id="w2dc_listings_own_page" name="w2dc_listings_own_page" value="1" ' . checked(get_option('w2dc_listings_own_page'), 1, false) .' />';
	}
	public function w2dc_images_on_tab_callback() {
		echo '<input type="checkbox" id="w2dc_images_on_tab" name="w2dc_images_on_tab" value="1" ' . checked(get_option('w2dc_images_on_tab'), 1, false) .' />';
	}
	public function w2dc_images_lightbox_callback() {
		echo '<input type="checkbox" id="w2dc_images_lightbox" name="w2dc_images_lightbox" value="1" ' . checked(get_option('w2dc_images_lightbox'), 1, false) .' />';
	}
	public function w2dc_listing_contact_form_callback() {
		echo '<input type="checkbox" id="w2dc_listing_contact_form" name="w2dc_listing_contact_form" value="1" ' . checked(get_option('w2dc_listing_contact_form'), 1, false) .' />';
		echo '<p class="description">' . __('Contact Form 7 or standard form will be displayed on each listing page', 'W2DC') . '</p>';
	}
	public function w2dc_listing_contact_form_7_callback() {
		echo '<input type="text" id="w2dc_listing_contact_form_7" name="w2dc_listing_contact_form_7" value="' . esc_attr(get_option('w2dc_listing_contact_form_7')) .'" size="53" />';
		echo '<p class="description">' . __('This will work only when Contact Form 7 plugin enabled, otherwise standard contact form will be displayed', 'W2DC') . '</p>';
	}
	public function w2dc_favourites_list_callback() {
		echo '<input type="checkbox" id="w2dc_favourites_list" name="w2dc_favourites_list" value="1" ' . checked(get_option('w2dc_favourites_list'), 1, false) .' />';
	}
	public function w2dc_print_button_callback() {
		echo '<input type="checkbox" id="w2dc_print_button" name="w2dc_print_button" value="1" ' . checked(get_option('w2dc_print_button'), 1, false) .' />';
	}
	public function w2dc_pdf_button_callback() {
		echo '<input type="checkbox" id="w2dc_pdf_button" name="w2dc_pdf_button" value="1" ' . checked(get_option('w2dc_pdf_button'), 1, false) .' />';
	}
	public function w2dc_change_expiration_date_callback() {
		echo '<input type="checkbox" id="w2dc_change_expiration_date" name="w2dc_change_expiration_date" value="1" ' . checked(get_option('w2dc_change_expiration_date'), 1, false) .' />';
	}
	public function w2dc_hide_comments_number_on_index_callback() {
		echo '<input type="checkbox" id="w2dc_hide_comments_number_on_index" name="w2dc_hide_comments_number_on_index" value="1" ' . checked(get_option('w2dc_hide_comments_number_on_index'), 1, false) .' />';
	}
	public function w2dc_hide_listings_creation_date_callback() {
		echo '<input type="checkbox" id="w2dc_hide_listings_creation_date" name="w2dc_hide_listings_creation_date" value="1" ' . checked(get_option('w2dc_hide_listings_creation_date'), 1, false) .' />';
	}

	// Maps settings page /////////////////////////////////////////////////////////////////////////
	public function w2dc_map_on_index_callback() {
		echo '<input type="checkbox" id="w2dc_map_on_index" name="w2dc_map_on_index" value="1" ' . checked(get_option('w2dc_map_on_index'), 1, false) .' />';
	}
	public function w2dc_map_on_excerpt_callback() {
		echo '<input type="checkbox" id="w2dc_map_on_excerpt" name="w2dc_map_on_excerpt" value="1" ' . checked(get_option('w2dc_map_on_excerpt'), 1, false) .' />';
	}
	public function w2dc_show_directions_callback() {
		echo '<input type="checkbox" id="w2dc_show_directions" name="w2dc_show_directions" value="1" ' . checked(get_option('w2dc_show_directions'), 1, false) .' />';
	}
	public function w2dc_default_map_zoom_callback() {
		echo '<select id="w2dc_default_map_zoom" name="w2dc_default_map_zoom">';
		echo '<option value="1" ' . selected(get_option('w2dc_default_map_zoom'), 1, false) . '>1</option>';
		echo '<option value="2" ' . selected(get_option('w2dc_default_map_zoom'), 2, false) . '>2</option>';
		echo '<option value="3" ' . selected(get_option('w2dc_default_map_zoom'), 3, false) . '>3</option>';
		echo '<option value="4" ' . selected(get_option('w2dc_default_map_zoom'), 4, false) . '>4</option>';
		echo '<option value="5" ' . selected(get_option('w2dc_default_map_zoom'), 5, false) . '>5</option>';
		echo '<option value="6" ' . selected(get_option('w2dc_default_map_zoom'), 6, false) . '>6</option>';
		echo '<option value="7" ' . selected(get_option('w2dc_default_map_zoom'), 7, false) . '>7</option>';
		echo '<option value="8" ' . selected(get_option('w2dc_default_map_zoom'), 8, false) . '>8</option>';
		echo '<option value="9" ' . selected(get_option('w2dc_default_map_zoom'), 9, false) . '>9</option>';
		echo '<option value="10" ' . selected(get_option('w2dc_default_map_zoom'), 10, false) . '>10</option>';
		echo '<option value="11" ' . selected(get_option('w2dc_default_map_zoom'), 11, false) . '>11</option>';
		echo '<option value="12" ' . selected(get_option('w2dc_default_map_zoom'), 12, false) . '>12</option>';
		echo '<option value="13" ' . selected(get_option('w2dc_default_map_zoom'), 13, false) . '>13</option>';
		echo '<option value="14" ' . selected(get_option('w2dc_default_map_zoom'), 14, false) . '>14</option>';
		echo '<option value="15" ' . selected(get_option('w2dc_default_map_zoom'), 15, false) . '>15</option>';
		echo '<option value="16" ' . selected(get_option('w2dc_default_map_zoom'), 16, false) . '>16</option>';
		echo '<option value="17" ' . selected(get_option('w2dc_default_map_zoom'), 17, false) . '>17</option>';
		echo '<option value="18" ' . selected(get_option('w2dc_default_map_zoom'), 18, false) . '>18</option>';
		echo '<option value="19" ' . selected(get_option('w2dc_default_map_zoom'), 19, false) . '>19</option>';
		echo '</select>';
	}
	public function w2dc_map_style_callback() {
		global $maps_styles;

		echo '<select id="w2dc_map_style" name="w2dc_map_style">';
		echo '<option value="default" ' . selected(get_option('w2dc_map_style'), 'default', false) . '>' . __('Default style', 'W2DC') . '</option>';
		foreach ($maps_styles AS $style_name=>$style) {
			echo '<option value="' . $style_name . '" ' . selected(get_option('w2dc_map_style'), $style_name, false) . '>' . $style_name . '</option>';
		}
		echo '</select>';
	}
	public function w2dc_enable_radius_search_cycle_callback() {
		echo '<input type="checkbox" id="w2dc_enable_radius_search_cycle" name="w2dc_enable_radius_search_cycle" value="1" ' . checked(get_option('w2dc_enable_radius_search_cycle'), 1, false) .' />';
	}
	public function w2dc_enable_clusters_callback() {
		echo '<input type="checkbox" id="w2dc_enable_clusters" name="w2dc_enable_clusters" value="1" ' . checked(get_option('w2dc_enable_clusters'), 1, false) .' />';
	}
	public function w2dc_default_geocoding_location_callback() {
		echo '<input type="text" id="w2dc_default_geocoding_location" name="w2dc_default_geocoding_location" value="' . esc_attr(get_option('w2dc_default_geocoding_location')) . '" size="53" />';
		echo '<p class="description">' . __('This value needed when you build local diirectory, all your listings place in one local area - country or state. You do not want to set countries or states in the search, so this hidden string will be automatically added to the address for correct geocoding when you create/edit listings.', 'W2DC') . '</p>';
	}

	// Notifications settings page /////////////////////////////////////////////////////////////////////////
	public function w2dc_send_expiration_notification_days_callback() {
		echo '<input type="text" id="w2dc_send_expiration_notification_days" name="w2dc_send_expiration_notification_days" value="' . esc_attr(get_option('w2dc_send_expiration_notification_days')) .'" size="1" />';
	}
	public function w2dc_preexpiration_notification_callback() {
		echo '<textarea id="w2dc_preexpiration_notification" name="w2dc_preexpiration_notification" cols="60" rows="3">' . esc_textarea(get_option('w2dc_preexpiration_notification')) . '</textarea>';
	}
	public function w2dc_expiration_notification_callback() {
		echo '<textarea id="w2dc_expiration_notification" name="w2dc_expiration_notification" cols="60" rows="3">' . esc_textarea(get_option('w2dc_expiration_notification')) . '</textarea>';
	}

	public function w2dc_settings_page() {
		if (isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true')
			w2dc_addMessage(__('Settings saved!', 'W2DC'));
		
		$section = 'w2dc_settings_page';
		if (isset($_GET['section']) && $_GET['section'])
			$section = $_GET['section'];

		w2dc_renderTemplate('settings_common.tpl.php', array('section' => $section));
	}
	
	public function w2dc_notinclude_maps_api_callback() {
		echo '<input type="checkbox" id="w2dc_notinclude_maps_api" name="w2dc_notinclude_maps_api" value="1" ' . checked(get_option('w2dc_notinclude_maps_api'), 1, false) .' />';
		echo '<p class="description">' . __('Some themes and 3rd party plugins include Google Maps API - this may cause conflicts and unstable work of maps.', 'W2DC') . '</p>';
	}
	
	// adapted for WPML
	public function w2dc_enable_automatic_translations_callback() {
		echo '<input type="checkbox" id="w2dc_enable_automatic_translations" name="w2dc_enable_automatic_translations" value="1" ' . checked(get_option('w2dc_enable_automatic_translations'), 1, false) .' />';
		echo '<p class="description">' . __('After successfull submission new listing will be copied automatically to all active languages.', 'W2DC') . '</p>';
	}
}

?>