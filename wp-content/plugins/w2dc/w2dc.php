<?php
/*
Plugin Name: Web 2.0 Directory core plugin
Plugin URI: http://www.salephpscripts.com/wordpress_directory/
Description: Provides an ability to build any kind of directory site: classifieds, events directory, cars, bikes, boats and other vehicles dealers site, pets, real estate portal on your WordPress powered site. In other words - whatever you want.
Version: 1.6.2
Author: Mihail Chepovskiy
Author URI: http://www.salephpscripts.com
License: GPLv2 or any later version
*/

define('W2DC_VERSION', '1.6.2');

define('W2DC_PATH', plugin_dir_path(__FILE__));
define('W2DC_URL', plugins_url('/', __FILE__));

function w2dc_loadPaths() {
	define('W2DC_TEMPLATES_PATH', W2DC_PATH . 'templates/');

	if (!defined('W2DC_THEME_MODE')) {
		define('W2DC_RESOURCES_PATH', W2DC_PATH . 'resources/');
		define('W2DC_RESOURCES_URL', W2DC_URL . 'resources/');

		// Content fields icons constant
		define('W2DC_FIELDS_ICONS_PATH', W2DC_RESOURCES_PATH . 'images/content_fields_icons/');
		define('W2DC_FIELDS_ICONS_URL', W2DC_RESOURCES_URL . 'images/content_fields_icons/');

		// Categories icons constant
		define('W2DC_CATEGORIES_ICONS_PATH', W2DC_RESOURCES_PATH . 'images/categories_icons/');
		define('W2DC_CATEGORIES_ICONS_URL', W2DC_RESOURCES_URL . 'images/categories_icons/');
	}
}
add_action('init', 'w2dc_loadPaths', 0);

define('W2DC_POST_TYPE', 'w2dc_listing');
define('W2DC_CATEGORIES_TAX', 'w2dc-category');
define('W2DC_LOCATIONS_TAX', 'w2dc-location');
define('W2DC_TAGS_TAX', 'w2dc-tag');

include_once W2DC_PATH . 'install.php';
include_once W2DC_PATH . 'classes/admin.php';
include_once W2DC_PATH . 'classes/form_validation.php';
include_once W2DC_PATH . 'classes/listings/listings_manager.php';
include_once W2DC_PATH . 'classes/listings/listing.php';
include_once W2DC_PATH . 'classes/categories_manager.php';
include_once W2DC_PATH . 'classes/media_manager.php';
include_once W2DC_PATH . 'classes/content_fields/content_fields_manager.php';
include_once W2DC_PATH . 'classes/content_fields/content_fields.php';
include_once W2DC_PATH . 'classes/locations/locations_manager.php';
include_once W2DC_PATH . 'classes/locations/locations_levels_manager.php';
include_once W2DC_PATH . 'classes/locations/locations_levels.php';
include_once W2DC_PATH . 'classes/locations/location.php';
include_once W2DC_PATH . 'classes/levels/levels_manager.php';
include_once W2DC_PATH . 'classes/levels/levels.php';
include_once W2DC_PATH . 'classes/frontend_controller.php';
include_once W2DC_PATH . 'classes/shortcodes/directory_controller.php';
include_once W2DC_PATH . 'classes/shortcodes/listings_controller.php';
include_once W2DC_PATH . 'classes/shortcodes/map_controller.php';
include_once W2DC_PATH . 'classes/shortcodes/categories_controller.php';
include_once W2DC_PATH . 'classes/shortcodes/search_controller.php';
include_once W2DC_PATH . 'classes/ajax_controller.php';
include_once W2DC_PATH . 'classes/settings_manager.php';
include_once W2DC_PATH . 'classes/search_form.php';
include_once W2DC_PATH . 'classes/google_maps.php';
include_once W2DC_PATH . 'classes/widgets.php';
include_once W2DC_PATH . 'classes/csv_manager.php';
include_once W2DC_PATH . 'classes/location_geoname.php';
include_once W2DC_PATH . 'functions.php';
include_once W2DC_PATH . 'functions_ui.php';
include_once W2DC_PATH . 'maps_styles.php';

global $w2dc_instance;
global $w2dc_messages;

define('W2DC_MAIN_SHORTCODE', 'webdirectory');

/*
 * There are 2 types of shortcodes in the system:
 1. those process as simple wordpress shortcodes
 2. require initialization on 'wp' hook
 */
global $w2dc_shortcodes, $w2dc_shortcodes_init;
$w2dc_shortcodes = array(
		'webdirectory' => 'w2dc_directory_controller',
		'webdirectory-listings' => 'w2dc_listings_controller',
		'webdirectory-map' => 'w2dc_map_controller',
		'webdirectory-categories' => 'w2dc_categories_controller',
		'webdirectory-search' => 'w2dc_search_controller',
		);
$w2dc_shortcodes_init = array();

class w2dc_plugin {
	public $admin;
	public $listings_manager;
	public $locations_manager;
	public $locations_levels_manager;
	public $categories_manager;
	public $content_fields_manager;
	public $media_manager;
	public $settings_manager;
	public $levels_manager;
	public $csv_manager;

	public $current_listing; // this is object of listing under edition right now
	public $levels;
	public $index_page_id;
	public $index_page_slug;
	public $index_page_url;
	public $frontend_controllers;
	public $_frontend_controllers; // this duplicate property needed because we unset each controller when we render shortcodes, but WP doesn't really know which shortcode already was processed
	public $action;
	public $map_markers_url = '';
	
	public $avoid_to_print_these_scripts_twice = array();
	
	public $order_by_date = false; // special flag, used to display or hide sticky pin

	public function __construct() {
		register_activation_hook(__FILE__, array($this, 'activation'));
		register_deactivation_hook(__FILE__, array($this, 'deactivation'));
	}
	
	public function activation() {
		global $wp_version;

		if (version_compare($wp_version, '3.6', '<')) {
			deactivate_plugins(basename(__FILE__)); // Deactivate ourself
			wp_die("Sorry, but you can't run this plugin on current WordPress version, it requires WordPress v3.6 or higher.");
		}
		flush_rewrite_rules();
		
		wp_schedule_event(current_time('timestamp'), 'hourly', 'sheduled_events');
	}

	public function deactivation() {
		flush_rewrite_rules();

		wp_clear_scheduled_hook('sheduled_events');
	}
	
	public function init() {
		global $w2dc_instance, $w2dc_shortcodes, $wpdb;

		$_GET = stripslashes_deep($_GET);
		if (isset($_REQUEST['w2dc_action']))
			$this->action = $_REQUEST['w2dc_action'];

		add_action('plugins_loaded', array($this, 'load_textdomain'), 0);
		
		add_action('sheduled_events', array($this, 'suspend_expired_listings'));

		foreach ($w2dc_shortcodes AS $shortcode=>$function)
			add_shortcode($shortcode, array($this, 'renderShortcode'));

		add_action('init', array($this, 'register_post_type'), 0);
		add_action('init', array($this, 'getIndexPage'), 0);
		
		if (!isset($wpdb->content_fields))
			$wpdb->content_fields = $wpdb->prefix . 'w2dc_content_fields';
		if (!isset($wpdb->levels))
			$wpdb->levels = $wpdb->prefix . 'w2dc_levels';
		if (!isset($wpdb->levels_relationships))
			$wpdb->levels_relationships = $wpdb->prefix . 'w2dc_levels_relationships';
		if (!isset($wpdb->locations_levels))
			$wpdb->locations_levels = $wpdb->prefix . 'w2dc_locations_levels';
		if (!isset($wpdb->locations_relationships))
			$wpdb->locations_relationships = $wpdb->prefix . 'w2dc_locations_relationships';

		add_action('wp', array($this, 'loadFrontendControllers'), 1);

		$w2dc_instance->levels = new w2dc_levels;
		$w2dc_instance->locations_levels = new w2dc_locations_levels;
		$w2dc_instance->content_fields = new w2dc_content_fields;

		$w2dc_instance->ajax_controller = new w2dc_ajax_controller;

		$this->admin = new w2dc_admin();

		add_filter('template_include', array($this, 'printlisting_template'));

		add_action('wp_loaded', array($this, 'wp_loaded'));
		add_filter('query_vars', array($this, 'add_query_vars'));
		add_filter('rewrite_rules_array', array($this, 'rewrite_rules'));
		
		add_filter('redirect_canonical', array($this, 'prevent_wrong_redirect'), 10, 2);
		add_filter('post_type_link', array($this, 'listing_permalink'), 10, 2);
		add_filter('term_link', array($this, 'category_permalink'), 10, 3);
		add_filter('term_link', array($this, 'tag_permalink'), 10, 3);

		// WPML builds wrong urls for translations,
		// also Paid Memberships Pro plugin breaks its redirect after login and before session had started,
		// that is why this filter must be disabled
		//add_filter('home_url', array($this, 'add_trailing_slash_to_home'), 1000, 2);

		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_styles'));
		add_action('wp_print_styles', array($this, 'enqueue_frontend_style'), 9999);
		
		add_filter('script_loader_src', array($this, 'avoid_to_print_scripts_twice'), 10, 2);
	}

	public function load_textdomain() {
		load_plugin_textdomain('W2DC', '', dirname(plugin_basename( __FILE__ )) . '/languages');
	}

	public function renderShortcode() {
		global $w2dc_shortcodes, $w2dc_shortcodes_init;

		// remove content filters in order not to break the layout of page
		remove_filter('the_content', 'wpautop');
		remove_filter('the_content', 'wptexturize');
		remove_filter('the_content', 'shortcode_unautop');
		remove_filter('the_content', 'convert_chars');
		remove_filter('the_content', 'prepend_attachment');
		remove_filter('the_content', 'convert_smilies');

		$attrs = func_get_args();
		$shortcode = $attrs[2];

		// we'll execute shortcode ONLY from the content of the page - because some "bad" plugins may force shortcodes to execute twice on one page in different filters
		if (current_filter() == 'the_content') {
			if (isset($this->_frontend_controllers[$shortcode])) {
				$shortcode_controllers = $this->_frontend_controllers[$shortcode];
				foreach ($shortcode_controllers AS $key=>$controller) {
					unset($this->_frontend_controllers[$shortcode][$key]);
	
					if (method_exists($controller, 'display'))
						return $controller->display();
				}
			}

			if (isset($w2dc_shortcodes[$shortcode])) {
				$shortcode_class = $w2dc_shortcodes[$shortcode];
				$shortcode_instance = new $shortcode_class($attrs[0]);
				$this->frontend_controllers[$shortcode][] = $shortcode_instance;
				
				if (method_exists($shortcode_instance, 'display'))
					return $shortcode_instance->display();
			}
		}
	}

	public function loadFrontendControllers() {
		global $post;

		if ($post) {
			$pattern = get_shortcode_regex();
			$this->loadNestedFrontendController($pattern, $post->post_content);
		}
	}

	// this may be recursive function to catch nested shortcodes
	public function loadNestedFrontendController($pattern, $content) {
		global $w2dc_shortcodes_init;

		if (preg_match_all('/'.$pattern.'/s', $content, $matches) && array_key_exists(2, $matches)) {
			foreach ($matches[2] AS $key=>$shortcode) {
				if (isset($w2dc_shortcodes_init[$shortcode]) && class_exists($w2dc_shortcodes_init[$shortcode])) {
					$shortcode_class = $w2dc_shortcodes_init[$shortcode];
					if (!($attrs = shortcode_parse_atts($matches[3][$key])))
						$attrs = array();
					$shortcode_instance = new $shortcode_class($attrs);
					$this->frontend_controllers[$shortcode][] = $shortcode_instance;
					$this->_frontend_controllers[$shortcode][] = $shortcode_instance;
				}
				if ($shortcode_content = $matches[5][$key])
					$this->loadNestedFrontendController($pattern, $shortcode_content);
			}
		}
	}

	public function getIndexPage() {
		if ($array = w2dc_getIndexPage()) {
			$this->index_page_id = $array['id'];
			$this->index_page_slug = $array['slug'];
			$this->index_page_url = $array['url'];
		}
		
		if ($this->index_page_slug == get_option('w2dc_category_slug') || $this->index_page_slug == get_option('w2dc_tag_slug'))
			w2dc_addMessage('Categories or tags slug is the same as slug of directory page! This may cause problems. Go to <a href="' . admin_url('admin.php?page=w2dc_settings') . '">settings page</a> and enter another slug.', 'error');
		
		if ($this->index_page_id === 0)
			w2dc_addMessage(sprintf(__('<b>Web 2.0 Directory plugin</b>: sorry, but there isn\'t any page with [webdirectory] shortcode. Create <a href="%s">this special page</a> for you?', 'W2DC'), admin_url('admin.php?page=w2dc_admin&action=directory_page_installation')));
	}

	public function add_query_vars($vars) {
		$vars[] = 'listing';
		$vars[] = 'category';
		$vars[] = 'tag';
	
		$key = array_search('order', $vars);
		unset($vars[$key]);
	
		return $vars;
	}
	
	public function rewrite_rules($rules) {
		return $this->w2dc_addRules() + $rules;
	}
	
	public function w2dc_addRules() {
		global $wp_rewrite;
		//var_dump($wp_rewrite);
	
		//var_dump($wp_rewrite->rewrite_rules());
		/* 		foreach (get_option('rewrite_rules') AS $key=>$rule)
		 echo $key . '
		' . $rule . '
	
	
		'; */
	
		$page_url = $this->index_page_slug;
		
		foreach (get_post_ancestors($this->index_page_id) AS $parent_id) {
			$parent = get_page($parent_id);
			$page_url = $parent->post_name . '/' . $page_url;
		}

		$rules['(' . $page_url . ')/' . $wp_rewrite->pagination_base . '/?([0-9]{1,})/?$'] = 'index.php?page_id=' .  $this->index_page_id . '&paged=$matches[2]';
		$rules['(' . $page_url . ')/?$'] = 'index.php?page_id=' .  $this->index_page_id;
	
		$rules['(' . $page_url . ')?/?' . get_option('w2dc_category_slug') . '/([^\/.]+)/' . $wp_rewrite->pagination_base . '/?([0-9]{1,})/?$'] = 'index.php?page_id=' .  $this->index_page_id . '&category=$matches[2]&paged=$matches[3]';
		$rules['(' . $page_url . ')?/?' . get_option('w2dc_category_slug') . '/([^\/.]+)/?$'] = 'index.php?page_id=' .  $this->index_page_id . '&category=$matches[2]';
	
		$rules['(' . $page_url . ')?/?' . get_option('w2dc_tag_slug') . '/([^\/.]+)/' . $wp_rewrite->pagination_base . '/?([0-9]{1,})/?$'] = 'index.php?page_id=' .  $this->index_page_id . '&tag=$matches[2]&paged=$matches[3]';
		$rules['(' . $page_url . ')?/?' . get_option('w2dc_tag_slug') . '/([^\/.]+)/?$'] = 'index.php?page_id=' .  $this->index_page_id . '&tag=$matches[2]';
	
		$rules[$page_url . '/([^\/.]+)/?$'] = 'index.php?page_id=' . $this->index_page_id . '&listing=$matches[1]';
		//$rules['([0-9]{1,})/([^\/.]+)/?$'] = 'index.php?page_id=' . $this->index_page_id . '&listing=$matches[2]';
		// Avoid mismatches with archive pages with /%year%/%monthnum%/ permalinks structure
		$rules['(?!(?:199[0-9]|20[012][0-9])/(?:0[1-9]|1[012]))([0-9]{1,})/([^\/.]+)/?$'] = 'index.php?page_id=' . $this->index_page_id . '&listing=$matches[2]';
	
		return $rules;
	}
	
	public function wp_loaded() {
		if ($rules = get_option('rewrite_rules'))
			foreach ($this->w2dc_addRules() as $key=>$value)
				if (!isset($rules[$key]) || $rules[$key] != $value) {
					global $wp_rewrite;
					$wp_rewrite->flush_rules();
					return;
				}
	}
	
	public function prevent_wrong_redirect($redirect_url, $requested_url) {
		global $wp_query;
	
		if (get_option('page_on_front') == $this->index_page_id)
			if (is_page() && !is_feed() && isset($wp_query->queried_object) && 'page' == get_option('show_on_front') && $wp_query->queried_object->ID == get_option('page_on_front'))
				return $requested_url;
	
		return $redirect_url;
	}

	public function listing_permalink($permalink, $post) {
		if ($post->post_type == W2DC_POST_TYPE) {
			global $wp_rewrite;
			if ($wp_rewrite->using_permalinks())
				if (get_option('page_on_front') == $this->index_page_id)
					return w2dc_directoryUrl($post->ID . '/' . $post->post_name);
				else
					return w2dc_directoryUrl($post->post_name);
			else
				return w2dc_directoryUrl(array('listing' => $post->post_name));
		}
		return $permalink;
	}

	public function category_permalink($permalink, $category, $tax) {
		if ($tax == W2DC_CATEGORIES_TAX) {
			global $wp_rewrite;
			if ($wp_rewrite->using_permalinks())
				return w2dc_directoryUrl(get_option('w2dc_category_slug') . '/' . $category->slug);
			else
				return w2dc_directoryUrl(array('category' => $category->slug));
		}
		return $permalink;
	}

	public function tag_permalink($permalink, $tag, $tax) {
		if ($tax == W2DC_TAGS_TAX) {
			global $wp_rewrite;
			if ($wp_rewrite->using_permalinks())
				return w2dc_directoryUrl(get_option('w2dc_tag_slug') . '/' . $tag->slug);
			else
				return w2dc_directoryUrl(array('tag' => $tag->slug));
		}
		return $permalink;
	}

	public function register_post_type() {
		$args = array(
			'labels' => array(
				'name' => __('Directory listings', 'W2DC'),
				'singular_name' => __('Directory listing', 'W2DC'),
				'add_new' => __('Create new listing', 'W2DC'),
				'add_new_item' => __('Create new listing', 'W2DC'),
				'edit_item' => __('Edit listing', 'W2DC'),
				'new_item' => __('New listing', 'W2DC'),
				'view_item' => __('View listing', 'W2DC'),
				'search_items' => __('Search listings', 'W2DC'),
				'not_found' =>  __('No listings found', 'W2DC'),
				'not_found_in_trash' => __('No listings found in trash', 'W2DC')
			),
			'has_archive' => true,
			'description' => __('Directory listings', 'W2DC'),
			'public' => true,
			'exclude_from_search' => false, // this must be false otherwise it breaks pagination for custom taxonomies
			'supports' => array('title', 'editor', 'author', 'categories', 'tags', 'excerpt', 'comments'),
			'menu_icon' => W2DC_RESOURCES_URL . 'images/menuicon.png',
		);
		register_post_type(W2DC_POST_TYPE, $args);
		
		register_taxonomy(W2DC_CATEGORIES_TAX, W2DC_POST_TYPE, array(
				'hierarchical' => true,
				'has_archive' => true,
				'labels' => array(
					'name' =>  __('Listing categories', 'W2DC'),
					'menu_name' =>  __('Directory categories', 'W2DC'),
					'singular_name' => __('Category', 'W2DC'),
					'add_new_item' => __('Create category', 'W2DC'),
					'new_item_name' => __('New category', 'W2DC'),
					'edit_item' => __('Edit category', 'W2DC'),
					'view_item' => __('View category', 'W2DC'),
					'update_item' => __('Update category', 'W2DC'),
					'search_items' => __('Search categories', 'W2DC'),
				),
			)
		);
		register_taxonomy(W2DC_LOCATIONS_TAX, W2DC_POST_TYPE, array(
				'hierarchical' => true,
				'labels' => array(
					'name' =>  __('Listing locations', 'W2DC'),
					'menu_name' =>  __('Directory locations', 'W2DC'),
					'singular_name' => __('Location', 'W2DC'),
					'add_new_item' => __('Create location', 'W2DC'),
					'new_item_name' => __('New location', 'W2DC'),
					'edit_item' => __('Edit location', 'W2DC'),
					'view_item' => __('View location', 'W2DC'),
					'update_item' => __('Update location', 'W2DC'),
					'search_items' => __('Search locations', 'W2DC'),
					
				),
			)
		);
		register_taxonomy(W2DC_TAGS_TAX, W2DC_POST_TYPE, array(
				'hierarchical' => false,
				'labels' => array(
					'name' =>  __('Listing tags', 'W2DC'),
					'menu_name' =>  __('Directory tags', 'W2DC'),
					'singular_name' => __('Tag', 'W2DC'),
					'add_new_item' => __('Create tag', 'W2DC'),
					'new_item_name' => __('New tag', 'W2DC'),
					'edit_item' => __('Edit tag', 'W2DC'),
					'view_item' => __('View tag', 'W2DC'),
					'update_item' => __('Update tag', 'W2DC'),
					'search_items' => __('Search tags', 'W2DC'),
				),
			)
		);

		if (!get_option('w2dc_installed_directory') || get_option('w2dc_installed_directory_version') != W2DC_VERSION)
			w2dc_install_directory();
	}

	public function suspend_expired_listings() {
		global $wpdb;

		$posts_ids = $wpdb->get_col($wpdb->prepare("
				SELECT
					wp_pm1.post_id
				FROM
					{$wpdb->postmeta} AS wp_pm1
				LEFT JOIN
					{$wpdb->postmeta} AS wp_pm2 ON wp_pm1.post_id=wp_pm2.post_id
				WHERE
					wp_pm1.meta_key = '_expiration_date' AND
					wp_pm1.meta_value < %d AND
					wp_pm2.meta_key = '_listing_status' AND
					(wp_pm2.meta_value = 'active' OR wp_pm2.meta_value = 'stopped')
			", time()));
		foreach ($posts_ids AS $post_id) {
			if (!get_post_meta($this->current_listing->post->ID, '_expiration_notification_sent', true)) {
				$post = get_post($post_id);
				$listing_owner = get_userdata($post->post_author);
			
				/* $headers =  "MIME-Version: 1.0\n" .
						"From: get_option('blogname' <" . get_option('admin_email') . ">\n" .
						"Reply-To: get_option('admin_email')\n" .
						"Content-Type: text/plain; charset=\"" . get_option('blog_charset') . "\"\n"; */
				$headers[] = "From: " . get_option('blogname') . " <" . get_option('admin_email') . ">";
				$headers[] = "Reply-To: " . get_option('admin_email');
			
				$subject = "[" . get_option('blogname') . "] " . __('Expiration notification', 'W2DC');
			
				$body = str_replace('[listing]', $post->post_title, str_replace('[link]', admin_url('options.php?page=w2dc_renew&listing_id=' . $post->ID), get_option('w2dc_expiration_notification')));
				
				$listings_ids = array();
				
				// adapted for WPML
				if (function_exists('icl_object_id')) {
					global $sitepress;
					$trid = $sitepress->get_element_trid($post_id, 'post_' . W2DC_POST_TYPE);
					$translations = $sitepress->get_element_translations($trid);
					foreach ($translations AS $lang=>$translation)
						$listings_ids[] = $translation->element_id;
				} else
					$listings_ids[] = $post_id;
			
				if (wp_mail($listing_owner->user_email, $subject, $body, $headers)) {
					foreach ($listings_ids AS $listing_id)
						add_post_meta($listing_id, '_expiration_notification_sent', true);
				}

				foreach ($listings_ids AS $listing_id) {
					update_post_meta($listing_id, '_listing_status', 'expired');
					wp_update_post(array('ID' => $listing_id, 'post_status' => 'draft'));
				}
			}
		}

		$posts_ids = $wpdb->get_col($wpdb->prepare("
				SELECT
					wp_pm1.post_id
				FROM
					{$wpdb->postmeta} AS wp_pm1
				LEFT JOIN
					{$wpdb->postmeta} AS wp_pm2 ON wp_pm1.post_id=wp_pm2.post_id
				WHERE
					wp_pm1.meta_key = '_expiration_date' AND
					wp_pm1.meta_value < %d AND
					wp_pm2.meta_key = '_listing_status' AND
					(wp_pm2.meta_value = 'active' OR wp_pm2.meta_value = 'stopped')
			", time()+(get_option('w2dc_send_expiration_notification_days')*86400)));
		foreach ($posts_ids AS $post_id) {
			if (!get_post_meta($post_id, '_preexpiration_notification_sent', true)) {
				$post = get_post($post_id);
				$listing_owner = get_userdata($post->post_author);
				
				/* $headers =  "MIME-Version: 1.0\n" .
						"From: get_option('blogname' <" . get_option('admin_email') . ">\n" .
						"Reply-To: get_option('admin_email')\n" .
						"Content-Type: text/plain; charset=\"" . get_option('blog_charset') . "\"\n"; */
				$headers[] = "From: " . get_option('blogname') . " <" . get_option('admin_email') . ">";
				$headers[] = "Reply-To: " . get_option('admin_email');

				$subject = "[" . get_option('blogname') . "] " . __('Expiration notification', 'W2DC');
				
				$body = str_replace('[listing]', $post->post_title, str_replace('[days]', get_option('w2dc_send_expiration_notification_days'), get_option('w2dc_preexpiration_notification')));
				
				if (wp_mail($listing_owner->user_email, $subject, $body, $headers)) {
					$listings_ids = array();
					
					// adapted for WPML
					if (function_exists('icl_object_id')) {
						global $sitepress;
						$trid = $sitepress->get_element_trid($post_id, 'post_' . W2DC_POST_TYPE);
						$translations = $sitepress->get_element_translations($trid);
						foreach ($translations AS $lang=>$translation)
							$listings_ids[] = $translation->element_id;
					} else
						$listings_ids[] = $post_id;
					
					foreach ($listings_ids AS $listing_id)
						add_post_meta($listing_id, '_preexpiration_notification_sent', true);
				}
			}
		}
	}

	/**
	 * Special template for listings printing functionality
	 */
	public function printlisting_template($template) {
		if (is_page($this->index_page_id) && ($this->action == 'printlisting' || $this->action == 'pdflisting')) {
			if (is_file(W2DC_TEMPLATES_PATH . 'frontend/listing_print-custom.tpl.php'))
				$template = W2DC_TEMPLATES_PATH . 'frontend/listing_print-custom.tpl.php';
			else
				$template = W2DC_TEMPLATES_PATH . 'frontend/listing_print.tpl.php';
		}
		return $template;
	}
	
	public function add_trailing_slash_to_home($url, $path) {
		if ($path == '')
			$url = rtrim($url, '/') . '/';

		return $url;
	}

	public function enqueue_scripts_styles() {
		wp_register_style('w2dc_bootstrap', W2DC_RESOURCES_URL . 'css/bootstrap.css');
		wp_register_style('w2dc_frontend', W2DC_RESOURCES_URL . 'css/frontend.css');

		if (isset($_COOKIE['w2dc_compare_palettes']) && $_COOKIE['w2dc_compare_palettes'])
			$color_scheme = $_COOKIE['w2dc_compare_palettes'];
		else
			if (!($color_scheme = get_option('w2dc_color_scheme')))
				$color_scheme = 'default';
		wp_register_style('w2dc_colors', W2DC_RESOURCES_URL . 'css/color_schemes/' . $color_scheme . '.css');

		if (is_file(W2DC_RESOURCES_PATH . 'css/frontend-custom.css'))
			wp_register_style('w2dc_frontend-custom', W2DC_RESOURCES_URL . 'css/frontend-custom.css');

		wp_register_script('js_functions', W2DC_RESOURCES_URL . 'js/js_functions.js', array('jquery'));
			
		wp_register_script('w2dc_tax_dropdowns_handle', W2DC_RESOURCES_URL . 'js/tax_dropdowns.js', array('jquery'));
		
		wp_register_script('categories_edit_scripts', W2DC_RESOURCES_URL . 'js/categories_icons.js', array('jquery'));
		wp_register_script('categories_scripts', W2DC_RESOURCES_URL . 'js/manage_categories.js', array('jquery'));
		
		wp_register_style('media_styles', W2DC_RESOURCES_URL . 'lightbox/css/lightbox.css');
		wp_register_script('media_scripts_lightbox', W2DC_RESOURCES_URL . 'lightbox/js/lightbox-2.6.min.js', array('jquery'));
		wp_register_script('media_scripts', W2DC_RESOURCES_URL . 'js/ajaxfileupload.js', array('jquery'));

		// this jQuery UI version 1.10.4 is for WP v3.9.0
		wp_register_style('jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');

		wp_register_script('jquery_cookie', W2DC_RESOURCES_URL . 'js/jquery.coo_kie.js', array('jquery'));

		wp_register_script('google_maps_view', W2DC_RESOURCES_URL . 'js/google_maps_view.js', array('jquery'));
		wp_register_script('w2dc_google_maps', '//maps.google.com/maps/api/js?v=3.14&sensor=false', array('jquery'), false, true);
		wp_register_script('w2dc_google_maps_edit', W2DC_RESOURCES_URL . 'js/google_maps_edit.js', array('jquery'));
		wp_register_script('google_maps_infobox', W2DC_RESOURCES_URL . 'js/infobox_packed.js', array('jquery'), false, true);
		wp_register_script('google_maps_clasterer', W2DC_RESOURCES_URL . 'js/google_maps_clasterer.js', array('jquery'));

		wp_register_script('listing_tabs', W2DC_RESOURCES_URL . 'js/tab.js', array('jquery'));

		wp_register_style('carousel_style', W2DC_RESOURCES_URL . 'css/anythingslider/css/anythingslider.css');
		wp_register_style('carousel_theme', W2DC_RESOURCES_URL . 'css/anythingslider/css/theme-minimalist-round.css');
		wp_register_script('carousel_scripts', W2DC_RESOURCES_URL . 'js/jquery.anythingslider.min.js', array('jquery'));

		wp_enqueue_script('js_functions');
	
		wp_enqueue_script('jquery-ui-dialog');
		wp_enqueue_script('jquery-ui-autocomplete');
		wp_enqueue_style('jquery-ui-style');
	
		wp_enqueue_script('jquery_cookie');

		wp_enqueue_script('google_maps_view');
		if (!get_option('w2dc_notinclude_maps_api')) {
			wp_enqueue_script('w2dc_google_maps');
			$this->avoid_to_print_these_scripts_twice['w2dc_google_maps'] = 'maps.google.com/maps/api/js';
			$this->avoid_to_print_these_scripts_twice['w2dc_googleapis_maps'] = 'maps.googleapis.com/maps/api/js';
		}
		wp_enqueue_script('google_maps_infobox');

		wp_localize_script(
				'js_functions',
				'js_objects',
				array(
						'ajaxurl' => admin_url('admin-ajax.php'),
						'ajax_loader_url' => W2DC_RESOURCES_URL . 'images/ajax-loader.gif',
						'ajax_map_loader_url' => W2DC_RESOURCES_URL . 'images/ajax-map-loader.gif',
						'in_favourites_icon' => W2DC_RESOURCES_URL . 'images/folder_star.png',
						'not_in_favourites_icon' => W2DC_RESOURCES_URL . 'images/folder_star_grscl.png',
						'in_favourites_msg' => __('Put in favourites list', 'W2DC'),
						'not_in_favourites_msg' => __('Out of favourites list', 'W2DC'),
						'map_style_name' => get_option('w2dc_map_style'), // place it here, not in google_maps class - needed in frontend and backend
						'map_style' => getMapStyle(get_option('w2dc_map_style')), // place it here, not in google_maps class - needed in frontend and backend
				)
		);
		
		$map_content_fields = $this->content_fields->getMapContentFields();
		$map_content_fields_icons = array();
		foreach ($map_content_fields AS $content_field)
			if (is_a($content_field, 'w2dc_content_field') && $content_field->icon_image)
				$map_content_fields_icons[] = W2DC_FIELDS_ICONS_URL . $content_field->icon_image;
			else
				$map_content_fields_icons[] = '';
		wp_localize_script(
				'js_functions',
				'google_maps_objects',
				array(
						'global_map_icons_path' => $this->map_markers_url,
						'marker_image_width' => W2DC_MARKER_IMAGE_WIDTH,
						'marker_image_height' => W2DC_MARKER_IMAGE_HEIGHT,
						'marker_image_anchor_x' => W2DC_MARKER_ANCHOR_X,
						'marker_image_anchor_y' => W2DC_MARKER_ANCHOR_Y,
						'infowindow_width' => W2DC_INFOWINDOW_WIDTH,
						'infowindow_offset' => W2DC_INFOWINDOW_OFFSET,
						'infowindow_logo_width' => W2DC_INFOWINDOW_LOGO_WIDTH,
						'infowindow_logo_height' => W2DC_INFOWINDOW_LOGO_HEIGHT,
						'w2dc_map_content_fields_icons' => $map_content_fields_icons,
						'w2dc_map_info_window_button_readmore' => __('Read more »', 'W2DC'),
						'w2dc_map_info_window_button_summary' => __('« Summary', 'W2DC')
				));
		wp_enqueue_script('google_maps_clasterer');

		// Single Listing page
		if (get_query_var('listing')) {
			wp_enqueue_script('listing_tabs');
			
			wp_enqueue_style('media_styles');
			
			if (get_option('w2dc_images_lightbox')) {
				wp_enqueue_script('media_scripts_lightbox');
				$this->avoid_to_print_these_scripts_twice['media_scripts_lightbox'] = 'lightbox';
			}
			
			if (get_option('w2dc_images_on_tab')) {
				wp_enqueue_style('carousel_style');
				wp_enqueue_style('carousel_theme');
				wp_enqueue_script('carousel_scripts');
			}
		}
	}

	/**
	 * Do not allow to print JS scripts twice, like Google Maps API file from google CDN
	 * 
	 * @param string $src
	 * @param string $handle
	 */
	public function avoid_to_print_scripts_twice($src, $handle) {
		if (array_search($handle, array_keys($this->avoid_to_print_these_scripts_twice)) === false)
			foreach ($this->avoid_to_print_these_scripts_twice AS $src_part)
				if (strpos($src, $src_part) !== false)
					return false;

		return $src;
	}

	/**
	 * enqueue own styles exactly after all other "native" styles
	 */
	public function enqueue_frontend_style() {
		wp_enqueue_style('w2dc_bootstrap');
		wp_enqueue_style('w2dc_frontend');
		wp_enqueue_style('w2dc_colors');
		wp_enqueue_style('w2dc_frontend-custom');
	}
}

$w2dc_instance = new w2dc_plugin();
$w2dc_instance->init();

?>
