<?php 

if (!function_exists('w2dc_getValue')) {
	function w2dc_getValue($target, $key, $default = false) {
		$target = is_object($target) ? (array) $target : $target;
	
		if (is_array($target) && isset($target[$key]))
			return $target[$key];
	
		return $default;
	}
}

if (!function_exists('w2dc_addMessage')) {
	function w2dc_addMessage($message, $type = 'updated') {
		global $w2dc_messages;
	
		if (!isset($w2dc_messages[$type]) || (isset($w2dc_messages[$type]) && !in_array($message, $w2dc_messages[$type])))
			$w2dc_messages[$type][] = $message;
	
		if (session_id() == '')
			@session_start();
	
		if (!isset($_SESSION['w2dc_messages'][$type]) || (isset($_SESSION['w2dc_messages'][$type]) && !in_array($message, $_SESSION['w2dc_messages'][$type])))
			$_SESSION['w2dc_messages'][$type][] = $message;
	}
}

if (!function_exists('w2dc_renderMessages')) {
	function w2dc_renderMessages() {
		global $w2dc_messages;
	
		$messages = array();
		if (isset($w2dc_messages) && is_array($w2dc_messages) && $w2dc_messages)
			$messages = $w2dc_messages;
	
		if (session_id() == '')
			@session_start();
		if (isset($_SESSION['w2dc_messages']))
			$messages = array_merge($messages, $_SESSION['w2dc_messages']);
	
		$messages = w2dc_superUnique($messages);
	
		foreach ($messages AS $type=>$messages) {
			echo '<div class="' . $type . '">';
			foreach ($messages AS $message)
				echo '<p>' . $message . '</p>';
			echo '</div>';
		}
		
		$w2dc_messages = array();
		unset($_SESSION['w2dc_messages']);
	}
	function w2dc_superUnique($array) {
		$result = array_map("unserialize", array_unique(array_map("serialize", $array)));
		foreach ($result as $key => $value)
			if (is_array($value))
				$result[$key] = w2dc_superUnique($value);
		return $result;
	}
}

function w2dc_sumDates($date, $active_days, $active_months, $active_years)
{
	$date = strtotime('+'.$active_days.' day', $date);
	$date = strtotime('+'.$active_months.' month', $date);
	$date = strtotime('+'.$active_years.' year', $date);
	return $date;
}

if (!function_exists('w2dc_renderTemplate')) {
	function w2dc_renderTemplate($template, $args = array(), $return = false) {
		global $w2dc_instance;
	
		if ($args)
			extract($args);
		
		if (is_array($template)) {
			$plugin_template_path = $template[0];
			$template = $template[1];
		} else
			$plugin_template_path = W2DC_TEMPLATES_PATH;

		$core_theme_template_path = get_template_directory() . '/templates/' . $template;
		$core_child_theme_template_path = get_stylesheet_directory() . '/templates/' . $template;
		$core_template_path = $plugin_template_path . $template;

		// first of all check for this template in w2dc theme
		if (defined('W2DC_THEME_MODE') && (is_file($core_theme_template_path) || is_file($core_child_theme_template_path))) {
			if (is_file($core_child_theme_template_path))
				$template = $core_child_theme_template_path;
			else
				$template = $core_theme_template_path;
		} else {
			if (!is_file($template))
				if (!is_file($core_template_path))
					return false;
				else
					$template = $core_template_path;
		}

		$custom_template = str_replace('.tpl.php', '', $template) . '-custom.tpl.php';
		if (is_file($custom_template))
			$template = $custom_template;
	
		if ($return)
			ob_start();
	
		include($template);
		
		if ($return) {
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}
	}
}

function w2dc_getCurrentListingInAdmin() {
	global $w2dc_instance;
	
	return $w2dc_instance->current_listing;
}

function w2dc_getIndexPage() {
	global $wpdb, $wp_rewrite;

	if (!($index_page = $wpdb->get_row("SELECT ID AS id, post_name AS slug FROM {$wpdb->posts} WHERE post_content LIKE '%[" . W2DC_MAIN_SHORTCODE . "]%' AND post_status = 'publish' AND post_type = 'page' LIMIT 1", ARRAY_A)))
		$index_page = array('slug' => '', 'id' => 0);

	// adapted for WPML
	if (function_exists('icl_object_id')) {
		if ($tpage = icl_object_id($index_page['id'], 'page')) {
			$index_page['id'] = $tpage;
			$index_page['slug'] = get_post($index_page['id'])->post_name;
		}
	}
	
	if ($wp_rewrite->using_permalinks())
		// found that on some instances of WP "native" trailing slashes may be missing
		//$index_page['url'] = rtrim(get_permalink($index_page['id']), '/') . '/';
		$index_page['url'] = get_permalink($index_page['id']);
	else
		$index_page['url'] = add_query_arg('page_id', $index_page['id'], home_url());
	
	return $index_page;
}

function w2dc_directoryUrl($path = '') {
	global $w2dc_instance;
	
	// adapted for WPML
	if (function_exists('icl_object_id')) {
		global $sitepress;
		$settings = get_option( 'icl_sitepress_settings' );
		if ($sitepress->get_option('language_negotiation_type') == 3) {
			$code = $sitepress->get_current_language();
			// remove any previous value.
			if ( strpos( $w2dc_instance->index_page_url, '?lang=' . $code . '&' ) !== false ) {
				$w2dc_instance->index_page_url = str_replace( '?lang=' . $code . '&', '', $w2dc_instance->index_page_url );
			} elseif ( strpos( $w2dc_instance->index_page_url, '?lang=' . $code . '/' ) !== false ) {
				$w2dc_instance->index_page_url = str_replace( '?lang=' . $code . '/', '', $w2dc_instance->index_page_url );
			} elseif ( strpos( $w2dc_instance->index_page_url, '?lang=' . $code ) !== false ) {
				$w2dc_instance->index_page_url = str_replace( '?lang=' . $code, '', $w2dc_instance->index_page_url );
			} elseif ( strpos( $w2dc_instance->index_page_url, '&lang=' . $code . '/' ) !== false ) {
				$w2dc_instance->index_page_url = str_replace( '&lang=' . $code . '/', '', $w2dc_instance->index_page_url );
			} elseif ( strpos( $w2dc_instance->index_page_url, '&lang=' . $code ) !== false ) {
				$w2dc_instance->index_page_url = str_replace( '&lang=' . $code, '', $w2dc_instance->index_page_url );
			}
		}
	}

	if (!is_array($path)) {
		if ($path)
			$path = rtrim($path, '/') . '/';
		// found that on some instances of WP "native" trailing slashes may be missing
		$url = rtrim($w2dc_instance->index_page_url, '/') . '/' . $path;
	} else
		$url = add_query_arg($path, $w2dc_instance->index_page_url);

	// adapted for WPML
	if (function_exists('icl_object_id')) {
		$url = $sitepress->convert_url($url);
	}
	
	return $url;
}

function w2dc_get_term_parents($id, $tax, $link = false, $return_array = false, $separator = '/', &$chain = array()) {
	$parent = &get_term($id, $tax);
	if (is_wp_error($parent))
		return $parent;

	$name = $parent->name;
	
	if ($parent->parent && ($parent->parent != $parent->term_id))
		w2dc_get_term_parents($parent->parent, $tax, $link, $return_array, $separator, $chain);
	
	if ($link)
		$chain[] = '<a href="' . get_term_link($parent->slug, $tax) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>';
	else
		$chain[] = $name;
	
	if ($return_array)
		return $chain;
	else
		return implode($separator, $chain);
}

function checkQuickList($is_listing_id = null)
{
	if (isset($_COOKIE['favourites']))
		$favourites = explode('*', $_COOKIE['favourites']);
	else
		$favourites = array();
	$favourites = array_values(array_filter($favourites));

	if ($is_listing_id)
		if (in_array($is_listing_id, $favourites))
			return true;
		else 
			return false;

	$favourites_array = array();
	foreach ($favourites AS $listing_id)
		if (is_numeric($listing_id))
		$favourites_array[] = $listing_id;
	return $favourites_array;
}

function getDatePickerFormat() {
	$wp_date_format = get_option('date_format');
	return str_replace(
			array('S',  'd', 'j',  'l',  'm', 'n',  'F',  'Y'),
			array('',  'dd', 'd', 'DD', 'mm', 'm', 'MM', 'yy'),
		$wp_date_format);
}

function w2dc_getDatePickerLangFile($locale) {
	if ($locale) {
		$locale = str_replace('_', '-', $locale);
		$lang_code = array_shift(explode('-', $locale));
		if (is_file(W2DC_RESOURCES_PATH . 'js/i18n/datepicker-'.$locale.'.js'))
			return W2DC_RESOURCES_URL . 'js/i18n/datepicker-'.$locale.'.js';
		elseif (is_file(W2DC_RESOURCES_PATH . 'js/i18n/datepicker-'.$lang_code.'.js'))
			return W2DC_RESOURCES_URL . 'js/i18n/datepicker-'.$lang_code.'.js';
	}
}

function w2dc_getDatePickerLangCode($locale) {
	if ($locale) {
		$locale = str_replace('_', '-', $locale);
		$lang_code = array_shift(explode('-', $locale));
		if (is_file(W2DC_RESOURCES_PATH . 'js/i18n/datepicker-'.$locale.'.js'))
			return $locale;
		elseif (is_file(W2DC_RESOURCES_PATH . 'js/i18n/datepicker-'.$lang_code.'.js'))
			return $lang_code;
	}
}

function generateRandomVal($val = null) {
	if (!$val)
		return rand(1, 10000);
	else
		return $val;
}

/**
 * Fetch the IP Address
 *
 * @return	string
 */
function ip_address()
{
	if (isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['HTTP_CLIENT_IP']))
		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
	elseif (isset($_SERVER['REMOTE_ADDR']))
		$ip_address = $_SERVER['REMOTE_ADDR'];
	elseif (isset($_SERVER['HTTP_CLIENT_IP']))
		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
	elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else
		return false;

	if (strstr($ip_address, ',')) {
		$x = explode(',', $ip_address);
		$ip_address = trim(end($x));
	}

	$validation = new form_validation();
	if (!$validation->valid_ip($ip_address))
		return false;

	return $ip_address;
}

?>